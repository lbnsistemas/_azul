<?php
	//Get all errors except NOTICE, STRICT e WARNINGS
	error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT ^ E_WARNING);
	
	// private rejeitaria o cache do proxy mas permite que o cliente armazene o conteúdo em cache.
	session_cache_limiter('public');
	// Defines cache limit to 60*24*30 minutes (30 days)
	session_cache_expire((60*24*30));
	// Defines garbage coletor date to 30 days
	ini_set('session.gc_maxlifetime', 30*60*60*24);
	if(isset($_POST['sessionId'])) {
		if($_POST['sessionId']!='') {
			// Registers sessionId in log file
			error_log('SessionID: '.$_POST['sessionId'].' - Datetime:'.date('Y-m-d H:i:s')."\r\n",3,'etc/test_sessions.txt');
			// Defines the session ID, made and sent by remote application
			session_id($_POST['sessionId']);
		}
	}
	
	// Start sessions
	session_start();
	// Timezone Brasil - São Paulo
	date_default_timezone_set('America/Sao_Paulo');
	
	/**
	 * Cria as Statics obrigatórias para o carregamento da página (template)
	 * @param string $pagina : nome da página  a ser carregada pela View
	 * @param string $cliente : define o arquivo config.php e diretorios exclusivos
	 * @return void
	 */ 
	function defineConstants($routesFile) {
		// Check if routes.xml exists
		if(!file_exists($routesFile)) die("Routes file (routes.xml) not exists.");
		// Check if it not received POST data 
		if(!isset($_POST['client'])) {
			$routesXML = new DOMDocument;
			// Load routes file
			$routesXML->loadXML(file_get_contents($routesFile));
			// Load clients
			$clients = $routesXML->getElementsByTagName("client");
			// Get a portion of URL string
			$pURL = parse_url($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			// Get just path of url string
			$pURL = $pURL['path'];
			// Reads the clients
			foreach($clients as $client) {
				$rule1 = $client->getAttribute("rule");// Property of <client>
				//echo $rule1." - ".$pURL;
				//exit();
				if(preg_match($rule1,$pURL,$matches)) {
					// Defines client constant
					$client_nameID = $client->getAttribute("nameID");// Property of <client>
					define("_client", $client_nameID);	
					// Defines aplication constant
					$client_application = $client->getAttribute("application");// Property of <client>
					define( "_application", $client_application );	
					// Load routes in parent <client>
					$routes = $client->getElementsByTagName("route");
					// Reads the routes found
					foreach( $routes as $route ) {
						$rule2 = $route->getElementsByTagName("rule");
						$rule2 = $rule2->item(0)->nodeValue;
						if( preg_match( $rule2, $pURL, $matches ) ) {
							// Module
							$dom = $route->getElementsByTagName("module");
							$name = $dom->item(0)->nodeValue;
							define( "_module", $name );	
							// View
							$dom = $route->getElementsByTagName("view");
							$value = $dom->item(0)->nodeValue;
							$name = (is_numeric($value)===true ? $matches[$value] : $value);
							define( "_view", $name );
							// Access type
							$dom = $route->getElementsByTagName("access");
							$name = $dom->item(0)->nodeValue;
							define( "_access", $name );
							break;
						}
					}
				}
			}
		// Defines constants throug POST variables
		}else {
			define('_application',	$_POST['application']);
			define('_client',		$_POST['client']);
			define('_module',		$_POST['module']);
			define('_view',			$_POST['view']);
		}
		// Check the constants: _client, _application e _module 
		if(!defined('_client') || !defined('_application') || !defined('_module')) 
			die("_client, _application ou _module n&atildeo reconhecido(s).");
		// Check the constants content defined above
		if(_client=='' || _application=='' || _module=='')
			die("_client, _application ou _module est&atildeo vazio(s).");
	}
	
	// Defines the system constants based on routes file 
	defineConstants('routes.xml');	

	// Master plataform config file 
	require_once 'application/'._application.'/'._client.'/'._module.'/config.php';
	
	// Calls  the custom error functions
	require_once SYS_SRC_LIBS."error.php";
	
	// Loads and starts class Controller
	// Controller file loads Controller.classes.php
	require_once SYS_SRC_LIBS.'Controller.php';
	$controller = new Controller();
	
	// Starts a new instance of Database class
	$azul_DB = $controller->getMysqli();
	
	// Loads and starts class View
	require_once SYS_SRC_LIBS.'View.php';
	$view = new View();
	
	/*Checks 'www'
	if(strpos($view->urlVirtual(),'www.')!==false) {
		$newURL = str_replace("www.","",$view->urlVirtual());
		$view->printLocation($newURL);
	}*/	

	// Valid request for Controller
	if($controller->isControllerRequest()) {
		
		require(SYS_SRC_FRAMEWORK."webservice.php");
		
		
	// Others requests
	} else {
		// Alteração dia 11/08/2017 - Lucas Nunes
		// Realiza acesso via token (acesso direto de outros dispositivos)
		$ret = $controller->doLoginByToken();
		//var_dump($ret);
		//exit();
		
		// Checks access type
		if(_access!='public') {
			// Verifica se a view necessita de validação de sessão, caso necessite : 
			// verifica se o usuário está logado em uma sessão ativa e tem permissão de visualização sobre a View
			$ret = $controller->checksPermission('V',_view);
			
			//var_dump($_SESSION);
		 	//exit();	
		}else $ret=true;	
			
		// Se houver permissão e login ativo:
		if($ret===true) {
			// Mostra a view requerida
			require SYS_SRC_APP.'views/'._view.'/'._view.'.php';
			
		// Usuário não logado :
		}else if($ret==-1){
			// Mostra view de login
			require SYS_SRC_APP.'views/login/login.php';
			
		// Sem permissão de visualização (V) para a view requerida :
		}else {
			$msgErro = "Permiss&atilde;o negada. Verifique o cadastro de usu&aacute;rios.";
			require SYS_SRC_APP.'views/errorPages/general.php';
			
		}	
	}	
	
?>	