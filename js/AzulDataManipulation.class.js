/**
  @class AzulDataManipulation
  @author Lucas Nunes <devmaster@lbnsistemas.com.br>
  @description Objeto para enviar as ações da view e receber o retorno do controller
  @requires jQuery, moment
 */
var AzulDataManipulation = function () {
  // Construtor
};
/**
  @description Função para formatar o padrão de entrada de um determinado campo
  @param mask:string => ex.: "###-###-###-###" -> 123.123.123.123
  @param outputType:string =>  9 - Somente números, A - Somente Texto, * - Alfanumerico
  @param field:Dom element
  @param event:Dom event
  @return void
*/
AzulDataManipulation.prototype.formatField = function (mask, outputType, field, event){
	// Verifica se foi informado uma mascara ou um array com mais de 1 mascara
	var mask1 = $.isArray(mask)==true ? mask[0] : mask;

	// Correção de BUG : 10/07/2016
	// Veirifica se está selecionado, se estiver permite a edição
	var selIni = $(field)[0].selectionStart;
	var selFim = $(field)[0].selectionEnd;
	if(selIni != selFim) {
		return(true);
	}
	if (document.all){var evt=event.keyCode;} // caso seja IE
	else{var evt = event.charCode;}     // do contrário deve ser Mozilla
	var chr= String.fromCharCode(evt);      // pegando a tecla digitada
	if (evt==0) {return true; } //Se for a tecla APAGAR libera

	// Permite apenas letra
	if(outputType=='A') {
		// Se estiver dentro da faixa de caracteres numericos, encerra
		if (evt > 47 && evt < 58) return(false);
	// Permite apenas numero
	}else if(outputType=='9') {
		// Se estiver fora da faixa de caracteres numericos, encerra
		if (evt > 57 || evt < 48) return(false);
	// Permite tudo
	}else if(outputType=='*') {
		//return(true);
	}

	// Realização formatação do campo com a mascara informada :

	var totalCampo = $(field).val().length; // Pega o total de caracteres atualmente no campo
	var value=$(field).val();// Pega o conteúdo atual do campo
	// Verifica se chegou ao final da primeira mascara
	if(totalCampo >= mask1.length && $.isArray(mask)==true) {
		// Pega a segunda posição do array de mascaras
		var mask2 = mask[1];
		// Verifica se o campo tem o mesmo tamanho de mask2 e encerra
		if(mask2.length==value.length) return(false);
		// Retira toda formatação antiga do valor atual do campo
		for(var c=0; c < mask1.length; c++ ){
			if(mask1[c]!='#'){
				value = value.replace(mask1[c],'');
			}
		}
		// Ativa a mascara 2 para padronização
		var maskAtiva = mask2;
		// Padroniza com o formato da mascara ativa
		var novoValue = '';
		for(var c=0,c2=0; c < mask2.length; c++ ){
			if(maskAtiva[c]!='#'){
				novoValue += mask2[c];
			}else {
				novoValue += value[c2];
				c2++;
				// Se já padronizou toda string, encerra
				if(value.length==c2) break;
			}
		}
		// Escreve o valor repadronizado
		$(field).val(novoValue);
	// Caso contrário, ativa a mascara 1 para padronização
	}else {
		var maskAtiva = mask1;
	}

	// Continua o processo normalmente utilizando a mask2, redefinindo as variaveis :

	var totalCampo = $(field).val().length; // Pega o total de caracteres atualmente no campo
	var restoMascara = maskAtiva.substring(totalCampo);// Restante da mascara a ser trabalhada
	// Verifica se o primeiro char do restante da mascara é diferente do caracter de mascara ("#")
	if (restoMascara.substring(0,1) != '#' ){
		var pontuacao = restoMascara.substring(0,restoMascara.indexOf('#'));
		// Qualquer outro char diferente de # é adicionado ao campo
		$(field).val($(field).val() + pontuacao);
	}
}
/**
   @description Função para formatar campos de valores monetarios
   @param objTextBox:DOM Element
   @param millionthSeparator:string => '.' or ','
   @param decimalSeparator:string => '. ou ,'
   @param e:DOM Event
   @return void|boolean
*/
AzulDataManipulation.prototype.formatCurrency = function(objTextBox, millionthSeparator, decimalSeparator, e){
	var sep = 0;
	var key = '';
	var i = j = 0;
	var len = len2 = 0;
	var strCheck = '0123456789';
	var aux = aux2 = '';
	// Correção de BUG : 10/07/2016
	// Veirifica se está selecionado, se estiver permite a edição
	selIni = $(objTextBox)[0].selectionStart;
	selFim = $(objTextBox)[0].selectionEnd;
	if(selIni != selFim) {
		return(true);
	}
	// IE 8 var whichCode = (window.Event) ? e.which : e.keyCode;
	var whichCode = (window.addEventListener) ? e.which : e.keyCode;
	// 13=enter, 8=backspace as demais retornam 0(zero)
	// whichCode==0 faz com que seja possivel usar todas as teclas como delete, setas, etc
	if ((whichCode == 13) || (whichCode == 0) || (whichCode == 8)){
		return(true);
	}
	key = String.fromCharCode(whichCode); // Valor para o código da Chave
	if (strCheck.indexOf(key) == -1)
		return(false); // Chave inválida
	len = objTextBox.value.length;
	for(i = 0; i < len; i++)
		if (($(objTextBox).val().charAt(i) != '0') && ($(objTextBox).val().charAt(i) != decimalSeparator))
			break;
	aux = '';
	for(; i < len; i++)
		if (strCheck.indexOf($(objTextBox).val().charAt(i))!=-1)
			aux += $(objTextBox).val().charAt(i);
	aux += key;
	len = aux.length;
	if (len == 0)
		$(objTextBox).val('');
	if (len == 1)
		var value = '0'+ decimalSeparator + '0' + aux;
		$(objTextBox).val(value);
	if (len == 2)
		var value = '0'+ decimalSeparator + aux;
		$(objTextBox).val(value);
	if (len > 2) {
		aux2 = '';
		for (j = 0, i = len - 3; i >= 0; i--) {
			if (j == 3) {
				aux2 += millionthSeparator;
				j = 0;
			}
			aux2 += aux.charAt(i);
			j++;
		}
		$(objTextBox).val('');
		len2 = aux2.length;
		var value='';
		for (i = len2 - 1; i >= 0; i--)
			value += aux2.charAt(i);
		value += decimalSeparator + aux.substr(len - 2, len);
		$(objTextBox).val(value);
	}
	return(false);
}
/**
 	 @description Função para inverter o formato de uma data (BR / US)
   @param date:string
   @return string (data formatada ou '')
*/
AzulDataManipulation.prototype.invertDate = function(date) {
	if(date == undefined ) {
		_azGeneral.alert('Ops !', 'A data informada não é válida.','falha');
		return(false);
	}else if(date.trim() == '') return(false);
	// Identifica o separador
	if(date.indexOf('-')>-1) {
		separador = '-';
	}else if(date.indexOf('/')>-1) {
		separador = '/';
	}else {
		_azGeneral.alert('Ops !', 'A data informada não é válida.','falha');
		return(false);
	}
	// Identifica o formato inicial (BR ou US)
	formatoInicial = date.indexOf(separador)==2 ? 'BR' : 'US';
	// Fatia a data
	ar_data = date.split(" ");
	data = ar_data[0].split(separador);
	hora = "";
	if (ar_data[1]!=undefined) {
		hora = ar_data[1];
	}
	hora = hora!="" ?  " "+hora : '';
	// Converte para o formato US
	if (formatoInicial=='BR') {
		separador = '-';
		dia = data[0];
		mes = data[1];
		ano = data[2];
		formatoFinal = ano+separador+mes+separador+dia+hora;
		// Valida a data no formato US
		dataValida = moment(formatoFinal,'YYYY'+separador+'MM'+separador+'DD').isValid();
	// Converte para o formato BR
	}else if(formatoInicial=='US') {
		ano = data[0];
		mes = data[1];
		dia = data[2];
		formatoFinal = dia+separador+mes+separador+ano+hora;
		// Valida a data no formato BR
		dataValida = moment(formatoFinal,'DD'+separador+'MM'+separador+'YYYY').isValid();
	}
	// Certifica-se se é uma data válida
	if(dataValida) {
		return(formatoFinal);
	// Data inválida
	}else {
    _azGeneral.alert('Ops !', 'A data informada não é válida.','falha');
		return('');
	}
}
/**
 	 @description Converter padrão moeda para  float, ex.: "12,55" -> 12.55
   @param value:string
   @return float
*/
AzulDataManipulation.prototype.currencyToFloat = function(value){
	var output = 0;
	output = value;
	if(output==0) {
		return 0.00;
	}
	if (output.trim()=="" ) {
		return 0.00;
	}
	output = output.replace(/\./g, "");
	output = output.replace(",",".");
	output = parseFloat(output);
	output = ((Math.round(output*100))/100);
	return output;
}
/**
 	 @description Converter padrão moeda para  float
   com precisão de x casas decimais, ex.: ex.: "12,55" -> 12.550 (3 casas decimais)
   @param value:string
   @param decimalHouses:number
   @return float
*/
AzulDataManipulation.prototype.currencyToFloatPrec = function (value,decimalHouses){
	var output = 0;
	output = value;
	if (output.trim()=="") {
		return 0.00;
	}
	output = output.replace(/\./g, "");
	//Verifica a quantidade de casas decimais
	for(var c = 0; c < decimalHouses; c++ ){
	   if(c==0) {
			mult = 10;
	   }else {
			mult *= 10;
	   }
	}
	output = output.replace(",",".");
	output = parseFloat(output);
	output = ((Math.round(output*mult))/mult);
	return output;
}
/**
 	 @description Convert um float para padrão moeda, ex.: 12.55 -> '12,55'
   @param num:float
   @return string
*/
AzulDataManipulation.prototype.floatToCurrency = function (num) {
   x = 0;
   if(num<0) {
      num = Math.abs(num);
      x = 1;
   }
	 if(isNaN(num)) num = "0";
   cents = Math.floor((num*100+0.5)%100);
   num = Math.floor((num*100+0.5)/100).toString();
   if(cents < 10) cents = "0" + cents;
      for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
         num = num.substring(0,num.length-(4*i+3))+'.'
               +num.substring(num.length-(4*i+3));
	 ret = num + ',' + cents;
   if (x == 1) ret = ' - ' + ret;return ret;
}
/**
 	 @description Transforma inteiro em formato moeda,
   com precisão de casas decimais, ex.: 12.55 -> '12.550' (3 casas decimais)
   @param num:float
   @param decimalHouses:number
   @return string
*/
AzulDataManipulation.prototype.floatToCurrencyPrec = function (num, decimalHouses) {
	//Variaveis
	nNeg = false;
	cents = '';
	if(isNaN(num))  {
		num = "0";
	}
	if(num<0) {
	  num = Math.abs(num);
	  nNeg = true;
	}
	//Verifica a quantidade de casas decimais
	for(c = 0; c < decimalHouses;c++ ){
	   if(c==0) mult = 10; else mult *= 10;
	}
	//Separa os centesimos do numero
	numString = num.toString();
	if(numString.indexOf('.')>-1) {
		numString = numString.split(".");
		cents = numString[1].substring(0,casasDec);
	} else {
		for(i=0;i<casasDec;i++) {
			cents += '0';
		}
	}
	//Pega apenas o numero inteiro
	num = Math.floor((num*100+0.5)/100).toString();
	//Coloca os pontos das casas de milhares
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++) {
		num = num.substring(0,num.length-(4*i+3))+'.'+num.substring(num.length-(4*i+3));
	}
	//Acrescenta as casas decimais
	ret = num + ',' + cents;
	//Verifica se é número negativo
	if (nNeg == 1) ret = ' - ' + ret;
	//Retorno da função
	return ret;
}
/**
 	 @description Função para permitir somente número e mais alguns caracteres
   informados no argumento 'args'
   @param event:DOM Event
   @param args:string
   @return boolean
*/
AzulDataManipulation.prototype.onlyNumbers = function(event,args) {
	event = event || window.event // IE does not pass event to the function
  evt=event.which;
	var valid_chars = '0123456789'+args;    // criando a lista de teclas permitidas
	var chr= String.fromCharCode(evt);      // pegando a tecla digitada
	if (evt==8) {return true; } //Se for a tecla APAGAR libera
	if (valid_chars.indexOf(chr)>-1 ){return true;} // se a tecla estiver na lista de permissão permite-a
	// para permitir teclas como <BACKSPACE> adicionamos uma permissão para
	// códigos de tecla menores que 09 por exemplo (geralmente uso menores que 20)
	if (valid_chars.indexOf(chr)>-1 || evt < 20){return true;} // se a tecla estiver na lista de permissão permite-a
	return(false);   // do contrário nega
}

/**
   @description Configura a função de formatação para o tipo de dado
   do campo informado no arg 'obj'
   @param {object} obj
   @return {void}
 */
AzulDataManipulation.prototype.setFormatting = function(obj, format='') {
  // Tipo de dados do campo
  var objDataType = $(obj).attr('az-dataType')==undefined  ? $(obj).prop('type') : $(obj).attr('az-dataType');
	if(format==''){
	  switch(objDataType) {
	    // Formata campo para receber cpf
	    case 'cpf' :
	      format = '###.###.###-##';
	      break;
	    // Formata campo para receber cnpj
	    case 'cnpj' :
	      format = '##.###.###/####-##';
	      break;
	    // Formata o campo para receber ie
	    case 'ie' :
	      format = '###.###.###.###';
	      break;
	    // Formata o campo para receber postal
	    case 'postal' :
	      format = '##.###-###';
	      break;
	    // Formata o campo para receber telefone
	    case 'phone' :
			case 'tel' :
	      format = ['(##) ####-####','(##) #.####-####'];
	      break;
	    // Formata o campo para receber data
	    case 'date' :
	      format = '##/##/####';
	      break;
	    // Formata o campo para receber hora
	    case 'time' :
	      format = '##:##';
	      break;
	    // Formata o campo para receber dataHora
	    case 'dateTime' :
	      format = '##/##/#### ##:##';
	      break;
	    // Formata o campo para receber apenas número
	    case 'number' :
	      format = '##############';
	      break;
		// Formata o campo para receber Moeda
	    case 'currency' :
		  $(obj).unbind("keypress");
		  var self = this;
		  $(obj).on("keypress",function(event) {
	      	return(self.formatCurrency(obj, '.', ',', event));
		  });
		  return(true);
	      break;
	    default : return(true);
	  }
	}
  // Configura a função no evento keyPress do campo
  $(obj).unbind("keypress");
  var self = this;
  $(obj).on("keypress",function(event) {
    return(self.formatField(format,'9',this,event));
  });
 // $(obj).attr("onKeyPress","return(_azDataManipulation.formatField('"+format+"','9',this,event))");
  
}
/**
  @description Faz checkIn tratando o valor selecionado/preenchido antes do envio ao WS
  utilizar a mesma estrutura de campos do Bootstrap, porem com os atributos : az-dataType
  @param {DOMElement} obj
  @return string or float
*/
AzulDataManipulation.prototype.checkIn = function(obj) {
  var retorno = '';
  // Nome do nó (input, textarea,select,..)
  objNodeName = $(obj).prop('nodeName').toLowerCase();
  // Tipo do campo (text, password, radio, checkbox)
  objType = $(obj).attr('type') != undefined ? $(obj).attr('type').toLowerCase() : '';
  // Tipo de dados deste campo
  objDataType = $(obj).attr('az-dataType').trim();
  // Se for um input (radio, checkbox)
  if( objNodeName=='input' &&  objType=='checkbox' ) {
    valueWithoutTreatment = $(obj).prop('checked')==true ? '1' : '0';

  // Se for um text area
  }else if(objNodeName=='textarea') {
	valueWithoutTreatment = $(obj).val().trim()!='' ? $(obj).val().trim() :  $(obj).text().trim();

  // Para todos outros campos (input)
  }else   {
    valueWithoutTreatment = $(obj).val().trim();

  }
  // Verifica se é um array, e convert para uma string
  if(typeof valueWithoutTreatment == 'object' && valueWithoutTreatment!=null) {
    valueWithoutTreatment = valueWithoutTreatment.toString();
  }
  // Tipo de dados data/hora ou data
  if(objDataType.indexOf('date')>-1) {
    retorno = this.invertDate(valueWithoutTreatment);
  }else if(objDataType=='currency') {
    retorno = this.currencyToFloat(valueWithoutTreatment);
  }else {
    retorno	= valueWithoutTreatment;
	// fix BUG URL strings
	retorno = this.escapeForJSON(retorno);
  }
  return(retorno);
}
/**
  * Escapa caracteres especiais
  */
AzulDataManipulation.prototype.escapeForJSON = function(string){
	// fix BUG URL strings
	retorno = encodeURIComponent(string);
	retorno = retorno.replace(/%22/g,'\"');
	retorno = retorno.replace(/%0A/g, '\n').replace(/\r/g, '\r').replace(/\t/g, '\t');
	return(retorno);
}
/**
   !BETA
   @description Define o array com os objetos dos filtros de um determinado container
   @param {string} container : nameID do container
 */
AzulDataManipulation.prototype.getArrayFilters = function (container) {
  var arFilters=[];
  // Se for informado uma string em form :
  if(typeof container == 'string') {
	 var filterGroups = $('#'+container).find('.az-filter-group');

     for (var fg=0; fg < filterGroups.length; fg++) {
		// (Re)cria o objeto filtro
		var objFilter = new Object();
		// Define o corrente grupo a ser analisado
		var filterGroup = filterGroups[fg];
		// Encontra o(s) campos(s) do corrente grupo de filtro (máximo dois no caso de um periodo, min e max...)
		var filterFields = $(filterGroup).find('.az-field');
		var contFields = 0;
		for(var ff=0;ff<filterFields.length;ff++) {
			var filterField = filterFields[ff];
			objFilter.value1 = $(filterField).val();
			if(contFields>0) {
				objFilter.value2 = $(filterField).val();
			}
			contFields++;
		}
		objFilter.rule = $(filterGroup).attr('az-filter-rule');
		objFilter.columns = $(filterGroup).attr('az-filter-columns').split(',');
		// Incrementa o array arFilters com o novo objeto criado
		arFilters.push(objFilter);
	}
  // Erro
  }else {
    this.alert("Desculpe-nos !","Houve um erro no carregamento dos filtros. Tente novamente.","fail");
    return(false);
  }
  // Retorna os campos encontrados
  return(arFilters);
}
// Cria nova instancia para AzulDataManipulation
//_azDataManipulation = new AzulDataManipulation();
//console.log('file "AzulDataManipulation.class.js" is ready!');
