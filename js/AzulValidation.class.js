/**
  @class AzulValidation
  @author Lucas Nunes <devmaster@lbnsistemas.com.br>
  @description Realiza a validação de formulários considerando as regras
  definidas em cada campo e na tag <form>
  Atributos :
  az-condition (será obrigatorio se a condição for verdadeira),
  az-dataType (date, time, dateTime, number, currency, text, postal, cpf, cnpj, ie, password, phone, email, html)
  az-required ('true' or 'false')
  az-custom  (reservado para validações server-side)
  az-remoteMessage (mensagem de erro retornada do servidor remoto)
  @requires jQuery
 */
var AzulValidation = function () {
    // Construtor
}
/**
   Função para exibir erros de validação, mostrando os campos com borda vermelha
   @param string fieldNameID, string message
   @return void
 */
AzulValidation.prototype.playError = function(fieldNameID, message) {
    // Pega o grupo do campo
    var obj = $('#'+fieldNameID).closest('.form-group');
    // Adiciona classe de erro para o grupo do campo
    obj.addClass('has-error');
    // Adiciona mensagem de erro ao campo
    obj.find('.help-block').html(message);
}
/**
   Função para tirar a borda vermelha e as mensagens de erro de validação (caso tenha esteja ok!)
   @param string fieldNameID
   @return void
 */
AzulValidation.prototype.turnOffErrors = function (fieldNameID) {
    // Pega o grupo do campo
    var obj = $('#'+fieldNameID).closest('.form-group');
    // Remove classe de erro para o grupo do campo
    obj.removeClass('has-error');
    // Remove mensagem de erro ao campo
    obj.find('.help-block').html('');
},
/**
   Função para retornar o exemplo de preenchimento para um det. tipo de Dado
   @param string dataType
   @return string
 */
AzulValidation.prototype.exampleOfFilling = function(dataType) {
  var retorno = '';
  switch(dataType){
    case "data" :
      retorno = 'Ex. : 01/01/2020';
      break;
    case "hora" :
      retorno = 'Ex. : 12:00';
      break;
    case "dataHora" :
      retorno = 'Ex. : 01/01/2020 12:00';
      break;
    case "moeda" :
      retorno = 'Ex. : 1.000,00';
      break;
    case "postal" :
      retorno = 'Ex. : 18.160-000';
      break;
    case "cpf" :
      retorno = 'Ex. : 999.999.999-00';
      break;
    case "cnpj" :
      retorno = 'Ex. : 99.999.999/0001-00';
      break;
    case "ie" :
      retorno = 'Ex. : 999.999.999.999';
      break;
    case "phone" :
      retorno = 'Ex. : (15) 9.9999-999 ou (15) 3033-4715';
      break;
    case "email" :
      retorno = 'Ex. : teste@gmail.com.br';
      break;
  }
  return(retorno);
}
/**
   @description Função para validar CNPJ
   @param  el:DOM Element
   @return boolean
*/
AzulValidation.prototype.validaCnpj = function(el) {
  var i = 0;
  var l = 0;
  var strNum = "";
  var strMul = "6543298765432";
  var character = "";
  var iValido = 1;
  var iSoma = 0;
  var strNum_base = "";
  var iLenNum_base = 0;
  var iLenMul = 0;
  var iSoma = 0;
  var strNum_base = 0;
  var iLenNum_base = 0;
  var cnpj = $(el).val();
	//Limpa pontos e Traços da string
	cnpj = cnpj.replace(/\./g, "");
	cnpj = cnpj.replace(/\-/g, "");
	cnpj = cnpj.replace(/\//g, "");
  //CNPJ INVALIDO
  if (cnpj.length != 14 || cnpj == "00000000000000" || cnpj == "11111111111111" || cnpj == "22222222222222"
  || cnpj == "33333333333333" || cnpj == "44444444444444" || cnpj== "55555555555555" || cnpj == "66666666666666"
  || cnpj == "77777777777777" || cnpj == "88888888888888" || cnpj == "99999999999999") return(false);

  l = cnpj.length;
  for (var i = 0; i < l; i++) {
		caracter = cnpj.substring(i,i+1)
		if ((caracter >= '0') && (caracter <= '9'))
		   strNum = strNum + caracter;
  };
  strNum_base = strNum.substring(0,12);
  iLenNum_base = strNum_base.length - 1;
  iLenMul = strMul.length - 1;
  for(i = 0;i < 12; i++)
		iSoma = iSoma +
						parseInt(strNum_base.substring((iLenNum_base-i),(iLenNum_base-i)+1),10) *
						parseInt(strMul.substring((iLenMul-i),(iLenMul-i)+1),10);

  iSoma = 11 - (iSoma - Math.floor(iSoma/11) * 11);
  if(iSoma == 11 || iSoma == 10)
		iSoma = 0;

  strNum_base = strNum_base + iSoma;
  iSoma = 0;
  iLenNum_base = strNum_base.length - 1
  for(i = 0; i < 13; i++)
		iSoma = iSoma +
						parseInt(strNum_base.substring((iLenNum_base-i),(iLenNum_base-i)+1),10) *
						parseInt(strMul.substring((iLenMul-i),(iLenMul-i)+1),10)

  iSoma = 11 - (iSoma - Math.floor(iSoma/11) * 11);
  if(iSoma == 11 || iSoma == 10)
		iSoma = 0;
  strNum_base = strNum_base + iSoma;
 //CNPJ INVALIDO
  if(strNum != strNum_base) return(false);
  //CNPJ VALIDO
  return (true);
}
/**
   @description Função para validar CPF
   @param  el:DOM Element
   @return boolean
*/
AzulValidation.prototype.validaCpf = function(el) {
	var cpf = $(el);
	//RETIRA TODOS CARACTERES QUE NAO SAO NUMEROS
	cpf = cpf.val().replace('.','');
	cpf = cpf.replace('-','');
	cpf = cpf.replace('.','');
	if (cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf== "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999")
	return(false);
	var add = 0;
	for (var i=0; i < 9; i ++)
	add += parseInt(cpf.charAt(i)) * (10 - i);
	rev = 11 - (add % 11);
	if (rev == 10 || rev == 11)
	rev = 0;
	if (rev != parseInt(cpf.charAt(9)))
	return(false);
	add = 0;
	for (i = 0; i < 10; i ++)
	add += parseInt(cpf.charAt(i)) * (11 - i);
	rev = 11 - (add % 11);
	if (rev == 10 || rev == 11)
	rev = 0;
	if (rev != parseInt(cpf.charAt(10))) return(false);
	return(true);
}
/**
   @description Validação de campos de datas (JS puro)
   aceita somente o padrão BR : dd/mm/YYYY H:m:s
   @param  field:DOM Element
   @return boolean
*/
AzulValidation.prototype.validaDataHora = function(field)
{
	if($(field).val().trim()!="" && field !=undefined){
		var bissexto = 0;
		var data = $(field).val();
		var tam = data.length;
		// Define o separador
		if(data.indexOf('-')>-1) {
			separador = '-';	
		}else {
			separador = '/';		
		}
		// Se houver hora e minuto no campo, verifica
		if(tam>10) {
			var hora = parseInt(data.substr(10,2));
			var minuto = parseInt(data.substr(13,2));
			if(hora>24 || minuto>59) {
				return(false);
			}else if(hora==24 && minuto>0) {
				return(false);
			}
		}
		//Padrão BR
		if(data.indexOf(separador)==2){
		  var dia = data.substr(0,2);
			var mes = data.substr(3,2);
			var ano = data.substr(6,4);
	
		//Padrão EUA
		}else {
		  var dia = data.substr(8,2);
			var mes = data.substr(5,2);
			var ano = data.substr(0,4);
		}
		if ((ano > 1900)||(ano < 2100))
		{
			switch (mes)
			{
				case '01':
				case '03':
				case '05':
				case '07':
				case '08':
				case '10':
				case '12':
					if  (dia <= 31)
					{
						return(true);
					}
					break

				case '04':
				case '06':
				case '09':
				case '11':
					if  (dia <= 30)
					{
						return(true);
					}
					break
				case '02':
					/* Validando ano Bissexto / fevereiro / dia */
					if ((ano % 4 == 0) || (ano % 100 == 0) || (ano % 400 == 0))
					{
						bissexto = 1;
					}
					if ((bissexto == 1) && (dia <= 29))
					{
						return(true);
					}
					if ((bissexto != 1) && (dia <= 28))
					{
						return(true)
					}
					break
			}
		}
  // Apenas hora
	}else if(tam==5) {
		if(hora<24 && minuto<60) {
			return(true);
		}else if(hora==24 && minuto==0) {
			return(true);
		}
	}
	return(false);
}
/**
   @description Função para validar email
   @param  obj:DOM Element
   @return boolean
*/
AzulValidation.prototype.validEmail = function(obj) {
  var self = this;
  var email = $(obj).val();
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}
/**
   @description Função para validar telefone (fixo ou celular)
   @param  obj:DOM Element
   @return boolean
*/
AzulValidation.prototype.validPhone = function(obj) {
  var str = $(obj).val();
  var reg = /^(?:\(\+[0-9]{2} \))?\([1-9]{2}\) 9\.[0-9]{4}-[0-9]{4}$/;
  return reg.test(str);
}
/**
   @description Função para validar telefone (fixo ou celular)
   @param  obj:DOM Element
   @return boolean
*/
AzulValidation.prototype.validarTelefone = function(obj) {
  var self = this;
  var numberPattern = /\d+/g;
  var telfone = $(obj).val().match( numberPattern ).join('');
  var re = /^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/;
  return re.test(telfone);
}

/**
   @description Inicia as principais funções do validador
   incluindo as bibliotecas necessárias e setando os eventos para cada campo
   @param  form:string | DOM Element
   @return boolean
*/
AzulValidation.prototype.init = function(form, dynamic=false, ID='') {
  var self = this;
  console.log(form);
  // Se não foi informado ID nem Class HTML e também não é um objeto, usa o método default (ID)
  if(typeof form == 'string') {
    if(form.indexOf('.')==-1 && form.indexOf('#')==-1) {
      form1 = '#'+form;
    }else form1 = form;
  }else form1 = form;
  // Desabilita o envio automatico do formulario
  jQuery(form1).submit(function(e){
    e = e || window.event;
    // Verifica se é um formulário com validação captcha (imagem de códigos embaralhados)
    if($(this).hasClass('az-form-captcha')==true) {
      // Verifica se o campo do captcha ainda não foi criado
      if($(this).find('.az-field-captcha').length ==0 ){
        // Gera um ID para a imagem captcha
		
        var idCaptcha = 'imgCaptcha'+Math.floor(Math.random()*1000);
        // Botao submit
        var objSubmit = $($(this).find('[type="submit"]'));
        // Cria o popover e vincula ao submit
        objSubmit.popover(
          {
            html: true,
            content : '<img src="'+_SYS_DOMAIN+'az-captcha" id="'+idCaptcha+'" /> <a href="javascript: self.updateCaptcha(\'#'+idCaptcha+'\')">atualizar</a> <input class="form-control az-field az-field-captcha" name="captcha" az-required="true" az-dataType="text" type="text" size="15" />',
            title : 'Digite o código :',
            trigger : 'manual'
          }
        );
        // Mostra pop-over
        objSubmit.popover('show');
      }
    }
    //if(!self.exec(this)){
      //e.preventDefault();
    //}
  });

  // Pega os campos do formulario(s) ou do pacote principal
  var arFields =  _azGeneral.getFields(form, dynamic, ID);
   
  // Prepara os eventos de cada campo para validação quando sofrerem alguma alteração
  for(var i=0; i < arFields.length; i++) {
    obj = arFields[i];
    // Nome do nó (input, textarea, select, checkbox)
    var objNodeName = $(obj).prop('nodeName').toLowerCase();
    // Tipo do campo (text,password, radio, checkbox)
    var objType = $(obj).attr('type') != undefined ? $(obj).attr('type').toLowerCase() : '';
    // Tipo de dados do campo
    var objDataType = $(obj).attr('az-dataType');
    // Inclui biblioteca para validação de IE
    if(objDataType=='ie') {
      // Verifica se já foi incluído
      if(window.inscricaoEstadual==undefined) {
        _azGeneral.addPlugin('ie.min.js');
      }
    }
    // Se for um input (text, password) ou textarea
    if( objNodeName=='input' && (objType=='input' || objType=='text' || objType=='password') || objNodeName=='textarea' || objNodeName=='div') {
      $(obj).on('blur',function() {
        if($(this).parent().hasClass('has-error')) {
          self.exec(form);
        }
      });
      // Inicia função para formatação dos tipos iniciais
      _azDataManipulation.setFormatting(obj);
    // Se for um input (radio, checkbox)
    }else if( objNodeName=='input' && (objType=='radio' || objType=='checkbox') ) {
      $(obj).on('click',function() {
		  
        if($(this).parent().hasClass('has-error')) {
          self.exec(form);
        }
      });
    // Se for um select
    }else if( objNodeName=='select' || objNodeName=='select2'   ) {
      $(obj).on('change',function() {
        if($(this).parent().hasClass('has-error')) {
          self.exec(form);
        }
      })
    }
  }
  return(true);
}
/**
   @description Valida um determinado formulario
   @param {string} form : nameID do formulario ou a palavra "Principal" (todos formularios do pacote principal)
   @param {bool} dynamic : true (formulario dinamico) ou false
   @param {integer} ID : PK do cadastro principal ou do cadastro dinamico relacionado
 */
AzulValidation.prototype.exec = function(form, dynamic=false, ID='') {
  // console.log("Quem chamou a função foi: "+this.exec.caller.name);
  // Pega os campos do formulario(s) ou do pacote principal
  var arFields =  _azGeneral.getFields(form, dynamic, ID);
  // Status da validação do formulario
  var validationStatus = true;
  // Percorre a lista de campos
  for(var h=0; h < arFields.length; h++) {
    // Seleciona um campo da lista
    var obj = arFields[h];
    // Id do campo selecionado
    var objId = $(obj).attr('id');
    // Tipo de dados do campo
    var objDataType = $(obj).attr('az-dataType');
    // Conteúdo html* (específico para capo tipo div)
    if(objDataType=='html') {
      var objValue = $(obj).summernote('code');
    // Para qualquer outros campos :
    }else {
      // Cria uma referencia ao campo com o ID
      // DEPRECATED eval("var "+objId+"=typeof $(obj).val() == 'string' ? $(obj).val().trim() : $(obj).val();");
      // Value do campo selecionado
      var objValue = typeof $(obj).val() == 'string' ? $(obj).val().trim() : $(obj).val();
    }
    // Regra de validação
    var objRequired = $(obj).attr('az-required');
    // Atributo para validação customizada (validação de usuarios, consistência de dados, ...)
    var objCustom = $(obj).attr('az-custom');
    // Mensagem de erro pré-definida ou estabelecida através de validação remota
    var remoteMsgError = $(obj).attr('az-remoteMessage');
    // Verifica se o campo é passível de validação nos seguintes casos:
    // Obrigatorio ou (cpf,cnpj,ie,email, data,data/hora,hora) ou Validação remota(az-custom=true ou false)
    if(objRequired=='true' || objDataType=='cpf' || objDataType=='cnpj' || objDataType=='ie'
    || objDataType=='email' || objDataType=='date' || objDataType=='phone'  || objCustom!=undefined  ) {
      /**
       * Validação de preenchimento obrigatório
       */
      if(	objRequired=='true' && _azGeneral.isEmpty(objValue)==true) {
        this.playError(objId, 'Campo obrigatório');
        validationStatus = false;
        continue;
      // OK!
      }else if (objRequired=='true' && _azGeneral.isEmpty(objValue)==false ) {
        this.turnOffErrors(objId);
      }
      /**
       * Validação de Telefone ou Celular
       */
      if(	objDataType=='phone' && $(obj).val().trim()!='' ) {
        if(this.validarTelefone($(obj))==false) {
          this.playError(objId, 'Telefone inválido');
          validationStatus = false;
          continue;
        // OK!
        }else {
          this.turnOffErrors(objId);
          continue;
        }
      }
      /**
       * Validação de CPF
       */
      if(objDataType=='cpf' && $(obj).val().trim()!='' ) {
        if(this.validaCpf($(obj))==false) {
          this.playError(objId, 'CPF inválido');
          validationStatus = false;
          continue;
        // OK!
        }else {
          this.turnOffErrors(objId);
          continue;
        }
      }
      /**
       * Validação de CNPJ
       */
      if(objDataType=='cnpj' && $(obj).val().trim()!=''  ) {
        if(this.validaCnpj($(obj))==false) {
          this.playError(objId, 'CNPJ inválido');
          validationStatus = false;
          continue;
        // OK !
        }else {
          this.turnOffErrors(objId);
          continue;
        }
      }
      /**
       * Validação de IE
       */
      if(objDataType=='ie' && $(obj).val().trim()!='' ) {
        // Pega a UF do list relacionado com o campo IE atraves do atributo sa-listUF(campo ie) e sa-UF(list uf)
        var UF = $('#'+$(obj).attr('az-listUF')).find(':selected').attr('az-UF');
        // Caso a UF esteja vazia
        if( UF=='' || UF==undefined) {
          this.playError(objId, 'Selecione o Estado');
        }else {
          // Verifica se o valor é uma IE válida
          if(!inscricaoEstadual($(obj).val(),UF)) {
            this.playError(objId, 'IE inválida');
            validationStatus = false;
            continue;
          // OK !
          }else {
            this.turnOffErrors(objId);
            continue;
          }
        }
      }
      /*
        Validação de data/data hora
      */
      if(	objDataType.trim().indexOf('date')==0 && $(obj).val().trim()!=''  ) {
        if(this.validaDataHora($(obj))==false) {
          this.playError(objId, 'Data/hora inválida');
          validationStatus = false;
          continue;
        // OK !
        }else {
          this.turnOffErrors(objId);
          continue;
        }
      }
      // Validação de email
      if(	objDataType=='email' && $(obj).val().trim()!='' ) {
        if(this.validEmail($(obj))==false) {
          this.playError(objId, 'Email inválido');
          validationStatus = false;
          continue;
        // OK !
        }else {
          this.turnOffErrors(objId);
          continue;
        }
      }
      /**
       * Validação customizada (pregmatch, ajax, etc..)
       */
      if(objCustom=='false' && objValue!=$(obj).attr('az-default')) {
        this.playError(objId, remoteMsgError);
        validationStatus = false;
        continue;
      // OK!
      }else if(objCustom=='true')  {
        this.turnOffErrors(objId);
        continue;
      }
    }
  }
  // Retorna status de validação do formulario
  return(validationStatus);
}
/**
 * @function      atualizaCaptcha
 * @description   Atualiza a imagem captcha, trocando o código
 * @param         {DOM object} obj : captcha
 */
AzulValidation.prototype.updateCaptcha = function (objCaptcha) {
  $(objCaptcha).attr("src", _SYS_DOMAIN+'az-captcha?' + new Date().getTime());
}


// Cria uma instancia de AzulValidation
//_azValidation = new AzulValidation();
//console.log('file "AzulValidation.class.js" is ready!');
