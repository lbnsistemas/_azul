/**
  @description Principais classes JS do framework Azul
  @author Lucas Nunes <devmaster@lbnsistemas.com.br>
  @version 2.2.3
  @since  22/09/2016
  @update 24/07/2019
*/
_SYS_FRAMEWORK  = '@MVC Azul Framework v2.2.3';
console.log(_SYS_FRAMEWORK);


/**
  @class Azul
  @author Lucas Nunes <devmaster@lbnsistemas.com.br>
  @description Controle geral das bibliotecas do framework
 */
var Azul = function () {
  // Construtor
  // Executa determinadas funções após o carregamento das bibliotecas Azul
  this.afterLoad = [];
};
/**
   Executa todas as funções adicionas no array afterLoad
   @return {void}
 */
Azul.prototype.executeFunctions = function () {
	for(var f in this.afterLoad) {
		if(jQuery.isFunction(this.afterLoad[f])){
			this.afterLoad[f]();
		}	
	}
}
// Cria nova instância da classe geral do framework Azul
_azAzul = new Azul();

// Classe com as principais funções JS
$.getScript(_SYS_FRAMEWORK_URL+"js/AzulGeneral.class.js", function()
{
	console.log('file "AzulGeneral.class.js" is ready!');
	_azGeneral = new AzulGeneral();
	// Classe com funções JS para validação de formulários
	$.getScript(_SYS_FRAMEWORK_URL+"js/AzulValidation.class.js", function()
	{
		console.log('file "AzulValidation.class.js" is ready!');
		_azValidation = new AzulValidation();
		// Classe com funções JS para tratamento de dados
		$.getScript(_SYS_FRAMEWORK_URL+"js/AzulDataManipulation.class.js", function()
		{
			console.log('file "AzulDataManipulation.class.js" is ready!');
			_azDataManipulation = new AzulDataManipulation();
			// Classe com funções JS para envelopamento e envio de pacotes de dados para o webservice
			$.getScript(_SYS_FRAMEWORK_URL+"js/AzulSender.class.js", function()
			{
				console.log('file "AzulSender.class.js" is ready!');
				_azSender7845 = new AzulSender(); // Sufixo '7845', pois para cada thread será chamada uma nova instância, previnindo conflitos
				// Executa todas funções adicionadas no array afterLoad
				_azAzul.executeFunctions();
			});
		});
	});
});
