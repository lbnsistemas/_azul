/**
  @class AzulSender
  @author Lucas Nunes <devmaster@lbnsistemas.com.br>
  @description Objeto para enviar as ações da view e receber o retorno do controller
 */
var AzulSender = function () {
    // Carga valiosa a ser implementada e enviada ao WS
    this.payload = {
        client: _SYS_CLIENT,
        application: _SYS_APPLICATION,
        module: _SYS_MODULE,
        controller: _SYS_CONTROLLER,
        view: _SYS_VIEW,
        action: '',
        url: _SYS_DOMAIN  + _SYS_CLIENT + '/webservice',
        method: 'POST',
        envelope: {
            // dataTypes : date, time, dateTime, number, currency, text, cep, cpf, cnpj, ie, password
            fields: [{ value: '', column: '', dataType: '' }],
            // Pode ser um array (varias colunas) ou string (unica coluna)
            columns: [],
            init: 0,
            limit: 1,
            orderBy: '',
			extra: '',
            // PK da tabela principal ou da tabela do formulario dinamico
            // Pode ser um array (varios IDs selecionados) ou string (unico ID)
            ID: 0,
            // Regras : =, %(like), _B(beetween)
            filters: [{ value1: '', value2: '', columns: [], rule: '=' }],
        },
        envelopeJSON : ''
    };
    // Cópia inicial de payload
    this.payloadInitialCopy = jQuery.extend(true, {}, this.payload);
    // Callback a ser implementado pela View
    this.callback = {};
	// Callback para eventos 'fail' ou 'info' a ser implementado pela View
    this.callbackFail = '';
	this.onError = ''; // Compatibilidade, espelho de callbackFail
};
/**
  Seta as propriedades de pauload.envelope{}
  @return void
*/
AzulSender.prototype.setPayload = function (p) {
    if (p.client)
        this.payload.client = p.client;
    if (p.application)
        this.payload.application = p.application;
    if (p.module)
        this.payload.module = p.module;
    if (p.controller)
        this.payload.controller = p.controller;
    if (p.view)
        this.payload.view = p.view;
    if (p.action)
        this.payload.action = p.action;
	if (p.url)
        this.payload.url = p.url;
};
/**
  Seta as propriedades de pauload.envelope{}
  @return void
*/
AzulSender.prototype.setEnvelope = function (p) {
    if (p.fields)
        this.payload.envelope.fields = p.fields;
    if (p.columns)
        this.payload.envelope.columns = p.columns;
    if (p.ID > Array.isArray(p.ID))
        this.payload.envelope.ID = p.ID;
    if (p.init)
        this.payload.envelope.init = p.init;
    if (p.limit)
        this.payload.envelope.limit = p.limit;
    if (p.orderBy)
        this.payload.envelope.orderBy = p.orderBy;
	if (p.extra)
		this.payload.envelope.extra = p.extra;
    if (Array(p.filters).length > 0)
        this.payload.envelope.filters = p.filters;
};
/**
  Retorna a string de envio para o WS
  @return string
*/
AzulSender.prototype.getDataString = function () {
	/*this.payload.envelopeJSON = JSON.stringify(this.payload.envelope);
	
	var retString = "client="+this.payload.client+"&application="+this.payload.application+"&module="+this.payload.module+
	"&controller="+this.payload.controller+"&view="+this.payload.view+"&action="+this.payload.action+"&envelopeJSON="+
	this.payload.envelopeJSON;
	
	return(retString);*/
	
	
	this.payload.envelopeJSON = JSON.stringify(this.payload.envelope);
	var formData = new FormData();
	formData.append('client', this.payload.client);
	formData.append('application', this.payload.application);
	formData.append('module', this.payload.module);
	formData.append('controller', this.payload.controller);
	formData.append('view', this.payload.view);
	formData.append('action', this.payload.action);
	// Prepara os arquivos para upload junto com os campos do formulario
	for(var i in this.payload.files) {
		var objFile = this.payload.files[i];
		$.each(objFile.files,function(i,file) {
			formData.append(objFile.nameID+i,file);
			self.isUploadForm = true;	
		});
	}
	formData.append('envelopeJSON',this.payload.envelopeJSON);
	return(formData);
}
/**
  Envia payload para: {domain}/webservice (indexphp -> home/{user}/_azul/webservice.php)
  Se receber uma mensagem JSON informando sucesso, executa a function callback()
  definida pelo usuário
  @param silentMode:boolean => se TRUE, faz o enviar sem exibir a div loading ...
  @return void
*/
AzulSender.prototype.send = function (silentMode=false, formData=false) {
  // Proxy para a função callback ser executada pela função ajax do jQuery
  var self = this;
 
  // Utiliza função do Jquery para envio dinamico (AJAX)
  $.ajax({
   type: self.payload.method,
   url:  self.payload.url,
   data: formData!=false ? formData : self.getDataString(),
   contentType: false,
   processData: false,
   cache: false,
   error: function(jqXHR){
      if(jqXHR.status==0) {
          _azGeneral.alert("Erro","Falha de conexão, verifique se você possui acesso a internet.");
      }else {
          _azGeneral.alert("Erro","Falha no envio do dados, verifique sua conexão e tente novamente.");
      }
	  if(silentMode==false) {
	    // Oculta o preloader
	    app.preloader.hide();
	  }
   },
   beforeSend: function(){
     if(silentMode==false) {
       // Mostra GIF loader enquanto estiver processando a solicitação
       bootbox.dialog({
         closeButton: false,
         title: "Processando...",
         message: '<img src="'+_SYS_FRAMEWORK_URL+'images/master/loader.gif"   height="100px"/>'
       });
     }
   },
   success: function(data){
     
     if(silentMode==false) {
       // Fecha todos os dialogs
       bootbox.hideAll();
     }
	
     // Determina se o retorno é uma String JSON válida:
     if(_azGeneral.testJSON(data)!==false) {
         objRetJSON = JSON.parse(data);
		 // Se estiver em modo de debugação salva e imprime o retorno no console
    	 if(_DEBUG_MODE) _azGeneral.debug(objRetJSON);
		 var redirect;
         // Se retornar mensagem de erro/info
         if(objRetJSON.type=='fail' || objRetJSON.type=='info') {
		   if(self.callbackFail!='') {
			  self.callbackFail(objRetJSON.data); 
		   }else if(self.onError!=''){
			  self.onError(objRetJSON.data); 
		   }else {
			   _azGeneral.alert('Ops!', objRetJSON.message, objRetJSON.type,objRetJSON.data.redirect);
			   // Reseta o sa.sender
			   self.reset();
		   }
         // Caso a requisição tenha sido processada com sucesso
         }else if(objRetJSON.type == 'win'){
           // Se houver alguma mensagem de sucesso, exibe um modal
           if(objRetJSON.message.trim()!='') {
             _azGeneral.alert('Parabéns!', objRetJSON.message,'win',objRetJSON.data.redirect);
           }
		   
           // Executa a função callback
          self.callback(objRetJSON.data);
         }
     // Caso não seja uma string JSON válida :
     }else {
		   // Se estiver em modo de debugação salva e imprime o retorno no console
		 if(_DEBUG_MODE) _azGeneral.debug(data);
		 _azGeneral.alert('Desculpe-nos!', 'Algo estranho ocorreu no servidor, informe o suporte técnico.','fail');
		 // Reseta o sa.sender
		 self.reset();
		
     }
   }
  });
};
/**
  Reseta payload com as configurações iniciais
  @return void
*/
AzulSender.prototype.reset = function () {
  this.payload = this.payloadInitialCopy;
};

// Cria nova instância para AzulGeneral
//_azSender7845 = new AzulSender();
//console.log('file "AzulSender.class.js" is ready!');