<?php
/* Azul framework 
 *
 * desenvolvido por Lucas Nunes <lucasnunes@lbnsistemas.com.br>
 * @LBN Sistemas corporation
 *
 * @version 2.3.1
 * @since  22/09/2016
 * @update 30/10/2018
 *
*/	
?>
<script>
<?php
	$q = $view->query;	  	 //Função identica ao jQuery para localizar objectos pelo atributo nameID
	$prop = $view->config(); //Propriedades da View;

	// Instancia de Abas
	$Tabs = $view->config('',false);
	$Tabs = $Tabs['Tabs'];

?>
/**
 *	GLOBAIS DA APLICAÇÃO
 *
 **/
_RESERVED_ID = '<?=$prop->ID?>';							// Primary Key reservada
_IS_PORTABLE = <?=$view->isPortable() ? 'true' : 'false'?>;	// Identificação de dispositivo portátil
_CURRENT_PAGE = 0;											// Identifica a pagina atual
_DEBUG = [];												// Global para debugger das respostas do WS (Web Service)/Controller
_DEBUG_MODE = <?=DEBUG_MODE==true ? 'true' : 'false'?>;		// Modo de apuração/debugger ativado?
_SYS_APPLICATION = "<?=_application?>";						// URL da aplicação
_SYS_CLIENT = "<?=_client?>";								// Nome exclusivo de cliente/usuário
_SYS_MODULE = "<?=_module?>";								// Nome exclusivo de cliente/usuário
_SYS_CONTROLLER = "<?=defined("_controller")==true ? _controller : 'cMain'?>";	// Controller default
_SYS_VIEW = "<?=_view?>";									// View, definida em compiller.php
_SYS_DOMAIN = "<?=SYS_DOMAIN?>";							// Dominio principal
_SYS_FRAMEWORK_URL = "<?=SYS_FRAMEWORK_URL?>";				// URL do framework
_SYS_URL_APP = "<?=SYS_APP_URL?>";							// URL da aplicação
<?php
	// Apenas carrega a listagem se estiver no modo normal
	if($prop->normalMode==true || $prop->listingMode==true) :
?>
	// Array com as colunas visiveis da listagem
	var _azListingColumnsArray = [<?=$view->print_ListingColumnsArray($prop)?>];

	// Objeto com os dados para listagem dinamica
	var _azListingObject =
			{
				generalTotal 	: <?=$prop->generalTotal?>,		//Total de linhas no BD (será útil para acionar ou não a busca dinamica via ajax)
				maxRowsPage		: <?=$prop->maxRowsPage?>,		//Máximo de linhas por pagina
				maxRowsClient	: <?=$prop->maxRowsClient?>,	//Máximo de linhas pré carregadas no client
				dataDefault 	: "",							//Array com linhas default, carregadas na view
				data 			: ""							//Array com as linhas atuais
			}
<?php endif; ?>

// Biblioteca JS do MVC Azul Framework
<?php require(SYS_SRC_FRAMEWORK."js/Azul.js") ?>

/**
 * Abre ou fecha o menu lateral, expandindo o quadro (.content) principal da view
 */
function openCloseLateralMenu() {
	// Se estiver oculto(fechado)
	if($('.sidebar-nav').hasClass('hide')) {
		// Diminui quadro principal
		$('.content').css('margin-left','240px');
		// Mostra Menu
		$('.sidebar-nav').removeClass('hide');
		// Esconde botao abre menu
		$('#btAbreMenuLat').addClass('hide');
	// Se estiver vísivel(aberto)
	}else {
		// Aumento quadro principal
		$('.content').css('margin-left','0');
		// Esconde Menu
		$('.sidebar-nav').addClass('hide');
		// Mostra botao abre menu
		$('#btAbreMenuLat').removeClass('hide');
	}
}

<?php
	/**
	 *	Se for Modo Normal, carrega a listagem e todas as funções para as funções CRUD :
	 *	incluir, visualizar, editar, excluir
	 */
	if($prop->normalMode==true || $prop->listingMode==true ) : ?>
/**
 * Carrega/atualiza as linhas da view via WS
 * @param int linha inicial a ser exibida a partir de 0, string orderBy COLUN + ASC ou DESC
 */
function requestData(init,orderBy) {
	// Se houver uma função do usuário, chama está função
	if(window.user_requestData != undefined) {
		user_requestData(init,orderBy);
		return(false);
	}
	var _azSender7845 = new AzulSender();
	// Fecha janela modal com os filtros de pesquisa
	$('#mdFiltroBusca').modal('hide');
	// Seta a action
	_azSender7845.setPayload({
		action: "requestData"
	});
	// Prepara o envelope
	_azSender7845.setEnvelope(
		{
			init 	: init,
			limit 	: <?=$prop->maxRowsClient?>,
			orderBy : orderBy,
			columns : [<?php $view->print_ViewColumnsArray($prop)?>],
			filters : [<?php $view->print_FiltersObjects($prop)?>]
		}
	);
	// Seta a função pós-processamento com sucesso
	_azSender7845.callback = function(objRet) {
		// Atualiza _azListingObject com as linhas recebidas
		_azListingObject.data 		 = objRet.ret;
		_azListingObject.dataDefault  = objRet.ret;
		showData(0);
		// Reseta sa.sender
		_azSender7845.reset();
	}

	// Envia pacote
	_azSender7845.send(silentMode=true);
}

/**
 * Filtra cadastros utilizando os filtros, com JS ou via AJAX+PHP
 */
function azFilterData() {
	//Filtro genérico principal em minusculo sem espaços (sempre o primeiro filtro do array)
	var wordsFilter = $('#<?=$prop->arFilters[0]->nameID?>').val().toLowerCase().trim().split(" ");

	//Se o total geral for maior que o limite de linhas da view/client (maxRowsClient)
	//Ou se os filtros opcionais foram preenchidos
	//Então realizar a busca no servidor via Ajax + PHP
	if(_azListingObject.generalTotal>_azListingObject.maxRowsClient  <?=$view->print_azFilterData_ArgumentsIF($prop)?>) {
		requestData(0);
	//Caso contrario :
	}else {
		//Se foi digitado algo no filtro, realiza filtragem das linhas via JS
		if(wordsFilter[0]!='') {
			var result = $.grep(_azListingObject.dataDefault, function(element, index) {
				return(<?=$view->print_azFilterData_Return($prop)?>);

			});
		//Mostra as linhas default da view
		}else {
			var result = _azListingObject.dataDefault;
		}

        //Adiciona linhas filtradas ao objeto de listagem da view
        _azListingObject.data = result;

        //Mostra as linhas filtradas
        showData(0);
	}
}

// Monta e executa uma expressao com o array de palavras a serem pesquisadas em uma determinada coluna
// @param object element : {coluna : value}
// @param array palavrasFiltro : ['palavra1', 'palavra2',..] palavras a serem pesquisadas
// @param string regra : regra '%' (verifica se existe em parte da string) ou '=' verifica se é igual a string
function searchWords(element, wordsFilter,rule) {
	addExpr1 = rule=='%' ? '.*' : '^'; // Se for like(%) adiciona .* na expressão regular
	addExpr2 = rule=='%' ? '.*' : '$'; // Se for like(%) adiciona .* na expressão regular
	var contExpre = '';
	for(i in wordsFilter) {
		word = wordsFilter[i];
		contExpre += addExpr1+word+addExpr2;
		// Se ainda não for a ultima palavra, adiciona o operador "|" na expressão regular
		//if(i<palavrasFiltro.length-1) contExpre+= "|";
	}
	var retorno = (element.match(new RegExp(contExpre, "i"))!=null ? true : false);
	return(retorno);
}

/**
 * Ordena as colunas selecionadas, em ordem ASC ou DESC
 */
function orderColumn(objCol,colName) {
	//Salva estado/classe atual da seta de ordenação (caret)
	classCaret = $(objCol).children('span').attr('class');
	//Esconde todas as carets, para mostrar apenas a coluna ordenada
	$('table thead .caret').attr('class','caret hide');
	//Se estiver pra baixo, ao clicar, muda para cima e ordena inversamente (DESC)
	if( classCaret == 'caret') {
		$(objCol).children('span').attr('class','caret caret-up');
		order = '-';
	//Se estiver pra cima ou não existir o icone, mudar/mostra para baixo e ordena por ord. ASC
	}else {
		$(objCol).children('span').attr('class','caret');
		order = '';
	}
	//Reordena o array
	_azListingObject.data.sort(dynamicSort(order+colName));
	//Adiciona as linhas ao _azListingObject
	//_azListingObject.data = arLinhas;
	//Repagina a tabela com as linhas reordenadas
	showData(0);
}
/**
 * Função auxiliar para ordenar pelo indíce do objeto
 * @param string property titulo da coluna a ser ordenada, se iniciar com '-' efetua ordem reversa
 * @return int indice de ordenação
 */
function dynamicSort(property) {
	var sortOrder = 1;
	if(property[0] === "-") {
		sortOrder = -1;
		property = property.substr(1);
	}
	return function (a,b) {
		// tratatamento para coluna numerica
		if($.isNumeric(a[property])==true && $.isNumeric(b[property])==true) {
			fa = parseInt(a[property]);
			fb = parseInt(b[property]);
		}else {
			fa = a[property].toLowerCase();
			fb = b[property].toLowerCase();
		}
		var result = (fa < fb) ? -1 : (fa > fb) ? 1 : 0;
		return result * sortOrder;
	}
}

// Desmarca uma linha
function checkUncheckLine(lineObject) {
	// Caso esteja marcado
	if($(lineObject).hasClass('row_select'))
		$(lineObject).removeClass('row_select');
	// Caso esteja desmarcado
	else
		$(lineObject).addClass('row_select');
	// Verifica se existe alguma linha selecionada
	// Se não houver, trava o botao arquivar selecionados
	if($('#tbData tbody tr').addClass('row_select')==undefined)
		$('#btnSelectedFiles').attr('disabled',true);
}

// Seleciona todas as linhas da listagem
function doSelection() {
	// Seleciona todas linhas (acrescentando a classe 'row_select')
 	$('#tbData tbody tr').addClass('row_select');
	// Destrava botao 'arquivar selecionado(s)'
	$('#btnSelectedFiles').attr('disabled',false);
	// Altera texto do botão  de seleção
	$("#btnSelection").html('Desfazer seleção');
	// Altera função do botão de seleção
	$("#btnSelection").attr('onClick','undoSelection()');
}

// Desseleciona todas as linhas já selecionadas anteriormente
function undoSelection() {
	// Seleciona todas linhas (acrescentando a classe 'row_select')
 	$('#tbData tbody tr').removeClass('row_select');
	// Trava botao 'arquivar selecionado(s)'
	$('#btnSelectedFiles').attr('disabled',true);
	// Altera texto do botão  de seleção
	$("#btnSelection").html('Selecionar todos');
	// Altera função do botão de seleção
	$("#btnSelection").attr('onClick','doSelection()');
}

<?php endif; ?>



// Mostra formulario e esconde listagem, vice-versa
function showHideListing(action='',ID=''){
	$('#ctMain').toggleClass('hide');
	$('#ctListing').toggleClass('hide');
	if(action!='') {
		// Informa a ação (incluir ou alterar)
		$('#infoActionView').html(' - ['+action.toUpperCase()+'] - ID '+ID);
	}else {
		$('#infoActionView').html('');
		// Recarrega a listagem
		requestData(0,"<?=defined("_orderBy") ? _orderBy : ''?>");

	}

}

// Mostra ou oculta formulario dinamico
function showHideDynamicForm(form,obj) {
	if($('#'+form).hasClass('hide')==false) {
		// Oculta o formulario
		$('#'+form).addClass('hide');
		// Altera label, para indicar a próxima ação (+ = abrir)
		var newTitle = $(obj).html().replace('-','+');
		$(obj).html(newTitle);

	}else if($('#'+form).hasClass('hide')) {
		// Coloca o - para formulario aberto
		$('#'+form).removeClass('hide');
		// Altera label, para indicar a próxima ação (+ = abrir)
		var newTitle = $(obj).html().replace('+','-');
		$(obj).html(newTitle);

	}
}

// Reseta Formulario
function resetForms(){
	// Reseta os formularios do pacote principal
	<?php
		// Retorna um array com os ids (html) dos formularios do pacote principal
		$arrayIds = $view->getArrayIds('main_forms',$Tabs);
		// Imprime o mesmo array traduzido para JS
		echo "arrayIds = ['".implode("','",$arrayIds)."'];";
	?>
	for(f in arrayIds) {
		// Limpa os campos dos formularios do pacote principal
		clearFormFields(arrayIds[f]);
	}
	// Reseta os formularios dinamicos
	<?php
		// Retorna um array com os ids (html) dos formularios dinamicos
		$arrayIds = $view->getArrayIds('dynamic_forms',$Tabs);
		// Imprime o mesmo array traduzido para JS
		echo "arrayIds = ['".implode("','",$arrayIds)."'];";
	?>
	for(f in arrayIds) {
		// Limpa os campos dos formularios dinamicos
		clearFormFields(arrayIds[f]);
		// Exclui as linhas duplicadas
		$('#list'+arrayIds[f]).html('');
	}
	// Reseta os quadros de Upload
	<?php
		// Retorna um array com os ids (html) dos quadros de upload
		$arrayIds = $view->getArrayIds('upload_frames',$Tabs);
		// Imprime o mesmo array traduzido para JS
		echo "arrayIds = ['".implode("','",$arrayIds)."'];";
	?>
	for(f in arrayIds) {
		// Exclui os thumbnails dos quadros de upload
		$('#'+arrayIds[f]).html('');
		// Reseta o atributo value do campo(botao) do upload
		var objFieldUpload = arrayIds[f].replace('files','');
		$('#'+objFieldUpload).attr('value','');
		// Reseta o progress bar
		$('#progress'+objFieldUpload+' .progress-bar').css('width','0%');
		$('#progress'+objFieldUpload+' .progress-bar').removeClass("bgVermelho");
		$('#progress'+objFieldUpload+' .progress-bar').html('');
	}
}


// Função auxiliar para limpar os campos  de um det. formulario
function clearFormFields(formulario){
	arCampos = $('#'+formulario).find('.form-group.initial').find('.az-field');
	for(i=0; i < arCampos.length; i++) {
		obj = arCampos[i];
		// Tipo de campo : select, input, text, select2, html, checkbox,..
		var objTipoCampo = $(obj).attr('az-fieldType');

		// Se for um input (text, password) ou textarea
		if( objTipoCampo=='input' ||  objTipoCampo=='text' ) {
			$(obj).val('');

		// Se for um input (checkbox)
		}else if( objTipoCampo=='checkbox' ) {
			// Busca a referencia acima do hidden checkbox, alterando todos os checkbox do grupo
			$(obj).parent().find('input[type="checkbox"]').attr("checked", false);

		// Se for um select
		}else if( objTipoCampo=='select'  || objTipoCampo=='select2' ) {
			$(obj).find('option').prop('selected', false); // Exclui o atributo selected de todos options
			$(obj).find('option').attr('selected', false); // Exclui o atributo selected de todos options
			//$(obj).find(':nth-child(1)').prop('selected', true); // Seleciona o primeiro option
			//$(obj).find(':nth-child(1)').attr('selected', true); // Seleciona o primeiro option

			// Reinicia, se houver, os select2
			$('.az-select2').select2({tags: true});

		// Se for um editor HTML
		}else if( objTipoCampo=='html') {
			$(obj).summernote('code','');

		}
		// Limpa os valores default
		$(obj).attr('az-default','');
		$(obj).attr('az-datatype-default','');
		$(obj).attr('az-title-default','');
	}
}
/**
 * @desc Carrega a lista de dados cadastrados em um determinado formulario dinamico
 * @desc Duplicando a linha de campos, com os devidos values preenchidos
 * @param string formNameID atributo name e ID do formulario : 'form' + [titulo_aba] + [titulo_formulario]
 * @param json object objLinhasRet retorno do webservice com as linhas encontradas
 */
function showDynamicFormDataList(formNameID, objLinhasRet){
	// Se houver linhas a serem exibidas
	if(objLinhasRet!=false && objLinhasRet!=undefined) {
		// Destrói temporariamente todos os select2
		$('.az-select2').select2('destroy');

		// Lê todas linhas encontradas
		objLinhasRet1 = objLinhasRet;
		for(i in objLinhasRet) {
			// Lê e duplica a única* linha do formulario dinamico
			var linhaDup = $($('#'+formNameID).find('.row')[0]).clone();

			// Remove os labels (titulos) de cada campo, ou seja os primeiros labels de cada coluna
			linhaDup.find('label:nth-child(1)').remove();

			// Objeto com todas as colunas encontradas
			objLinhaRet = objLinhasRet[i];

			// Pega a PK (chave primaria) da tabela do formulario dinamico
			var pk_dinform = Object.values(objLinhaRet)[0];

			// Busca todos os grupos/colunas
			var objColunas = linhaDup.find('[class^=col-]');

			// Excluí a classe referenciadora de campo inicial (necessária para reset dos campos iniciais)
			$(objColunas).find('.form-group').removeClass('initial');

			// Exclui o icone informativo
			$(objColunas).find('i').remove();
			var objCampoDup=null;
			for(var c=0;c<objColunas.length;c++) {

				// Seleciona o campo dentro do grupo (form-group)
				objCampoDup = $($(objColunas[c]).find('.form-control'));

				// Se encontrou o campo
				if(objCampoDup.length>0) {
					// Tipo campo : select, input, text, senha, select2, checkbox
					objTipoCampo = objCampoDup.attr('az-fieldType').toLowerCase();
					// Tipo de dado do campo: date, currency, number, phone, email, text
					objTipoDado = objCampoDup.attr('az-datatype').toLowerCase();
					switch(objTipoDado) {
						case 'currency':
							$(objCampoDup).attr("onKeyPress","return(_azDataManipulation.formatCurrency(this,'.',',',event))");
							break;
						default:
							_azDataManipulation.setFormatting(objCampoDup);
							break;
					}
					// Altera ID do campo clonado
					//objCampoDup.attr('id',objCampoDup.attr('id')+i);
					objCampoDup.attr('id',objCampoDup.attr('id')+pk_dinform);
					// Acrescenta a informação de ID do formulario dinamico em cada campo do mesmo
					objCampoDup.attr('az-ID',pk_dinform);
					// Valor default do campo
					objCampoDup.attr('az-default',objLinhaRet[objCampoDup.attr('az-column-db')]);
					// Se for um input (text, password)
					if(objTipoCampo=='input') {
						// Preenche o campo clonado através do relacionamento com a coluna do bd
						objCampoDup.attr('value',objLinhaRet[objCampoDup.attr('az-column-db')]);
						// Valor default do campo
						objCampoDup.attr('az-default',objCampoDup.val());
						// Salva a alteração
						objCampoDup.attr('onBlur',"insertUpdateRegister('"+formNameID+"', true, 'update', "+pk_dinform+")");
					// Se for um input (checkbox)
					}else if( objTipoCampo=='checkbox'  ) {
						// Opção marcada
						var opcoesMarcadas = objLinhaRet[objCampoDup.attr('az-column-db')];

						// Preenche o campo clonado através do relacionamento com a coluna do bd
						objCampoDup.attr('value',opcoesMarcadas);
						// Valor default do campo
						objCampoDup.attr('az-default',objCampoDup.val());
						// Pega todos input checkbox do grupo
						var inputsChk = objCampoDup.parent().find('input[type="checkbox"]');
						// Lista os checkbox encontrados
						for(var ck=0; ck<inputsChk.length; ck++ ) {
							// Se o hidden contiver o value do checkbox, então marca este
							if(opcoesMarcadas.indexOf($(inputsChk[ck]).val())>-1) {
								// Marca cada input checkbox do grupo
								$(inputsChk[ck]).attr('checked',true);
							}
						}
						// Salva a alteração
						objCampoDup.attr('onClick',"insertUpdateRegister('"+formNameID+"', true, 'update', "+pk_dinform+")");

					// Se for um select
					}else if( objTipoCampo=='select' || objTipoCampo=='select2' ) {
						objCampoDup.find('option[value="'+objLinhaRet[objCampoDup.attr('az-column-db')]+'"]').attr('selected','selected');
						// Valor default do campo
						objCampoDup.attr('az-default',objCampoDup.val());
						// Salva a alteração 
						objCampoDup.attr('onChange',"insertUpdateRegister('"+formNameID+"', true, 'update', "+pk_dinform+")");

						
					// Se for um text area
					}else if( objTipoCampo=='text' ) {
						// Preenche o campo clonado através do relacionamento com a coluna do bd
						objCampoDup.text(objLinhaRet[objCampoDup.attr('az-column-db')]);
						// Valor default do campo
						objCampoDup.attr('az-default',objCampoDup.val());
						// Salva a alteração
						objCampoDup.attr('onBlur',"insertUpdateRegister('"+formNameID+"', true, 'update', "+pk_dinform+")");
					}//*/

				// Se não encontrou, provavelmente é a ultima coluna (func.)
				}else {

					// Botão para excluir cadastro dinamico
					htmlBotoes  = '<span class="glyphicon glyphicon-remove handCursor" data-toggle="tooltip" data-placement="top"';
					htmlBotoes += ' title="Excluir cadastro" onclick="archiveRegister(\''+formNameID+'\','+pk_dinform+',$(this).closest(\'.row\'))" style="margin-top:8px"></span>';

					// Botão para concluir alteração no cadastro dinamico
					//htmlBotoes  += '&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-floppy-disk handCursor" data-toggle="tooltip" data-placement="top" title="Salvar alteração" '+
					//'onclick="insertUpdateRegister(\''+formNameID+'\', true, \'update\', '+pk_dinform+');"></span>';

					// Adiciona os botoes
					objBotaoDup = $($(objColunas[c])).html(htmlBotoes);

				}
			}

			// Adiciona a nova linha duplicada com os campos devidamente preenchidos
			$('#list'+formNameID).append("<div class='row'>"+$(linhaDup).html()+"</div>");

			// Inicia novamente todos os  select2
			$('.az-select2').select2({tags: true});

			// Limpa os campos do formulario dinamico, deixando apenas a lista preenchida e salva no BD
			clearFormFields(formNameID);

		}
	}
	// Inicia os tooltips novos (dos icones excluir e salvar de cada linha dinamica)
	$('[data-toggle="tooltip"]').tooltip();
}

/*_____________
||             |
|| =========== >  Funções sincronas dinamicas (Ajax)
||_____________|
*/

/**
 * @desc	Função para rodar testes, necessário implementação da action azTeste em cMain
 */
function azTeste(envelope={},callback=''){
	var _azSender7845 = new AzulSender;
	// Prepara o envelope para a ação preparaContainer
	_azSender7845.setPayload({
		controller:'cMain',
		action: "azTeste"
	});
	// Seta o envelope
	_azSender7845.setEnvelope(envelope);
	// Seta a função pós-processamento com sucesso
	_azSender7845.callback = function(objRet) {
		if(callback!='') callback();
	}
	// Enviar
	_azSender7845.send();
}

/**
 * @desc	Prepara container para novo cadastro / alteração
 * @desc	No caso de inclusão, reserva um ID de cadastro para o usuario logado
 * @desc	também retorna os arquivos de Upload, caso tenham sido enviados anteriormente e não concluído
 *
 */
function prepareContainer(action,ID,objRet=''){
	// Chama a função do usuário (se existir) para preparar o container.
	if(window.user_prepareContainer!=undefined) {
		user_prepareContainer(action, ID,objRet);
		return(true);	
	}
	var _azSender7845 = new AzulSender();
	if(action=='insert') {
		// Padroniza o botao de confirmação
		$('#btnConfirmation').attr('az-action','insert');
		$('#btnConfirmation').html('Concluir cadastro');

		// Bloqueia um possível botao de impressão
		$('button[az-action="print"').attr('disabled',true);

		// Prepara o envelope para a ação preparaContainer
		_azSender7845.setPayload({
			action: "prepareContainer"
		});

		// Seta a função pós-processamento com sucesso
		_azSender7845.callback = function(objRet) {

			 // Se estiver em modo de debugação salva e imprime o retorno no console
			if(_DEBUG_MODE) _azGeneral.debug(objRet);

			// Registra a PK reservada na global
			_RESERVED_ID = objRet.ID;

			<?php
				// Imprimi o conteúdo dinamico da função prepareContainer
				$view->print_prepareContainer_content($Tabs);
			?>

			/* Caso o WS retorne um array quadrosUpload :
			if($.isArray(objRet.uploadFrames)) {

				// Lista todos os quadros de Upload retornados
				for(var i in objRet.uploadFrames) {
					// Seleciona o quadro
					var frame = objRet.uploadFrames[i];
					// Lista todos os arquivos no objeto quadro
					for(var e in frame.files) {
						//Seleciona o arquivo
						var frame = frame.files[e];

						// Monta o html do thumbnail do arquivo com os botoes (download - deletar)
						var html = htmlBoxFileUpload(file,frame.nameID)

						// Mostra thumbnail no quadro Upload
						$('#files'+frame.nameID).append(html);
					}

				}
			}*/

			// Registra o ID reservado no attr az-ID do botão de confirmação
			$('#btnConfirmation').attr('az-ID',_RESERVED_ID);

			// Mostra formulario e esconde listagem
			showHideListing(action,_RESERVED_ID);

			// Reseta sa.sender
			_azSender7845.reset();

		}
		// Envia pacote
		_azSender7845.send();
	}else if(action=='update') {
		// Registra a PK reservada na global
		_RESERVED_ID = ID;

		// Padroniza o botao de confirmação
		$('#btnConfirmation').attr('az-action','update');
		$('#btnConfirmation').html('Concluir alteração');

		// Libera um possível botao de impressão (se existir)
		$('button[az-action="print"]').attr('disabled',false);


		// Mostra formulario e esconde listagem
		showHideListing(action, ID);

		// Registra o ID reservado no attr az-ID do botão de confirmação
		$('#btnConfirmation').attr('az-ID',_RESERVED_ID);
	}

}
/**
 * @desc	Faz uma relação de todos IDs selecionados e envia ao WS
 *
 */
function archiveSelectedRegisters() {
	if(window.user_archiveSelectedRegisters != undefined) {
		user_archiveSelectedRegisters();
		return(false);	
	}
	// Fecha todos os dialogs
	bootbox.hideAll();
	
	var _azSender7845 = new AzulSender;
	// Confirmação
	bootbox.confirm("Tem certeza que deseja arquivar este(s) cadastro(s)?", function(result) {
	  if(result) {
		    var arrIDs = [];
			var ID = '';
		  	// Monta um array com todos os ID dos cadastros selecionados (attr. sa-ID)
			var selectedLines = $('#tbData tbody tr.row_select');
			for (var i=0; i < selectedLines.length; i++) {
				var selLine = selectedLines[i];
				ID = $(selLine).attr('az-ID');
				arrIDs.push(ID);
			}
			// Seta a ação para arquivar o cadastro
			_azSender7845.setPayload({
				action: "archive_Main"
			});
			// Prepara o envelope
			_azSender7845.setEnvelope( {  ID : arrIDs } );
			// Seta a função pós-processamento com sucesso
			_azSender7845.callback = function(objRet) {
			    // Se estiver em modo de debugação salva e imprime o retorno no console
				if(_DEBUG_MODE) _azGeneral.debug(objRet);
				// Apaga todas linhas selecionadas e arquivadas
				 $('#tbData tbody tr.row_select').remove();
				// Reseta sa.sender
				_azSender7845.reset();
			}
			// Envia pacote
			_azSender7845.send();
	  }else {
		   return(-1);
	  }
	});
}

/**
 * @desc	Efetua logout(sai) do sistema
 *
 */
function doLogout() {
	bootbox.confirm("Tem certeza que deseja sair do sistema?", function(result) {
	  if(result) {
		  	var _azSender7845 = new AzulSender;
		  	// Seta o controller para : cPrincipal
			_azSender7845.setPayload({
				action: "doLogout",
				controller: "cMain"
			});
			// Seta a função pós-processamento com sucesso
			_azSender7845.callback = function(objRet) {
				// Se estiver em modo de debugação salva e imprime o retorno no console
				if(_DEBUG_MODE) _azGeneral.debug(objRet);
				// Redireciona para a URL em objRet.redirect
				if(_azGeneral.isEmpty(objRet.redirect)==false) {
					window.location=_objRet.redirect;

				// Atualiza a tela para que o arquivo index.php na raíz direciona para a tela de login
				}else {
					window.location.reload();
				}

			}
			// Envia pacote
			_azSender7845.send();

        }
    });
}
/**
 * @desc 	Busca endereço por CEP (consulta WS de terceiros)
 * @param 	DOM object objCEP : campo de cep
 * @param 	string formulario : formulario de carregamento do endereço (default : Principal)
 * @param	string campoFocus : Id do campo para receber focus após o carregamento do endereço
 */
function findAddressByCEP(objCEP, form,fieldFocus="") {
	var cep = objCEP.value;
	var cepDefault = $(objCEP).attr('az-default');
	if(cep.trim().length!=10 || cep==undefined  || cep == cepDefault) return(false);

	var _azSender7845 = new AzulSender;
	
	// Seta o controller para : cPrincipal
	_azSender7845.setPayload({
		controller:'cMain',
		action:'findAddressByCEP'
	});

	// Prepara o envelope
	_azSender7845.setEnvelope({
		fields:[{column:'cep',value:cep}]
	});

	// Seta a função pós-processamento com sucesso
	_azSender7845.callback = function(objRet) {
		// Se estiver em modo de debugação salva e imprime o retorno no console
		if(_DEBUG_MODE) _azGeneral.debug(objRet);

		var codIBGE_cidade = objRet.ibge;
		var codIBGE_estado = objRet.ibge.substr(0,2);
		var codGIA = objRet.gia;
		// Formulario Outros Endereços
		if(form=='PrincipalOutrosEndereços') {
			var endereco = objRet.logradouro+", "+objRet.bairro+", "+objRet.localidade+", "+objRet.uf;
			// Altera o campo destino com as informações do endereço encontrado
			$('#txtendereco_end').attr('az-codIBGE',codIBGE_cidade);
			$('#txtendereco_end').attr('az-codGIA',codGIA);
			$('#txtendereco_end').val(endereco);
		// Principal
		}else {
			// Preenche campo logradouro
			$('#txtlogradouro').val(objRet.logradouro);
			// Preenche campo bairro
			$('#txtbairro').val(objRet.bairro);
			// Seleciona o estado correspondente
			$('#listestado').find('option[sa-codibge="'+codIBGE_estado+'"]').attr('selected',true);
			// Tenta selecionar a cidade na lista, caso não encontre adc. novo option
			if(!$('#listcidade').find('option[sa-codibge="'+codIBGE_cidade+'"]').attr('selected',true).length) {
				var option = '<option value="'+codIBGE_cidade+'" az-codIBGE="'+codIBGE_cidade+'" az-codGIA="'+codGIA+'">'+objRet.localidade+'</option>';
				$('#listcidade').html(option);
			}
		}
		// Altera o atributo default do campo, para bloquear nova consulta desnecessária
		$(objCEP).attr('az-default',cep);

		// Reseta sa.sender
		_azSender7845.reset();

		// Passa o focus para o campo (caso tenha sido informado)
		if(fieldFocus!='') {
			$('#'+fieldFocus).focus();
		}
	};

	// Envia pacote
	_azSender7845.send();
}


/**
 *	@desc	Preencher todos os formularios da view de forma dinamica
 *
 */
function fillOutForms(ID, bt=false){
	if(window.user_fillOutForms != undefined) {
		user_fillOutForms(ID, bt);	
		return(false);
	}
	// Trava botao
	if(bt!=false) 
		$(bt).attr('disabled',true);
	// Instancia AzulSender
	var _azSender7845 = new AzulSender;
	// Preapra os os cabeçalhos de 'payload':
	_azSender7845.setPayload({
		action:'formsData'
	});
	// Prepara o envelope

	_azSender7845.setEnvelope( {
		filters : [{columns:['<?=defined("_columnID") ? _columnID : 'ID'?>'],value1:ID,rule:'='}],
		ID		: ID
	});
	// Seta a função pós-processamento com sucesso
	_azSender7845.callback = function(objRet) {
	    // Se estiver em modo de debugação salva e imprime o retorno no console
		if(_DEBUG_MODE) _azGeneral.debug(objRet);
		
		<?php
			// Imprimi os campos para preenchimento do formulario
			$view->print_fillOutForms_content($Tabs);
		?>
		
		// Trava botao
		if(bt!=false) 
			$(bt).attr('disabled',false);

		// Prepara container
		prepareContainer("update",ID, objRet);
		// Reseta sa.sender
		_azSender7845.reset();
	}
	// Envia pacote
	_azSender7845.send();
}

/**
 * @desc	Incluir ou alterar um cadastro via WS
 * @param 	string formulario : 'form' + titulo_aba + titulo_formulario, bool dinamico, string acao, int ID
 */
function insertUpdateRegister(form, dynamic, action, ID) {
	// Função do usuário para inserir ou editar o cadastro
	if(window.user_insertUpdateRegister != undefined) {
		user_insertUpdateRegister(form, dynamic, action, ID);
		return(false);
	}
	var fields ='';
	// Valida o(s) formulario(s) antes de enviar
	if(!_azValidation.exec(form, dynamic, ID)) {
		_azGeneral.alert('Atenção','Falta algum campo obrigatório para ser preenchido (verifique as abas também)');
		return(false);
	}

	// Para o formulario principal, a ação e ID estará no proprio botao
	action = 	action == '' ? $('#btnConfirmation').attr('az-action') 	: action;// Pode ser ação do pacote principal ou do formulario dinamico
	ID 	 = 	ID 	 == '' ? $('#btnConfirmation').attr('az-ID') 	: ID;// PK da tabela principal ou do formulario dinamico

	/*
	=====> Linhas comentadas por Lucas Nunes : 09/06/2017
	// Campos dos formularios dinamicos
	<?php
		$campos = $view->getAllDynamicForms($Tabs->tabs);
		foreach($view->getAllDynamicForms($Tabs->tabs) as $tabForm) :
			$tab = $tabForm['tab'];
			$form = $tabForm['form'];
	?>
	// Ação incluir cadastro (formularios dinamicos) :
	// Pega todos os campos do formulario e envia ao WS
    if("<?=$form->nameID?>" == form && action=="insert") {
		// Pega todas os campos do formulario
		var fields = [<?=$view->print_FieldsObjects($form->getFields())?>];
	}
	<?php
		endforeach;
	?>
	// Ação alterar cadastro :
	// O WS só irá processar os campos editados, salvando no histórico
	// o valor anterior e o atual, com nome de usuário e data da alteração
	if(action=="update") {
		// Pega os campos que sofreram alterações
		var fields = _azGeneral.getUpdatedFields(form, dynamic, ID);

	// Ação incluir cadastro (pacote principal) :
	// Pega todos os campos do formulario e envia ao WS
	}else if("Main" == form && action=="insert") {
		// Objeto ID principal
		objID = {column: "<?=defined("_columnID") ? _columnID : 'ID'?>", value:_RESERVED_ID};

		// Pega todas os campos dos formularios do pacote principal
		var fields = [objID,<?=$view->print_FieldsObjects($view->getAllMainFormsFields($Tabs->tabs))?>];
	}
	=======> fim
	*/

	// Pega os campos que sofreram alterações
	if(action=="update") {
		var fields = _azGeneral.getUpdatedFields(form, dynamic, ID);
	}else {
		var fields = _azGeneral.getUpdatedFields(form, dynamic);
	}

	// Erro, nenhum campo encontrado para alteração/inclusão
	if(fields=='') {
		if(_DEBUG_MODE) _azGeneral.debug('Form : '+form);
		if(_DEBUG_MODE) _azGeneral.debug('Dynamic : '+dynamic);
		if(_DEBUG_MODE) _azGeneral.debug('ID : ' + ID);
		if(_DEBUG_MODE) _azGeneral.debug(fields);
		_azGeneral.alert('Ops !','Nenhum campo foi editado. ','falha');
		return(false);
	}
	var _azSender7845 = new AzulSender;
	
	// Seta a ação
	_azSender7845.setPayload({
		action:  action+'_'+form
	});
	// Prepara o envelope
	_azSender7845.setEnvelope( {ID : ID, fields : fields } );
	// Seta a função pós-processamento com sucesso
	_azSender7845.callback = function(objRet) {
	    // Se estiver em modo de debugação salva e imprime o retorno no console
		if(_DEBUG_MODE) _azGeneral.debug(objRet);
		// Pega a ID criada (ação incluir)
		var novoID = objRet.ret.ID;

		// Se for formulario dinamico, adiciona linha dos dados recem incluidos
		if(dynamic==true && action=='insert'){
			<?php $view->print_insertUpdateRegister_content($Tabs);?>

		// Se for modo normal e estiver incluindo ou editando o formulario principal : fecha o container e traz a lista
		// Se for modo de edição apenas ou se for uma edição de uma formulario dinamico : permanece no mesmo container
		}else if(dynamic==false) {
			<?php
			// Se for Modo de edição, não mostra a listagem, apenas um cadastro especifico
			if($prop->editMode==false) : ?>
				// Reseta os formularios
				resetForms();

				// Fecha formularios de cadastro e exibe listagem
				showHideListing();

				// Reseta global de reserva de PK para cadastro
				_RESERVED_ID = '';
			<?php endif; ?>
		}

		// Reseta sa.sender
		_azSender7845.reset();

	}
	// Envia pacote
	_azSender7845.send();

}
/**
 * @desc	Deletar um cadastro do pacote principal ou de uma formulario dinamico via WS
 * @param 	string formulario [sulfixo do formulario dinamico(aba_titulo+formulario_titulo) ou 'Principal']
 */
function archiveRegister(form, ID, objToRemove) {
	if(window.user_archiveRegister != undefined) {
		user_archiveRegister(form, ID, objToRemove);
		return(false);	
	}
	// Confirmação
	bootbox.confirm("Tem certeza que deseja arquivar este cadastro?", function(result) {
	  if(result) {
		  	var _azSender7845 = new AzulSender;
		  	// Seta a ação em Payload
			_azSender7845.setPayload({
				action:'archive_'+form
			});
			// Prepara o envelope
			_azSender7845.setEnvelope( { ID : ID } );
			// Seta a função pós-processamento com sucesso
			_azSender7845.callback = function(objRet) {
			   // Se estiver em modo de debugação salva e imprime o retorno no console
				if(_DEBUG_MODE) _azGeneral.debug(objRet);

				// Apaga linha/coluna do cadastro deletado
				$(objToRemove).remove();
				// Reseta sa.sender
				_azSender7845.reset();
			}
			// Envia pacote
			_azSender7845.send();
	  }else {
		   return(-1);
	  }
	});
}

<?php

	$uploadFrames = ($Tabs instanceof Tabs? $Tabs->getAllUploadsFrames() : array());
	// Verifica se existe quadro(s) de upload de arquivos na view
	if(count($uploadFrames)>0) :
?>
/**
 * @desc	Deletar um cadastro do pacote principal ou de uma formulario dinamico via WS
 * @param 	string fileName : nome completo do arquivo com extensao
 * @param 	string qdUploadID : ID do quadro de Upload
 * @param 	DOM Object idUpload : coluna a ser apagado após a exclusao do arquivo
 */
function deleteFile(fileName, qdUploadID, objToRemove) {
	// Confirmação
	bootbox.confirm("Tem certeza que deseja deletar este arquivo?", function(result) {
	  if(result) {
		  	var _azSender7845 = new AzulSender;
		  	// Seta a ação em Payload
			_azSender7845.setPayload({
				action:'deleteFile'
			});
			// Prepara o envelope
			_azSender7845.setEnvelope({
				fields:
				[
					{column:'fileName', value:fileName},
					{column:'qdUploadID', value:qdUploadID},
				],
				ID:_RESERVED_ID
			});
			// Seta a função pós-processamento com sucesso
			_azSender7845.callback = function(objRet) {
			    // Se estiver em modo de debugação salva e imprime o retorno no console
				if(_DEBUG_MODE) _azGeneral.debug(objRet);
				// Apaga coluna do arquivo deletado
				$(objToRemove).remove();

				// Reseta o value do campo upload
				$('#'+qdUploadID).attr('value','');

				// Reseta sa.sender
				_azSender7845.reset();
			}
			// Envia pacote
			_azSender7845.send();
	  }else {
		   return(-1);
	  }
	});
}

/**
 * Carrega os arquivos de um determinado quadro de Upload
 * @param objUploadFramesContent:array -> [{nameID, files},...]
 * @param json object objFiles retorno do webservice com os arquivos
 */
function showUploadFramesContent(objUploadFramesContent){
	// Se houver arquivos a serem exibidos
	if($.isArray(objUploadFramesContent)) {
		// Abre cada posição do array, para exibir os arquivos no frames de upload corretos
		for(var i in objUploadFramesContent) {
			var frameUpload = objUploadFramesContent[i];

			// Lê todos os arquivos no array do corrente uploadFrame
			for(var f in frameUpload.files) {
				var objFile = frameUpload.files[f];
				// Nome completo do arquivo
				var fileName = objFile.name+'.'+objFile.extension;

				// Cria o html para exibição do arquivo com os botões 'deletar' e 'download'
				var html = htmlBoxFileUpload(objFile,frameUpload.nameID);

				// Mostra thumbnail no quadro Upload
				$('#files'+frameUpload.nameID).append(html);
			}

			// Altera value do campo input file, para envio ao WS junto com o pacote principal
			$('#'+frameUpload.nameID).attr('value',fileName);
		}
	}
}
// Função complementar para criar o html do quadro do arquivo no formulario de upload
function htmlBoxFileUpload(objFile,qdUploadID){
	// Resume o nome do arquivo
	var nomeResumido = (objFile.name.length>19 ? objFile.name.substr(0,18)+".." : objFile.name)+'.'+objFile.extension;

	// Filename completo
	var fileName = objFile.name+'.'+objFile.extension;

	// HTML thumbnail/icone do arquivo
	var html   =  '<div class="col-xs-6 col-md-3 col-lg-3 handCursor" style="padding: 20px" fileName="'+fileName+'" qdUploadID="'+qdUploadID+'">';
	html  +=  '<img src="'+objFile.thumbnail+'" width=160/><br />';
	html  +=  nomeResumido;
	html  +=  '<br /><button class="btn btn-warning" onClick="deleteFile(\''+fileName+'\', \''+qdUploadID+'\', $(this).parent())">Deletar</button>';
	html  +=  ' <button class="btn btn-info" onClick="downloadFile(\''+fileName+'\', \''+qdUploadID+'\')">Download</button>';
	html  +=  '</div>';
	// Retorna o html criado
	return(html);
}


/**
 *  @desc Solicita ação para download de arquivo na mesma view
 *	@param string fileName : nome do arquivo com extensao a ser baixado
 *	@param string qdUploadID : ID do quadro de Upload do arquivo a ser baixado
 *
 */
function downloadFile(fileName,qdUploadID) {
	var _azSender7845 = new AzulSender;
	// Seta a ação em Payload
	_azSender7845.setPayload({
		action:'downloadFile'
	});
	// Prepara o envelope
	_azSender7845.setEnvelope({
		fields:
		[
			{column:'fileName', value:fileName},
			{column:'qdUploadID', value:qdUploadID},
		],
		ID:_RESERVED_ID
	});
	// Seta a função pós-processamento com sucesso
	_azSender7845.callback = function(objRet) {
	    // Se estiver em modo de debugação salva e imprime o retorno no console
		if(_DEBUG_MODE) _azGeneral.debug(objRet);

		// Url do arquivo downloader, que retornará o arquivo propriamente dito
		var urlDOwnloader = _SYS_FRAMEWORK_URL+'downloader.php';

		// Abri a janela de download do navegador já com o arquivo em stream
		var iframe = $("<iframe/>").attr({
								id: 'iFrameDownloader',
								src: urlDOwnloader,
								style: "visibility:hidden;display:none"
							}).appendTo('body');

		// Reseta sa.sender
		_azSender7845.reset();
	}
	// Envia pacote
	_azSender7845.send();
}
/**
  @description carrega o arquivo e executa uma função callback se houver
*/
function loadFile(input, callback='') {
  var curFiles = input.files;
  if(curFiles.length === 0) {
    _azGeneral.alert('Nenhum arquivo foi selecionado, tente novamente.');
	$(input).val('');
	return(false);
  } else {
    for(var i = 0; i < curFiles.length; i++) {
      if(validExtension(curFiles[i])) {
		filereader = new FileReader();
		filereader.readAsDataURL(curFiles[i]);
		if(_DEBUG_MODE) console.log(filereader);
        filereader.onload = function (filereader, curFiles) {
			// Função callback
			if(callback!='') callback(filereader, curFiles);
        };
      } else {
        _azGeneral.alert('Extensão de arquivo não permitida "'+curFiles[i].name+'".');
		$(input).val('');
		return(false);
      }
    }
  }
}
/**
  @description valida a extensao do arquivo carregado, proibindo algumas extensões 
*/
function validExtension(file) {
  var forbidden = [
	  'php',
	  'c',
	  'htaccess',
	  'ini',
	  'asp',
	  'h',
  ];
  for(var i = 0; i < forbidden.length; i++) {
	var re = new RegExp('\.'+forbidden[i]+'$');
    if(re.exec(file.name)!=null) {
      return false;
    }
  }
  return true;
}
<?php
		/**
		 *	Upload de arquivos
		 *	Cria uma função para cada quadro de upload encontrado
		 *
		 */
		 foreach ($uploadFrames as $qUp) :

?>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = _SYS_FRAMEWORK_URL+'webservice';

	// Inicial objeto fileupload
    $('#<?=$qUp->nameID?>').fileupload({
        url: url,
        dataType: 'json',
		maxFileSize: <?=($qUp->maxSizeMB*100000)?>,
		acceptFileTypes: /(\.|\/)(<?=$qUp->allowedExtensions?>)$/i,
		add: function(e,data) {
		    // Atualiza formData
			$('#progress<?=$qUp->nameID?> .progress-bar').css('width','0%');
			$('#progress<?=$qUp->nameID?> .progress-bar').removeClass("bgVermelho");
			$('#progress<?=$qUp->nameID?> .progress-bar').html('Enviando...');
			data.submit();
	    },
		// Executar após processamento com sucesso
        done: function (e, data) {
			// Resposta do webservice
			var objRet = data.result;

			// Se estiver em modo de debugação salva e imprime o retorno no console
			if(_DEBUG_MODE) _azGeneral.debug(objRet);

			// Verifica se processou o lote com sucesso ou houve falha no processamento
			if(objRet.type=='fail') {
				$('#progress<?=$qUp->nameID?> .progress-bar').addClass("bgVermelho");
				$('#progress<?=$qUp->nameID?> .progress-bar').html(objRet.message);
			//  Sucesso no envio e processamento dos arquivos
			}else if(objRet.type=='win') {
				$('#progress<?=$qUp->nameID?> .progress-bar').html('Concluído!');
				// Exibe o status de cada arquivo processado (pode ter sido rejeitado ou não)
				var erros='';
				$.each(objRet.data.files, function (index, file) {

					// Nome completo do arquivo
					var fileName = file.name+'.'+file.extension;

					// Arquivo enviado com sucesso!
					if(file.status==true) {

						// Altera value do campo input file, para envio ao WS junto com o pacote principal
						$('#<?=$qUp->nameID?>').attr('value',fileName);

						// Cria o html para exibição do arquivo com os botões 'deletar' e 'download'
						var html = htmlBoxFileUpload(file,'<?=$qUp->nameID?>');

						// Escreve o html com o retorno do processamento
						$('#files<?=$qUp->nameID?>').append(html);

					// Falha no processamento do envio
					}else {
						if(file.name.trim()!='') {
						  	erros  +=  ' "'+fileName+'" - '+file.status+'<br />';
						}else {
							erros  +=  file.status+'<br />';
						}
					}
				});
				// Caso exista, abre uma janela contendo todas as falhas de processamento dos arquivos
				_azGeneral.alert('Ops! Alguns arquivos não foram aceitos :',erros,'fail');
			}

        },
		// Exibir barra de progresso
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress<?=$qUp->nameID?> .progress-bar').css('width',progress + '%');
        },
		// Erro no upload ou no retorno do webservice
		error : function (e, data) {
			$('#progress<?=$qUp->nameID?> .progress-bar').addClass("bgVermelho");
			$('#progress<?=$qUp->nameID?> .progress-bar').html('Erro no processamento dos arquivos, contate o suporte técnico.');
		}

    }).prop('disabled', !$.support.fileInput)
		.parent().addClass($.support.fileInput ? undefined : 'disabled');

	// Função para atualizar os dados do formulario de upload
	$('#<?=$qUp->nameID?>').bind('fileuploadsubmit', function (e, data) {
		 data.formData = {
		    application	: _SYS_APPLICATION,
			client		: _SYS_CLIENT,
			module		: _SYS_MODULE,
			action		: 'receiveFile',
			controller	: _SYS_CONTROLLER,
			view		: _SYS_VIEW,
			envelopeJSON	: JSON.stringify({ID : _RESERVED_ID , qdUploadID : '<?=$qUp->nameID?>'})
		}
	});

});//GLobal function
<?php
	endforeach; // qdUploads
	endif;	// count () > 0
 ?>


/**
 * @desc	Copia conteúdo do menu lateral para dentro  do nav-bar #main-menu
 *
 */
$(function() {
	var uls = $('.sidebar-nav > ul > *').clone();
	uls.addClass('visible-xs');
	$('#main-menu').append(uls.clone());
});


// Funções após tudo estar carregado :
$(document).ready(function() {
	// Adiciona estas funções para serem executadas após carregamento do Framework Azul
	_azAzul.afterLoad.push(function() {
	<?php
	// Apenas se for modo Normal, carrega a listagem
	if(($prop->normalMode==true || $prop->listingMode==true) && count($Tabs)>0) : ?>

		// Traz as linhas iniciais da view (listagem de cadastros)
		requestData(0, '<?=(defined('_orderBy') ? _orderBy : '')?>');
		// Inicia o validador para os formularios do pacote principal
		_azValidation.init('Main');

		// Função para funcionamento das abas bootstrap
		$(".nav-tabs a").click(function(){
			$(this).tab('show');
		});
	<?php
	// Se foi informado um ID inicial e está no modo de edição, preenche os formularios
	elseif($prop->ID>0 && $prop->editMode==true) : ?>
		// Abre o único cadastro para edição
		fillOutForms(<?=$prop->ID?>);

		// Inicia o validador para os formularios do pacote principal
		_azValidation.init('Main');

		// Função para funcionamento das abas bootstrap
		$(".nav-tabs a").click(function(){
			$(this).tab('show');
		});

	<?php endif;?>
	// Inicia, se houver, todos os campos para edição html (div)
	<?php if($Tabs instanceof Tabs) : ?>
	<?php foreach($Tabs->getAllFieldsFromType('div') as $field) :?>
		$('#<?=$field->nameID?>').summernote({
			height:150,
			 toolbar: [
				['style', ['style']],
				['text', ['bold', 'italic', 'underline', 'color', 'clear']],
				['para', ['ul', 'ol', 'paragraph']],
				['height', ['height']],
				['size', ['fontsize']],
				['font', ['fontname']],
				['misc', ['fullscreen','codeview','undo','redo','help']],
				['insert', ['picture','link','video','table','hr']]
			]
		});
	<?php endforeach; ?>
	// Inicia, se houver, todos os objetos checkbox
	<?php foreach($Tabs->getAllFieldsFromType('checkbox') as $field) :?>
	_azGeneral.initCheckbox('<?=$field->nameID?>');
	<?php endforeach; ?>

	// Inicia, se houver,  todos os select2 incluídos na views
	<?php
		foreach($Tabs->getAllFieldsFromType('select2') as $field) :
			// Se foi informada a função formatState nas propriedades do campo :
			if($field->templateResult!='') {
				echo '
					$("#'.$field->nameID.'").select2({
						templateResult	: '.$field->templateResult.'
					});';
			// Inicialização normal
			}else {
				echo '$("#'.$field->nameID.'").select2({tags: true});
				';
			}
		endforeach;
	?>
	<?php endif; ?>


	// Adiciona função de pontuação nos campos monetários, se houver
	/*$('input[az-dataType="currency"]').keypress(function(e) {
		return(_azDataManipulation.formatCurrency(this,".",",",e));
	});*/

	// Inicia todas as tooltips (pop-up informativo)
	$('[data-toggle="tooltip"]').tooltip();

	});// azAzul.addFunctions()

});
</script>
