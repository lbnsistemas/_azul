<?php
// Incluí a classe Form : reponsavel pela criação de elementos dinamicos HTML para os formularios
require_once("View.classes.php");

/**
 * Class View : Preparar e criar o html, javascript, css da View
 * Extende a classe Form que possuí todos objetos necessários para montagem de formulários

 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 5.5.0
 */
class View extends Construtor{
	// Propriedades/Configurações da View
	private $properties = array();

	/**
	 * Parametros padrão da view
	 * @construct
	 */
	public function __construct() {
		// Verifica se ainda não foi definida o nome da view
		//if(!defined(_view)) define(_view, substr($_GET['template'],0,strpos($_GET['template'],'.')));

		// Propriedades padrão para a maioria das Views
		$this->properties = array(
				"normalMode"=>true,		//Modo padrão para operações CRUD (listagem, inclusão, exclusão, alteração)
				"editMode"=>false,		//Modo para edição de um único cadastro
				"listingMode"=>false,	//Modo para exibição de listagens/relatórios
				"ID"=>0,				//ID default para ser carregado os dados no modoEdicao
				"generalTotal"=>0,		//Total de linhas no BD (será útil para acionar ou não a busca dinamica via ajax)
				"maxRowsClient"=>250, 	//Limite de linhas carregas na view
				"maxRowsPage"=>50,		//Limite de linhas visualizadas por página
				"arFindColumnsOff"=>array(),	//Array com as colunas de bsuca off-line (apenas p/ modoNormal)
				"arFilters"=>array(),			//Array com a def. dos filtros de pesquisa (apenas p/ modoNormal)
				"arListingHead"=>array(),		//Array com a def. dos cabeçalhos de listagem (apenas p/ modoNormal)
				"Tabs"=>new StdClass(),				//Abas object : todas as abas/formularios/campos do sistema
		) ;

		// Se estiver no MODO_DEBUG, realiza a validação do XML do template
		//if(DEBUG_MODE) $this->validateXML();

	}
	/**
	 * Seta propriedades da View ou retorna as propriedades atuais
	 * @param array $config : array com as propriedades
	 * @return void | object stdClass : propriedades
	 */
	public function config($config='',$convertToStd=true){
		// Verifica se é um array como esperado
		if(is_array($config)) {
			// Altera apenas as chaves informadas e existentes
			foreach($config as $k=>$v) {
				if(isset($this->properties[$k])) {
					$this->properties[$k] = $v;
				}else die('Propriedade "'.$k.'" não reconhecida na View.');
			}
		// Se não for um array, então retorna as propriedades atuais
		}else {
			// Objeto stdClass : $p->{propriedade}
			if($convertToStd)
				return($this->arrayToStd($this->properties));
			else
				return($this->properties);
		}
	}
	/**
	 * Retorna a URL para o arquivo thumbnail se este existir
	 * @param string $src_file caminho para a imagem original (considerar a partir do diretorio publico "public_html")
	 * @return false ou string com a URL do thumbnail
	 **/
	public function getThumbnailURL($src_file) {
		//Direrio para o thumbnail
		$src_thumbnail = SYS_SRC_ROOT.pathinfo($src_file, PATHINFO_DIRNAME)."/".
						 pathinfo($src_file, PATHINFO_FILENAME)."-thumbnail.".
						 pathinfo($src_file, PATHINFO_EXTENSION);
		//Verifica se existe thumbnail para a imagem
		if(is_file($src_thumbnail)){
			//URL para o thumbnail
			$src_thumbnail = str_replace(SYS_SRC_ROOT,SYS_DOMAIN,$src_thumbnail);
		}else{
			$src_thumbnail=false;
		}
		return($src_thumbnail);
	}
	/**
	 * Valida o XML do template da View
	 * @return boolean or erros
	 **/
	public function validateXML($src_xds='') {
		if($src_xds=='') $src_xds=SYS_SRC_FRAMEWORK.'etc/schemas/azul_forms.xsd';
		$xml = new DOMDocument();
		$xml->load(SYS_SRC_APP.'views/'._view.'/'._view.'.xml');
		$errs = [ ];
		set_error_handler ( function ($severity, $message, $file, $line) use (&$errs) {
			$errs [] = $message;
		} );
		$validated = $xml->schemaValidate ( $src_xds );
		restore_error_handler ();
		if (!$validated) {
			var_dump($errs);
			print '<h3>XML do template ('._view.'.xml) inv&aacute;lido!</h3>';
			exit();
		}else return(true);
	}
	/**
	 * Incorpora de forma dinamica todos as abas/formularios/campos/listagem/filtros da View
	 * através do arquivo xml especifico da view na pasta /templates
	 * @params bool $toValidXML : indica se haverá validação prévia do xml
	 * @params string $src_xsd : caminho real do xsd para validação
	 * @return void()
	 **/
	public function incorporateXML($toValidXML=false,$src_xsd='') {
		// Se for informado argumento de validação igual a true : valida o xml antes de incorporar
		if($toValidXML) $this->validateXML($src_xsd);

		// Caminho para o arquivo xml da View
		$srcFile = SYS_SRC_APP.'views/'._view.'/'._view.".xml";

		// Abre o arquivo para manipulação do dados
		$dom = new DOMDocument;
		$dom->loadXML(file_get_contents($srcFile));

		// Seleciona o node "view"
		$domView = $dom->getElementsByTagName('view');

		# FORMULARIOS :

		// Seleciona o node "principal"
		$domMain = $domView->item(0)->getElementsByTagName('main');
		// Seleciona os nodes "aba"
		$domTabs = $domMain->item(0)->getElementsByTagName('tab');

		// Instancia as Abas
		$Tabs = new Tabs();

		foreach($domTabs as $domTab) {
			// Cria Aba
			//$Aba = Construtor::criaAba($domAba->getAttribute("titulo"));
			$Tab = $Tabs->makeTab($domTab->getAttribute("title"));

			// Seleciona os nodes "formulario"
			$domForms = $domTab->getElementsByTagName("form");
			foreach($domForms as $domForm) {
				$title = $domForm->getAttribute("title");
				$table = $domForm->getAttribute("table");
				$dynamic = $domForm->getAttribute("dynamic")=='true';
				$open = $domForm->getAttribute("open")=='true';
				$info = $domForm->getAttribute("info");
				// Cria Formulario
				$Form = $Tab->makeForm($title,$table, $dynamic, $open,$info);

				// Seleciona os nodes "linha"
				$domLines = $domForm->getElementsByTagName("line");
				foreach($domLines as $domLine) {
					// Cria Linha
					$Line = $Form->makeLine();

					// Seleciona os nodes "coluna"
					$domColumns = $domLine->getElementsByTagName("column");
					foreach($domColumns as $domColumn) {
						// Cria Coluna
						$col_xs = $domColumn->getAttribute("col-xs");
						$col_md = $domColumn->getAttribute("col-md");
						$col_lg = $domColumn->getAttribute("col-lg");
						$Column = $Line->makeColumn($col_xs, $col_md, $col_lg);

						// Seleciona os nodes "campo"
						$domFields = $domColumn->getElementsByTagName("field");
						foreach($domFields as $domField) {
							$arConfig = array();
							// Pega todos atributos do campo
							foreach($domField->attributes as $index=>$attr) {
								// Trata atributos string para boolean para os seguintes names :
								if($attr->name=='required' || $attr->name=='hidden' || $attr->name=='disabled') {
									$attr->value = $attr->value=='true';
								}

								$arConfig = array_merge($arConfig,array($attr->name => $attr->value));
							}
							// Verifica se existe opções no campo select
							$domOptions = $domField->getElementsByTagName("option");
							$varOptions = array();
							foreach($domOptions as $domOption) {
								$attributes = new StdClass();
								foreach($domOption->attributes as $attrName => $domAttr) {
									$attributes->$attrName = $domAttr->value;
								}
								$optionText = $domOption->nodeValue;
								// Salva os atributos e o texto do atual nó 'option'
								$varOptions[] = array('attributes'=>$attributes, 'text'=>$optionText);

							}

							// Incrementa o array de opções (apenas para select)
							$arConfig = array_merge($arConfig,array("options"=>$varOptions));


							// Cria stdClass : Campo
							$Column->makeField($arConfig);
						}
					}
				}

				// Selecona os nodes "quadroUpload"
				$domUploadFrames = $domForm->getElementsByTagName("uploadFrame");
				foreach($domUploadFrames as $domUploadFrame) {
					// Cria stdClass : quadroUpload
					$destination = $domUploadFrame->getAttribute('destination');
					$nameID = $domUploadFrame->getAttribute('nameID');
					$column = $domUploadFrame->getAttribute('column');
					$allowedExtensions = $domUploadFrame->getAttribute('allowedExtensions');
					$maxAmount = $domUploadFrame->getAttribute('maxAmount');
					$maxSizeMB = $domUploadFrame->getAttribute('maxSizeMB');
					$Form->makeUploadFrame($destination,$nameID,$column, $allowedExtensions,$maxAmount,$maxSizeMB);
				}
			}
		}
		// Seta a configurações para a definição dos filtros
		$this->config(array("Tabs"=>$Tabs));

		# LISTAGEM :

		// Seleciona o node "listagem"
		$domListing = $domView->item(0)->getElementsByTagName('listing');
		// Caso exista uma listagem (se for modo de edição não é obrigatória)
		if( $domListing->length > 0 ) :

		// Cabeçalhos da listagem
		// Seleciona os nodes "cabecalho"
		$domHeads =  $domListing->item(0)->getElementsByTagName('head');
		$arListingHead = array();
		foreach($domHeads as $domHead) {
			$title = $domHead->getAttribute("title");
			$column = $domHead->getAttribute("column");
			$isPortable = $domHead->getAttribute("isPortable")=='true';
			array_push($arListingHead,array('title'=>$title,'column'=>$column,'isPortable'=>$isPortable));
		}
		// Seta a configuração para a definição dos cabeçalhos de listagem
		$this->config(array("arListingHead"=>$arListingHead));

		// Filtros de pesquisa
		// Seleciona os nodes "filtro"
		$domFilters =  $domListing->item(0)->getElementsByTagName('filter');
		$arFilters = array();
		foreach($domFilters as $domFilter) {
			$nameID = $domFilter->getAttribute("nameID");
			$dataType = $domFilter->getAttribute("dataType");
			$fieldType = $domFilter->getAttribute("fieldType");
			$title = $domFilter->getAttribute("title");
			$columns  = $domFilter->getAttribute("columns");
			$rule = $domFilter->getAttribute("rule");

			// Verifica se existe opções no campo select
			$domOptions = $domFilter->getElementsByTagName("option");
			$varOptions = array();
			foreach($domOptions as $domOption) {
				$attributes = new StdClass();
				foreach($domOption->attributes as $attrName => $domAttr) {
					$attributes->$attrName = $domAttr->value;
				}
				$optionText = $domOption->nodeValue;
				// Salva os atributos e o texto do atual nó 'option'
				$varOptions[] = array('attributes'=>$attributes, 'text'=>$optionText);

			}

			// Se for data, dataHora ou moeda, cria campo adicional (dois campos/ intervalo)
			if($dataType=='date' || $dataType=='dateTime' ||  $dataType=='currency') {
				array_push($arFilters,array(
						array('nameID'=>$nameID,'dataType'=>$dataType,'fieldType'=>$fieldType,
					'title'=>$title,'columns'=>$columns,'rule'=>$rule),
						array('nameID'=>$nameID."1",'dataType'=>$dataType."1")
					)
				);

			// Demais filtros (unicos)
			}else {
				array_push($arFilters,array('nameID'=>$nameID,'dataType'=>$dataType,'fieldType'=>$fieldType,
				'title'=>$title,'columns'=>$columns,'rule'=>$rule, 'options'=>$varOptions));
			}

		}
		// Seta a configuraçõe para a definição dos filtros
		$this->config(array("arFilters"=>$arFilters));

		// Colunas para busca off-line
		// Seleciona os nodes "colunaOff"
		$domColumnsOff =  $domListing->item(0)->getElementsByTagName('columnOff');
		$arFindColumnsOff = array();
		foreach($domColumnsOff as $domColumnOff) {
			$column  = $domColumnOff->getAttribute("column");
			$rule = $domColumnOff->getAttribute("rule");
			array_push($arFindColumnsOff,array('column'=>$column,'rule'=>$rule));
		}
		// Seta a configuraçõe para a definição dos filtros
		$this->config(array("arFindColumnsOff"=>$arFindColumnsOff));

		// Fecha if listagem
		endif;
	}

	/**
	 * Limpa uma string de caracteres especiais para criar o atributo nameID da aba e do formulario
	 * Elimina caracteres com acentos e demais caracteres que não sejam alfabeto puro ou números
	 * @param $string : string a ser limpa, string $espacos : caracter para substituir espaços
	 * @return string sem espaços e sem caracteres especiais
	 **/
	public function clearString($string,$spaces="") {
		if($string!=''){
			$string = preg_replace('/\s+/',$spaces,preg_replace('/[^a-zA-Z0-9 \-]+/', '',$string));
		}
		return($string);
	}

	/**
	 * Substituí caracteres acentuados por alfabeto puro
	 * @param $string : string a ser tratada, retirando os acentos
	 * @return string alfabeto puro
	 **/
	public function takeAccents($string){
		$trocarIsso = array('à','á','â','ã','ä','å','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ü','ú','ÿ',
		'À','Á','Â','Ã','Ä','Å','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','O','Ù','Ü','Ú','Ÿ');
		$porIsso = array('a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','y','A',
		'A','A','A','A','A','C','E','E','E','E','I','I','I','I','N','O','O','O','O','O','O','U','U','U','Y');
		$titletext = str_replace($trocarIsso, $porIsso, $string);
		return($titletext);
	}

	/**
	 * Retorna o texto puro de uma string html
	 * @param $html : html
	 * @return string texto puro
	 **/
	public function htmlToText($html)
    {
        return html_entity_decode(
            trim(strip_tags(preg_replace('/<(head|title|style|script)[^>]*>.*?<\/\\1>/si', '', $html))),
            ENT_QUOTES,'utf-8'
        );
    }



	/**
	 *	Prepara uma string para compor um link SEO
	 *  (otimização para os sites de pesquisa)
	 *	@param string $texto : texto a ser padronizado
	 *	@return string : texto padronizado para efeito se otimização (google)
	 */
	public function prepareSEOText($text) {
		if(trim($text)!='') {
			$text = $this->clearString($this->takeAccents($text),"-");
		}
		return($text);
	}

	/**
	 * Resume um texto a x caracteres, se o texto for maior que o limite
	 * @param string $textoSimples : texto simples
	 * @param int $limite : numero máximo de caracteres
	 * @return  texto resumido + '...' ou texto
	 */
	public function resumeText($simpleText, $limit){
		if(mb_strlen($simpleText,'utf8') > $limit) {
			return(substr($simpleText, 0,$limit).'...');
		}else {
			return($simpleText);
		}
	}

	/**
	 * Cria uma imagem captcha e armazena a palavra secreta numa sessão
	 * @param $largura,$altura,$tamanho_fonte,$quantidade_letras
	 * @return image jpg
	 */
	public function captcha($largura,$altura,$tamanho_fonte,$quantidade_letras){
		header("Content-type: image/jpeg"); // Define o cabeçalho de retorno (imagem/jpg)
		$imagem = imagecreate($largura,$altura); // define a largura e a altura da imagem
		$fonte = SIS_SRC_APP."fonts/verdana.ttf"; //voce deve ter essa ou outra fonte de sua preferencia em sua pasta
		$preto  = imagecolorallocate($imagem,0,0,0); // define a cor preta
		$branco = imagecolorallocate($imagem,255,255,255); // define a cor branca

		// define a palavra conforme a quantidade de letras definidas no parametro $quantidade_letras
		$palavra = substr(str_shuffle("AaBbCcDdEeFfGgHhIiJjKkLlMmNnPpQqRrSsTtUuVvYyXxWwZz23456789"),0,($quantidade_letras));
		// atribui para a sessao a palavra gerada
		$_SESSION["captcha"] = $palavra;
		for($i = 1; $i <= $quantidade_letras; $i++){
			// atribui as letras a imagem
			imagettftext($imagem,$tamanho_fonte,rand(-25,25),($tamanho_fonte*$i),($tamanho_fonte + 10),$branco,$fonte,substr($palavra,($i-1),1));
		}
		imagejpeg($imagem); // gera a imagem
		imagedestroy($imagem); // limpa a imagem da memoria
	}

	/**
	 * Retorna um objeto stdClass de um array
	 * @param array $array : Array a ser convertido em object stdClass
	 * @return object stdClass
	 */
	public function arrayToStd($array){
		return(json_decode(json_encode($array)));
	}

	/**
     * Função para identificar dispositivos portateis
	 */
	public function isPortable() {
		$iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$ipad = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
		$android = strpos($_SERVER['HTTP_USER_AGENT'],"Android");
		$palmpre = strpos($_SERVER['HTTP_USER_AGENT'],"webOS");
		$berry = strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
		$ipod = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$symbian =  strpos($_SERVER['HTTP_USER_AGENT'],"Symbian");
		return ($iphone || $ipad || $android || $palmpre || $ipod || $berry || $symbian == true);
	}

	//FUNCAO PARA IMPRIMIR ALERT E REDIRECINAMENTO
	public function printAlert($msg,$redir) {
		if ($msg!="") {
			echo "<script>alert('$msg')</script>";
			if (trim($redir)!='') {
				echo "<script>window.location='$redir'</script>";
			}
		}
	}

	//FUNCAO PARA IMPRIMIR REDIRECIONAMENTO
	public function printLocation($url) {
		echo "<script>window.location='$url'</script>";
	}

	//IMPRIMI OS CABEÇALHOS EM HTML
	public function printHead($config,$index=-1) {
		//IMPRIMI TODOS HEADS
		if ($index==-1) {
			foreach($config as $key => $value) {
				if (strpos($value,".js")!==false) {
					echo "<!-- ".$key." -->";
					echo "<script type='text/javascript' src='".$value."' ></script>\r\n";
				}else if (strpos($value,".css")!==false) {
					echo "<!-- ".$key." -->";
					echo "<link rel='stylesheet' type='text/css' href='".$value."' />\r\n";
				}
			}
		//IMPRIMI APENAS O INDICE INFORMADO
		}else {
			if (strpos($config[$index],".js")!==false) {
				echo "<!-- ".$index." -->";
				echo "<script type='text/javascript' src='".$config[$index]."' ></script>\r\n";
			}else if (strpos($config[$index],".css")!==false) {
				echo "<!-- ".$index." -->";
				echo "<link rel='stylesheet' type='text/css' href='".$config[$index]."' />\r\n";
			}
		}
	}

	// RETORNAR URL VIRTUAL - URL AMIGAVEL
	// @param @queryString: adiciona a var. na URL se está já não estiver inclusa
	public function virtualURL($varURL='') {
		$protocolo    = (strpos(strtolower($_SERVER['HTTPS']),'https') === 'on') ? 'http' : 'https';
		$host         = $_SERVER['HTTP_HOST'];
		$script       = $_SERVER['REQUEST_URI'];
		$redirect 	  = $_SERVER['REDIRECT_URL'];
		$query		  = $_GET;
		//Tratamento de variaveis de URL
		if($varURL!='') {
			$posEq = strpos($varURL,'=');
			$varName  = ($posEq!==false ? substr($varURL,0,$posEq) : $varURL);
			$varName  = str_replace(array('?','&'),'',$varName);
			$varValue = ($posEq>0 ? substr($varURL,$posEq+1,strlen($varURL)) : '');
			// replace parameter(s)
			$query[$varName] = $varValue;
		}
		// rebuild url
		$query_result = http_build_query($query);
		$url = $protocolo . '://' . $host . $redirect . ($query_result!='' ? '?' . $query_result : '');
		return $url;
	}

	// RETORNA URL REAL
	public function realURL() {
		$protocolo    = (strpos(strtolower($_SERVER['SERVER_PROTOCOL']),'https') === false) ? 'http' : 'https';
		$host         = $_SERVER['HTTP_HOST'];
		$script       = $_SERVER['SCRIPT_NAME'];
		$parametros   = $_SERVER['QUERY_STRING'];
		$url     = $protocolo . '://' . $host . $script . '?' . $parametros;
		return $url;
	}

	//CONVERTE PARA MAISCULA
	public function strToUpper($term) {
		$palavra = strtr(strtoupper($term),"àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ","ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß");
		return $palavra;

	}

	//CONVERT PARA MINUSCULA
	public function strToLower($term) {
		$palavra = strtr(strtolower($term),"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß","àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ");
		return $palavra;
	}

	//CONVERTE VALOR NUMERICO POR EXTENSO
	public function valorPorExtenso($valor=0) {
		$singular = array("centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão");
		$plural = array("centavos", "reais", "mil", "milhões", "bilhões", "trilhões","quatrilhões");

		$c = array("", "cem", "duzentos", "trezentos", "quatrocentos","quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos");
		$d = array("", "dez", "vinte", "trinta", "quarenta", "cinquenta","sessenta", "setenta", "oitenta", "noventa");
		$d10 = array("dez", "onze", "doze", "treze", "quatorze", "quinze","dezesseis", "dezesete", "dezoito", "dezenove");
		$u = array("", "um", "dois", "três", "quatro", "cinco", "seis","sete", "oito", "nove");

		$z=0;

		$valor = number_format($valor, 2, ".", ".");
		$inteiro = explode(".", $valor);
		for($i=0;$i<count($inteiro);$i++)
			for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
				$inteiro[$i] = "0".$inteiro[$i];

		// $fim identifica onde que deve se dar junção de centenas por "e" ou por "," ;)
		$fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
		for ($i=0;$i<count($inteiro);$i++) {
			$valor = $inteiro[$i];
			$rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
			$rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
			$ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

			$r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd && $ru) ? " e " : "").$ru;
			$t = count($inteiro)-1-$i;
			$r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
			if ($valor == "000")$z++; elseif ($z > 0) $z--;
			if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t];
			if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
		}

		return($rt ? $rt : "zero");
	}

}


?>
