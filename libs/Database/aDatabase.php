<?php
/**
 * Irá manipular informações no banco de Dados, de forma rápida e segura
 * Todo alteração na base de dados será registrada no log : php/logs/{cli_id}/mysql.txt
 * Todas as assinaturas de funções mysql (select, update, delete, transaction ...) estão na interface iMysqlFunctions
 * 
 * @author    Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2015, Lucas Nunes
 * @version   1.0.0
 */ 
abstract class aDatabase { 
	/**
	 * Seleciona todas ou algumas colunas de uma determina tabela
	 * 
	 * @param  string $table nome da tabela, string $compl complemente da query string (ex.: 'WHERE COL = 1 LIMIT 30')
	 * @param string $cols nome das colunas a serem selecionadas (ex.: 'COL1,COL2')
	 * @return array (ex.: array(linha1,linha2,...))
	 * @since  version 1.0.0
	 */
	abstract protected function select($table,$compl='',$cols='*');
	/**
	 * Deleta uma linha de uma determinada tabela, registra log
	 * 
	 * @param string $table, string $col_id nome da coluna PK, int $id PK
	 * @return bool true (sucess) | (false) failed
	 * @since version 1.0.0
	 */
	abstract protected function delete($table, $col_id, $id);
	/**
	 * Deleta uma linha de uma determinada tabela, registra log
	 * 
	 * @param  string $table, array @dados (ex.: array(col1=value,col3=value,...)),
	 * @param  string $col_id nome da coluna PK, int/array $id PK, bool $writeLog true : registra no log
	 * @return bool true (sucess) | (false) failed
	 * @since  version 1.0.0
	 */
	abstract protected function update($table,$dados, $col_id, $id,$writeLog=true);
	/**
	 * Inseri uma linha em uma determinada tabela, registra log
	 * 
	 * @param  string $table, array @dados (ex.: array(col1=value,col3=value,...)), bool $writeLog true : registra no log
	 * @return bool true (sucess) | (false) failed
	 * @since  version 1.0.0
	 */
	abstract protected function insert($table,$dados,$writeLog=true);
	/**
	 * Executa uma série de Mysql Strings, registra log
	 * 
	 * @param  array $sqlStrings (ex.: array('update table set x=1','update ...')),  bool $writeLog true : registra no log
	 * @return bool true (sucess) | (false) failed
	 * @since  version 1.0.0
	 */
	abstract protected function transaction($sqlStrings,$writeLog=true);
	/**
	 * Abre uma conexão com o host, selecionando uma database que será utilizada 
	 * como padrão para todas demais consultas, obs: charset UTF8
	 *
	 * @return bool true (sucess) | (false) failed
	 * @since version 1.0.0
	 */ 
    abstract protected function connect();
	/**
	 * Fecha conexão com o banco de dados
	 * 
	 * @return bool true (sucess) | (false) failed
	 * @since version 1.0.0
	 */ 
	abstract protected function disconnect();
	/**
	 * Registra alguma mudança do BD no log do mysql, configurado em 
	 * config.php -> constant SIS_FILE_LOG_ERROR_MYSQL
	 *
	 * @param string $string query sql executada ou com erro  
	 * @param bool $erro true (log de erro mysql) | false (log do mysql)
	 * @return bool true (sucess) | (false) failed
	 * @since version 1.0.0
	 */
	abstract protected function writeLog($string,$erro=false);
	/**
	 * Devolve uma instancia iniciada de mysqli
	 * 
	 * @return mysqli object
	 * @since version 1.0.0
	 */ 
	abstract protected function getInstanceMysqli();
	/**
	 * Retorna o número de linhas afetadas
	 * 
	 * @return int numero de linhas
	 * @since version 1.1.0
	 */ 
	abstract protected function rows();
	/**
	 * Retorna o último ID gerado automaticamente na ultima consulta(insert, update)
	 * 
	 * @return int ultimo ID
	 * @since version 1.1.0
	 */ 
	abstract protected function id();
	
	
	
	
	
}




?>
