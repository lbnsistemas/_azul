<?php
/**
 * Classe Upload - recepção de arquivos enviados pelo cliente
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 2.2.0
 */
class Upload extends File  {
	//ARQUIVO(S) DO UPLOAD - > $_FILES[]
	private $files;
	//TAMANHO MAXIMO PARA UPLOAD
	private $maxSizeBytes=2048000;//PADRAO 2 Mb
	//QUANTIDADE MAXIMA PASTA
	private $maxAmount=10;//PADRAO 10 arquivos
	//CAMINHO PARA DESTINO DO UPLOAD
	private $destination;
	//EXTENSOES PERMITIDAS
	private $allowedExtensions="";
	//EXTENSOES PROIBIDAS
	private $forbiddenExtensions="";
	//CRIA OU NÃO UM THUMBNAIL DAS IMAGENS (GIF, JPG E PNG)
	private $thumbnail;
	//ALTERA NOME DO ARQUIVO
	private $updateFileName;
	//URL DOS DA PASTA DESTINO
	private $urlFiles;
	//NOVA EXTENSÃO, CASO A PROP. updateFileName == TRUE
	private $newExtension;

	/**
	 * CONSTRUTOR
	 * @params stdClass $envelope : {destino, [arquivos], tamanhoMaximo, alteraNome, [extensoesPermitidas], quantidadeMaxima}
	 */
	public  function __construct($envelope) {

		// Verifica se existe a pasta e já cria a mesma
		if(File::makeDir($envelope->destination)!==false) $this->destination=$envelope->destination;

		//VERIFICA ARQUIVO DO FORMULARIO
		if (!is_array($envelope->files)) {
			trigger_error('Erro, $envelope->files vazio ou não é um array.', E_USER_NOTICE);
			exit();

		// SALVA O ARRAY DE ARQUIVO(S) NA PROPRIEDADE DA CLASSE
		}else {
			$this->files = $envelope->files;
		}



		//EXTENSOES PERMITIDAS
		if(isset($envelope->allowedExtensions)) $this->allowedExtensions = $envelope->allowedExtensions;
		
		//EXTENSOES PROIBIDAS
		if(isset($envelope->forbiddenExtensions)) $this->forbiddenExtensions = $envelope->forbiddenExtensions;


		//CONVERTE MB EM Bytes (Default é 2 MB)
		if($envelope->maxSizeMB>0) $this->maxSizeBytes = File::convertBytes($envelope->maxSizeMB.'MB','B');


		//QUANTIDADE MAXIMA DE ARQUIVOS POR PASTA
		if($envelope->maxAmount>0) $this->maxAmount = $envelope->maxAmount;

		//ALTERA OU NAO O NOME ORIGINAI DO(S) ARQUIVO(S) DO UPLOAD
		$this->updateFileName = $envelope->updateFileName;

		//NOVA EXTENSÃO PARA ARQUIVOS COM NOMES ALTERADOS (SE ESTIVER EM BRANCO, MANTEM EXTENSAO ATUAL)
		$this->newExtension = $envelope->newExtension;

		//NORMALIZA O ARRAY DE ARQUIVOS
		$this->normalizeFilesArray();

		//CRIA OU NÃO UM THUMBNAIL DOS ARQUIVOS DE IMAGENS(GIF, JPG, PNG)
		$this->thumbnail = $envelope->thumbnail;

		//URL DOS ARQUIVOS
		$this->urlFiles = $envelope->urlFiles;

	}

	// VERIFICA QUANTIDADE MAXIMO DE ARQUIVOS NA PASTA DESTINO
	public function checkAmount(){
		$ret = false;
		$cont = 0;
		$dh =(is_dir($this->destination)? opendir($this->destination) : false);

		if($dh!=false) {
			// BUSCA TODOS ARQUIVOS
			while (false !== ($filename = readdir($dh))) {

				//verifica se é um arquivo valido
				if ($filename!="." && $filename!=".." && strpos($filename,'-thumbnail.')===false & is_dir($this->destination.$filename)==false) {
					$cont++;
				}
			}
			// VERIFICA SE A QUANTIDADE ATUAL + [arquivos] NÃO ULTRAPASSA O LIMITE DA PASTA
			if(($cont + count($this->files)) <= $this->maxAmount)
			$ret = true;
		}
		return($ret);
	}

	// VERIFICA TAMANHO DO ARQUIVO
	private function checkSize($fileSizeBytes) {
		if ($fileSizeBytes > $this->maxSizeBytes) {
			return false;
		}else {
			return true;
		}
	}

	// ALTERA NOME DO ARQUIVO A SER MOVIDO
	private function updateFileName($fileName) {
		$files = count(scandir($this->destination))-1;
		$ext = $this->newExtension!='' ? $this->newExtension : $this->getExtArquivo($fileName);
		// NOME PADRAO: DATA - UPLOAD + [0-INF.] + EXT
		$fileName = date("Y-m-d h-i-s").'-upload'.$files.".".$ext;
		return($fileName);
	}

	//VERIFICA SE A EXTENSAO DO ARQUIVO ENVIADO É PERMITIDA PARA UPLOAD
	private function checkExtension($file) {
		$fileName = strtolower($file['name']);
		$mime_type = $file['type'];
		$ext = $this->getFileExtension($fileName);
		// Arquivo sem extensão
		if($ext!='') {
			// Verifica se é uma extensão permitida
			if($this->allowedExtensions!='') 
				$permitida = preg_match("/\.(".$this->allowedExtensions."){1}$/i", $fileName);
			else $permitida = true;
			// Verifica se é uma extensão proibida
			$proibida =  preg_match("/\.(".$this->forbiddenExtensions."){1}$/i", $fileName);
			return(($permitida==true && $proibida==false));

		// Teoricamente um arquivo sem extensão não será executado pelo apache
		}else return(true);
	}

	//NORMALIZA O(S) ARQUIVO(S) EM UM ARRAY
	private function normalizeFilesArray() {
		$arr = array();
		//var_dump($this->files);
		// Verifica se está com a lógica invertida (indices e dados dos arquivos)
		if(is_array($this->files['name']) ) {
			foreach ($this->files['name'] as $index => $filename) {
				$arr[] = array(
					'name' => $filename,
					'tmp_name' => $this->files['tmp_name'][$index],
					'error' => $this->files['error'][$index],
					'size' => $this->files['size'][$index],
					'type' => $this->files['type'][$index]
				);
			}

		// Envio via aplicativo:
		}else {
			$arr[] = array(
					'name' => $this->files['name'],
					'tmp_name' => $this->files['tmp_name'],
					'error' => $this->files['error'],
					'size' => $this->files['size'],
					'type' => $this->files['type']
				);
		}
		$this->files = $arr;

	}

	//PEGA A EXTENSAO DO ARQUIVO
	private function getFileExtension ($fileName) {
		$ext =  pathinfo($fileName, PATHINFO_EXTENSION );
		return($ext);
	}

	//PROCESSA OS ARQUIVOS SEGUINDO AS REGRAS SETADAS NO INICIO DA CLASSE
	public function processFiles() {
		//Array de retorno : [['name', 'extension', 'status','thumbnail'], ... ]
		$arRet = array();

		// Se a quantidade enviada for maior que o limite por pasta, rejeita o lote
		if(!$this->checkAmount()) {
			array_push($arRet,array(
			'name'=>'',
			'extension'=>'' ,
			'status'=>'Quantidade máxima('.$this->maxAmount.') atingida.'));

		// Espaço liberado
		}else {
			// Salva arquivo por arquivo desde que atenda os críterios de envio
			foreach($this->files as $file) {
				$file['name'] = strtolower($file['name']);
				//ALTERA NOME DO ARQUIVO
				if($this->updateFileName) $file['name'] = $this->updateFileName($file['name']);

				//VERIFICA PERMISSAO DE EXTENSAO
				$extension = strtolower($this->getFileExtension($file['name']));

				if (!$this->checkExtension($file)) {
					array_push($arRet,array(
					'name'=>$file['name'],
					'extension'=>$extension ,
					'status_upload'=> false,
					'status'=>'Extensão "'.$extension .'" não permitida.'));
					continue;
				}

				//VERIFICA TAMANHO
				if (!$this->checkSize($file['size'])) {
					array_push($arRet,array(
					'name'=>$file['name'],
					'extension'=>$extension,
					'status_upload'=> false,
					'status'=>'Ultrapassou o limite de  '.File::convertBytes($this->maxSizeBytes.'B','MB').'MB. Tamanho do arquivo enviado : '.File::convertBytes($file['size'].'B','MB').'MB'));
					continue;
				}

				//VERIFICA SE O ARQUIVO JÁ EXISTE
				if (file_exists($this->destination.$file['name'])) {
					array_push($arRet,array(
					'name'=>$file['name'],
					'extension'=>$extension,
					'filename'=>$file['name'].'.'.$extension,
					'status_upload'=> false,
					'status'=>'Arquivo já existe.'));
					continue;
				}



				#
				# AQUI TENTA SALVAR O ARQUIVO DO UPLOAD
				#

				$retUpload = move_uploaded_file($file['tmp_name'], $this->destination.$file['name']);
				#
				# AQUI CRIA UM THUMBNAIL, CASO SEJA GIF/JPG/PNG
				#
				if($retUpload==true && ($extension=='gif' || $extension=='jpg' || $extension=='jpeg' || $extension=='png') && $this->thumbnail==true){
					$img = new Image($file['name'],$this->destination);
					// Tenta criar um thumbnail da imagem
					$res = $img->createthumbnail(500,500,'gd2');
					if($res) $thumbnail = $this->urlFiles.$img->getNewFileName();

				// Se for imagens, exibi a msm como thumbnail (não recomendado devido ao peso do arquivo)
				}else if($extension=='gif' || $extension=='jpg' || $extension=='jpeg' || $extension=='png') {
					$thumbnail = $this->urlFiles.$file['name'];

				// Se for qualquer outro arquivo, exibe um icone apenas
				}else {
					$thumbnail = File::getURLIcon($extension);
				}
				$urlFile = $thumbnail = $this->urlFiles.$file['name'];
				
				
				// Salva o resultado do processamento no array arRet
				array_push($arRet,array(
					'name'=>str_replace('.'.$extension,"",$file['name']),
					'extension'=>$extension,
					'filename'=>$file['name'].'.'.$extension,
					'thumbnail'=>$thumbnail,
					'url'=>$urlFile,
					'status_upload'=> true,
					'status'=>$retUpload));
			}
		}

		// Retorna o array com status de cada arquivo processado
		return(array('files'=>$arRet));
	}
}
?>
