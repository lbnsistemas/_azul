<?php
/**
 * Classe Image - Funções para edição e criação de imagens
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 1.1.0
 */
class Image extends File  {
	// Extensao da imagem
	private $extension;
	// Nome da imagem original
	private $fileName;
	// Nome da imagem original sem extensao
	private $fileNameWithoutExtension;
	// Novo nome da imagem editado
	private $newFileName;
	// Pasta de destino imagens editadas
	private $destination;
	// Pasta de origem das imagens originais
	private $source;

	/**
	 * Construtor
	 * @params string $nomeArquivo : caminho da imagem original,
	 * string $origem : caminho absoluto da imagem original
	 * string $destino : caminho absoluto das imagens editadas
	 */
	public  function __construct($fileName, $source, $destination='') {
		//Salva a extensao do arquivo
		$this->extension = pathinfo($fileName, PATHINFO_EXTENSION );
		// Extensões permitidas (gif, jp?g e png)
		if(in_array(strtolower($this->extension),array('gif','jpg','jpeg', 'png'))===false) {
			trigger_error("Extensão '".$this->extension."' não suportada.", E_USER_NOTICE);
			exit();
		}
		//Salva o nome do arquivo
		$this->fileName = $fileName;
		//Salva o nome do arquivo sem a extensao
		$this->fileNameWithoutExtension = pathinfo($fileName, PATHINFO_FILENAME );
		// Verifica se a imagem original existe na pasta informada
		if(file_exists($source.$fileName)==false) {
			trigger_error("Imagem original não encontrada.", E_USER_NOTICE);
			exit();
		}
		// Pasta de origem
		$this->source = $source;
		// Pasta de destino (se não for informada, será a mesma da origem)
		$this->destination = $destination!='' && file_exists($destination) ? $destination : $source;

	}

	/**
	 * Retorna o novo nome do arquivo
	 **/
	public function getNewFileName() {
		return($this->newFileName);
	}

	/**
	 * Esta é a função que se encarregará de criar as imagens menores (thumbnails)
	 * $new_w: Esta variável conterá o valor de configuração para a largura da imagem menor;
	 * $new_h: Está variável conterá o valor de configuração para a altura da imagem menor;
	 * $compress: tipo de compressão, 'gd1' ou ''
	 * @return true (sucesso) ou false(falha)
	 **/
	public function createthumbnail($new_w, $new_h, $compress) {
		/*Nas três linhas que seguem, são feitas algumas verificações acerca do tipo de arquivo de imagem.
		*Para cada verificação bem sucedida, é criado um novo arquivo de imagem a partir da imagem original.
		*a função imagecreatefrom*() retorna um identificador de imagem representando a imagem obtida através do nome de *arquivo dado.
		**/

		// Altera o diretorio de trabalho
		File::setWorkDir($this->destination);

		//Cria um novo nome para a imagem thumbnail
		$newName = File::newFileName($this->fileNameWithoutExtension."-thumbnail",$this->extension);
		$this->newFileName = $newName;
		$thumbfile = $this->destination.$newName;
		$origfile = $this->source.$this->fileName;

		switch(strtolower($this->extension)) {
			case  'gif' :
				$origimage = imagecreatefromgif($origfile);
				break;
			case 'jpg' : case 'jpeg' :
				$origimage = imagecreatefromjpeg($origfile);
				break;
			case 'png' :
				$origimage = imagecreatefrompng($origfile);
				break;

		}


		/*Nestas duas linhas que seguem, note que são obtidas largura e altura da imagem com as funções
		*imagesx() e imagesy() respectivamente. para mais informações sobre as funções utilizadas neste
		*script, acesse www.php.net
		*/
		$old_x = imagesx($origimage);
		$old_y = imagesy($origimage);

		/*verifica se a largura e altura obtidas anteriormente são maiores
		*que a largura e a altura estabelecidas estabelecidas para as imagens menores
		*que serão criadas.
		*Em seguida, estabelece as novas dimensões para a imagem original. veja o uso
		*da função round() na terceira linha do laço if():
		*$thumb_w = round(($old_x * $new_h) / $old_y).
		*multiplica-se o valor da largura que foi obtida da imagem, usando imagesx() pelo
		*valor da altura estabelecido para a imagem menor. em seguida divide-se o valor obtido
		*pela altura da imagem original, a função round() arredonda o valor do resultado nesta
		*operação. Este resultado é então armazenado na variável $thumb_w, que será a largura
		*da imagem pequena.
		*a operação $thumb_h = round(($old_y * $new_w) / $old_x); segue o mesmo raciocínio.
		*/
		if ($old_x > $new_w || $old_y > $new_h) {
		if ($old_x < $old_y) {
		$thumb_w = round(($old_x * $new_h) / $old_y);
		$thumb_h = $new_h;
		} elseif ($old_x > $old_y) {
		$thumb_w = $new_w;
		$thumb_h = round(($old_y * $new_w) / $old_x);
		} else {
		$thumb_w = $new_w;
		$thumb_h = $new_h;
		}
		} else {
		$thumb_w = $old_x;
		$thumb_h = $old_y;
		}


		/*a partir daqui, temos a criação propriamente dita, das imagens pequenas.
		*Repare nas funções imagecreate() e imagecreatetruecolor(). Esta ultima requer
		*versão GD 2.0.1 ou maiores. se não for possível usar gd1, então será usada gd2 para
		*diminuir a imagem original.
		*a função imagecopyresized() redimensiona e copia a imagem.
		*imagecreatetruecolor() tem um efeito similar a imagecreate().
		*Para mais informações a respeito de imagecreatetruecolor(), imagecreate() e imagecopyresampled(),
		*acesse www.php.net.
		*/
		if ($compress == "gd1") {
			@$thumbimage = imagecreate($thumb_w,$thumb_h);
			@$result = imagecopyresized($thumbimage, $origimage, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_x, $old_y);
			if ($thumbimage==false || $result==false) {
				trigger_error("Erro na tentativa de criar a imagem  redimensionada.", E_USER_NOTICE);
				exit();
			}
		} else {
			@$thumbimage = imagecreatetruecolor($thumb_w,$thumb_h);
			@$result = imagecopyresampled($thumbimage, $origimage, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_x, $old_y);
			if ($thumbimage==false || $result==false) {
				trigger_error("Erro na tentativa de criar a imagem  redimensionada.", E_USER_NOTICE);
				exit();
			}
		}

		/* FIX bug da foto de ponta cabeça quando é enviada pelo iphone (gambiarra.com)
		preg_match("/iPhone|Android|iPad|iPod|webOS/", $_SERVER['HTTP_USER_AGENT'], $matches);
		$os = current($matches);
		switch($os){
		   case 'iPhone':
		   		$thumbimage = imagerotate($thumbimage,180,0);
		    break;
		}*/


		// FIX problem rotação fotos do iphone 6, 7...
		@$exif = exif_read_data($origfile , 'IFD0');
		if($exif!==false) {
			if (isset($exif['Orientation']))
			{
			  switch ($exif['Orientation'])
			  {
				case 3:
				  // Need to rotate 180 deg
				  $thumbimage = imagerotate($thumbimage,180,0);
				  break;

				case 6:
				  // Need to rotate 90 deg clockwise
				  $thumbimage = imagerotate($thumbimage,270,0);
				  break;

				case 8:
				  // Need to rotate 90 deg counter clockwise
				   $thumbimage = imagerotate($thumbimage,90,0);
				  break;
			  }
			}
		}

		/*Muda o tempo de acesso e modificação do arquivo de imagem fornecido como terceiro parâmetro da função *createthumbnail() para o tempo atual.
		*/
		touch($thumbfile);
		/*a função imagegif() salva a imagem redimensionada gif em $thumbfile a partir de $thumbimage
		*que guarda a imagem que foi criada com novas dimensões.
		*imagejpeg() e imagepng() tem efeitos similares a função imagegif().
		*/
		$return = false;
		switch(strtolower($this->extension)) {
			case  'gif' :
				@$thumb_gif = imagegif($thumbimage, $thumbfile);
				if (!$thumb_gif) {
					trigger_error("Erro na tentativa de salvar a imagem 'GIF' redimensionada.", E_USER_NOTICE);
				}else $return = true;
				break;
			case 'jpg' || 'jpeg' :

				@$thumb_jpg = imagejpeg($thumbimage, $thumbfile,100);

				if (!$thumb_jpg) {
					trigger_error("Erro na tentativa de salvar a imagem  'JPG' redimensionada.", E_USER_NOTICE);
				}else $return = true;
				break;
			case 'png' :

				@$thumb_png = imagepng($thumbimage, $thumbfile,100);

				if (!$thumb_png) {
					trigger_error("Erro na tentativa de salvar a imagem  'PNG' redimensionada.", E_USER_NOTICE);
				}else $return = true;
				break;
		}
		//Retornar true (sucesso) ou false(falha)
		return($return);
	}
	// Reduzir

	// Cortar

	// Filtros

	// Escrever

	// Criar
}
?>
