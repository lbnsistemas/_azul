﻿<?php
require_once(SIS_SRC_LIBS.'Gerencianet/api/vendor/autoload.php'); 
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;
/**
 * Classe para emitir e manipular cobranças do banco Gerencianet
 * 
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 1.0.0
 */ 
class Gerencianet1   {
	/**
	 * Função para emitir uma nova fatura de cobrança e um boleto no banco Gerencianet
	 * @param array $dadosCobranca : [
	 	name, cpf, email, phone_number, birth, 
		address [street,number,neighborhood,zipcode,city,complement,state],
		juridical_person[corporate_name,cnpj], 
		expire_at,
		discount[type(currency,percentage),value]
		configurations[fine(multa), interest(juros)]
		message]
	 * @param array $config : [clienteID, clienteSecret, nossoNumero, notification_url]
	 * @param array $items : [name, (int)amount, (int)value]
	 * @return boolean
	 */
	public function emitirCobranca($dadosCobranca,$items,$config) {
		$options = [
		  'client_id' => $config['client_id'],// insira seu Client_Id, conforme o ambiente (Des ou Prod)
		  'client_secret' => $config['client_secret'],// insira seu Client_Secret, conforme o ambiente (Des ou Prod)
		  'sandbox' => true // altere conforme o ambiente (true = desenvolvimento e false = producao)
		];
		//Exemplo para receber notificações da alteração do status da transação.
		$metadata = ['notification_url'=>$config['notification_url']];	
		$body  =  [
			'items' => $items,
			'metadata' => $metadata
		];
		 
		try {
			$api = new Gerencianet($options);
			
			// Tenta gerar nova transação
			$objRet->ret = $api->createCharge([], $body);
			
			// Verifica se houve sucesso na geração da transação 
			if($objRet->ret['code']=='200') {
				$charge_id = $ret['data']['charge_id'];
				// $charge_id refere-se ao ID da transação gerada anteriormente
				$params = [
				  'id' => $charge_id;
				];
				
				$customer = [
				  'name' => $dadosCobranca['name'], // nome do cliente
				  'cpf' => $dadosCobranca['cpf'] , // cpf válido do cliente
				  'email' => $dadosCobranca['email'], // email do cliente
				  'phone_number' => $dadosCobranca['phone_number'], // telefone do cliente
				  'birth' => $dadosCobranca['birth'], // Dt. de nasc.
				  'address' => $dadosCobranca['address'], // Endereço do cliente
				];
				 
				$bankingBillet = [
				  'expire_at' => $dadosCobranca['expire_at'], // data de vencimento do boleto (formato: YYYY-MM-DD)
				  'customer' => $customer
				];
				 
				$payment = [
				  'banking_billet' => $bankingBillet // forma de pagamento (banking_billet = boleto)
				];
				 
				$body = [
				  'payment' => $payment
				];
				
				// Tenta emitir novo boleto
				$objRet->ret = $api->payCharge($params, $body);
				
				// Verifica se o boleto foi emitido
				if($objRet->ret['code']=='200') {
					return(true);
				}	
			}
			return(false);
		} catch (GerencianetException $e) {
			print_r($e->code);
			print_r($e->error);
			print_r($e->errorDescription);
		} catch (Exception $e) {
			print_r($e->getMessage());
		}
	}
	// Função para alterar vencimento
	
	// Função para cancelar cobrança
	
	// Função para reenviar por email
}
?>