<?php
/**
 * Classe File - Funções para manipulação de arquivos
 * Desta classe extende-se : Image, Upload
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 1.0.0
 */ 
class File  {
	// Extensao da imagem
	private $extension;
	// Nome da imagem original 
	private $fileName;
	// Novo nome da imagem editado
	private $newFileName;
	// Diretorio de trabalho
	private $workDir;
	// Url para o diretorio de trabalho
	private $urlWordDir;
	
	/**
	 * Construtor
	 */
	public  function __construct() {}
	
	/**
	 * Retorna o novo nome do arquivo
	 **/
	public function getNewFileName() {
		return($this->newFileName);
	}
	
	
	/**
	 * Retorna o diretorio de trabalho (caminho real)
	 **/
	public function getWorkDir() {
		return($this->workDir);
	}
	
	/**
	 * Retorna a URL para o diretorio de trabalho
	 **/
	public function getUrlWorkDir() {
		return($this->urlWordDir);
	}
	
	
	/**
	 * Seta o diretorio de trabalho
	 **/
	public function setWorkDir($dir){
		
		if(is_dir($dir)) {
			$this->workDir = $dir;	
			// Verifica e coloca ultima '/'
			$this->workDir = substr($this->workDir,-1)!='/' ? $this->workDir."/" : $this->workDir;
			return(true);
		}else return(false);
	}
	
	/**
	 * Seta a url para o diretorio de trabalho
	 **/
	public function setUrlWorkDir($url){
		if(!filter_var($url, FILTER_VALIDATE_URL) === false) {
			$this->urlWordDir = $url;	
			return(true);
		}else return(false);
	}
	
	/**
	 * Retorna um array com os arquivos do diretorio de trabalho
	 **/
	public function getFilesDir(){
		$retorno = array();
		$src = $this->workDir;
		$dh =(is_dir($src)? opendir($src) : false);			
		if($dh!=false) {
			while (false !== ($filename = readdir($dh))) {
				// Filtro os arquivos encontrados, excluindo '.', '..' e os thumbnails e as pastas
				if ($filename!="." && $filename!=".." && strpos($filename,"-thumbnail.")===false  && is_dir($src.$filename)==false) {
					$extension =  pathinfo($filename, PATHINFO_EXTENSION );
					$name = pathinfo($filename, PATHINFO_FILENAME );
					
					// Verifica se o arquivo possui thumbnail
					if(is_file($src.$name."-thumbnail.".$extension)) {
						$thumbnail = $this->urlWordDir.$name."-thumbnail.".$extension;
						
					// Se for imagem, exibe a mesma como thumbnail (não recomendado devido ao peso do arquivo)
					}else if(strtolower($extension)=='gif' | strtolower($extension)=='jpg' | 
					strtolower($extension)=='jpeg' | strtolower($extension)=='png' | strtolower($extension)=='bmp') {
						$thumbnail = $this->urlWordDir.$name.".".$extension;
						
					// Pega o icone padrão do sistema para arquivos conhecidos
					}else {
						$thumbnail = $this->getURLIcon($extension);	
					}
					
					// URL do arquivo original
					$fileURL = $this->urlWordDir.$filename;
					
					// Incrementa os dados do arquivo encontrado no array retorno
					array_push($retorno, array('name'=>$name, 'extension'=>$extension, 'thumbnail'=> $thumbnail, 'url'=>$fileURL));
					
				}//if
			}//while
			return($retorno);
		// Diretorio inválido
		}else {
			
			return(false);
		}
	}
	
	
	/**
	 * Retorna uma URL para o icone correspondente a extensao do arquivo
	 * @param string $extensao, string $src fonte do arquivo (diretorio raiz ou url)
	 **/
	public function getURLIcon($extensao, $src=SYS_FRAMEWORK_URL) {
		switch($extensao) {
			case 'jpg' :
			case 'jpeg' :
			case 'bmp' :
			case 'gif' :
			case 'png' :
				$thumbnail = $src."images/icones/image-icon.png";
				break;
			case 'txt' :
				$thumbnail = $src."images/icones/txt-icon.png";
				break;
			case 'xls' : case 'xlsx' : case  'xlt' :
				$thumbnail = $src."images/icones/excel-icon.png";
				break;
			case 'ppt' : case 'pps' : case 'ppsx' :
				$thumbnail = $src."images/icones/powerpoint-icon.png";
				break;
			case 'doc' : case 'docx' : case 'dot' : case 'rtf' :
				$thumbnail = $src."images/icones/word-icon.png";
				break;
			case 'rar' : case 'zip' :
				$thumbnail = $src."images/icones/zip-icon.png";
				break;
			case 'pdf' :
				$thumbnail = $src."images/icones/pdf-icon.png";
				break;
			default :
				 $thumbnail = $src."images/icones/default-icon.png";
				break;
		}
		return($thumbnail);
	}
	/**
	 * Deleta um determinado arquivo em $this->workDir
	 * @param string $fileName: nome do arquivo com extensao
	 * @return true ou false
	 **/
	public function deleteFile($fileName) {
		$ret = false;
		if(is_file($this->workDir.$fileName)) {	
			$ret = unlink($this->workDir.$fileName);
		}
		return($ret);
	}
	
	/**
	 * Caso o diretória não exista tenta criá-lo
	 * @param string $dir: diretorio a ser criado, 
	 * @param int $mode : 0755 - Tudo para o proprietario, leitura e execucao para os outros
	 * @param int $mode : 0644 - Escrita e leitura para o proprietario, leitura para todos os outros
	 * @param int $mode : 0600 - Escrita e leitura para o proprietario, nada ninguem mais
	 * @param int $mode : 0750 - Tudo para o proprietario, leitura e execucao para o grupo do prop
	 * @return true : dir. criado com sucesso, false : erro ao tentar criar dir., 0 : dir. já existe
	 **/
	protected function makeDir($dir,$mode=0755) {
		if(!file_exists($dir)) {
			// Tenta criar a pasta, se não conseguir emiti um erro e encerra a aplicação
			if(!@mkdir($dir, $mode)) {
				trigger_error("Não foi possível criar a pasta destino para upload do(s) arquivo(s). Destino : ".$dir, E_USER_NOTICE);
				return false;
			}return true;	
		}return 0;
	}
	
	/**
	 * Converte uma unidade de Bytes para uma outra unidade
	 * @param string $from :  entrada (1B, 1KB, 1MB, 1GB, 1TB, 1PB)
	 * @param string $to : unidade de saída  (B,KB, MB, GB, TB, PB)
	 * @return string : número de Bytes na unidade de saída 
	 **/
	public function convertBytes($from, $to='B') {
		// Número da unidade de entrada
		$numberEntrada = (float)$from;
		// Unidade de entrada
		$unidadeEntrada = strtoupper(str_replace($numberEntrada,"",$from));
		
		// ETAPA 1 : 
		// Converte tudo para Bytes
		switch($unidadeEntrada){
			case "KB":
				$bytes = $numberEntrada*1024;
				break;
			case "MB":
				$bytes = $numberEntrada*pow(1024,2);
				break;
			case "GB":
				$bytes = $numberEntrada*pow(1024,3);
				break;
			case "TB":
				$bytes = $numberEntrada*pow(1024,4);
				break;
			case "PB":
				$bytes = $numberEntrada*pow(1024,5);
				break;
			default:
				$bytes = $numberEntrada;
				break;
		}
		// ETAPE 2 :
		// Converte os Bytes na unidade de saída
		switch($to){
			case "KB":
				$output = $bytes/1024;
				break;
			case "MB":
				$output = $bytes/pow(1024,2);
				break;
			case "GB":
				$output = $bytes/pow(1024,3);
				break;
			case "TB":
				$output = $bytes/pow(1024,4);
				break;
			case "PB":
				$output = $bytes/pow(1024,5);
				break;
			default:
				$output = $bytes;
				break;
		}
		// Retorna o número de Bytes na unidade de saída ($to)
		return(round($output,2));
	}
	
	/**
	 * Cria um novo nome, verificando se o mesmo não existe, para a imagem editada/nova
	 * @param string $titulo : string a ser incrementada ao nome do arquivo original
	 **/
	public function newFileName($titulo,$extensao){
		$cont = 0;
		$novoNome = $titulo.".".$extensao;
		while(file_exists($this->workDir.$novoNome)) {
			$cont++;
			$novoNome = $titulo."(".$cont.").".$extensao;		
		}
		$this->newFileName = $novoNome;
		return($novoNome);
	}
}
?>