<?php
//COnfiguração de erros php.ini
error_reporting(E_ALL ^E_NOTICE ^E_STRICT ^E_WARNING);//Pega todos erros excluindo  tipo NOTICE, STRICT
ini_set('display_errors', DEBUG_MODE);//Não mostra a msg de erro padrão do php (exceto se estiver em modo DEBUG)
ini_set('log_errors', 0);//Não cria o arquivo de log padrão do php

//Função para saber o tipo(level) do erro
function friendlyErrorType($type)
{
    switch($type)
    {
        case E_ERROR: // 1 //
            return 'E_ERROR';
        case E_WARNING: // 2 //
            return 'E_WARNING';
        case E_PARSE: // 4 //
            return 'E_PARSE';
        case E_NOTICE: // 8 //
            return 'E_NOTICE';
        case E_CORE_ERROR: // 16 //
            return 'E_CORE_ERROR';
        case E_CORE_WARNING: // 32 //
            return 'E_CORE_WARNING';
        case E_COMPILE_ERROR: // 64 //
            return 'E_COMPILE_ERROR';
        case E_COMPILE_WARNING: // 128 //
            return 'E_COMPILE_WARNING';
        case E_USER_ERROR: // 256 //
            return 'E_USER_ERROR';
        case E_USER_WARNING: // 512 //
            return 'E_USER_WARNING';
        case E_USER_NOTICE: // 1024 //
            return 'E_USER_NOTICE';
        case E_STRICT: // 2048 //
            return 'E_STRICT';
        case E_RECOVERABLE_ERROR: // 4096 //
            return 'E_RECOVERABLE_ERROR';
        case E_DEPRECATED: // 8192 //
            return 'E_DEPRECATED';
        case E_USER_DEPRECATED: // 16384 //
            return 'E_USER_DEPRECATED';
    }
    return "";
}

// error handler function
function myErrorHandler($errno, $errstr, $errfile, $errline)
{

	// BUG -> E_NOTICE NÃO INTEREÇÃO EM PRODUÇÃO
  if(friendlyErrorType($errno)=='E_NOTICE' || friendlyErrorType($errno)=='E_STRICT' || friendlyErrorType($errno)=='E_WARNING' )
  return(true);

	$compl='';
	//Verifica se o usuário esta logado e registra o nome
	if($_SESSION[SES_USER_NAME]!='') {
		$compl .= "-".$_SESSION[SES_USER_NAME];
	}
	//Verifica se tem informação da empresa
	if($_SESSION[SES_COMPANY_NAME]!='') {
		$compl .= "-".$_SESSION[SES_COMPANY_NAME];
	}


	//Monta a linha com todas informações sobre o erro
  $linhaErro = "[".date('Y-m-d H:i:s')."] [".friendlyErrorType($errno)."] [".SYS_USER_IP.$compl."] [".$errfile." - ".$errline."] [View: "._view.", Module:"._module.", Action:".$_POST['action'].", Post: ".implode("|",$_POST).", Get: ".implode("|",$_GET)."]\r\n  $errstr \r\n";

	//Verifica se houve sucesso no envio do email ao administrador do sistema
	$avisado = error_log($linhaErro,1,SYS_SUPPORT_MAIL);
	//E_USER_WARNING São erros gerados manualmente, nesta plataforma está reservado para os erros do mysql
	//o registro já é efetuado pela classe Database->writeLog(erro,true) : errosmysql.txt
	$registrado = (friendlyErrorType($errno)!='E_USER_WARNING' ?  error_log($linhaErro,3,SYS_FILE_LOG_ERROR_PHP) : true);



	//Se houve falha no registro ou no envio, mostra os detalhes do erro na tela
	if($avisado==false && $registrado==false) {
		die('{"message":"O sistema retornou um erro, contate o suporte t&eacute;cnico: '.SYS_SUPPORT_MAIL.', envie este c&oacute;digo em anexo:
		<b>error: '.$errstr.', line: '.$errline.', file: '.$errfile.'</b>, "type":"fail","data":""}');
	//Registrado porém não avisado
	}else if($avisado==false) {
		die('{"message":"O sistema retornou um erro, contate o suporte t&eacute;cnico: '.SYS_SUPPORT_MAIL.'", "type":"fail","data":""}');
	//Registrado e avisado!
	}else {
		die('{"message":"O sistema retornou um erro, o suporte t&eacute;cnico j&aacute; foi avisado.", "type":"fail","data":""}');
	}

    /* Don't execute PHP internal error handler */
    return true;

}
//Função de erro customizada
if(!DEBUG_MODE) {
	set_error_handler("myErrorHandler");
}

?>
