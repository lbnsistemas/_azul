<?php 
/**
 * Class Construtor : Imprimi os objetos criados em Form, prepara os objetos JS, etc..
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 1.0.0
 */ 
class Construtor extends Tabs {
	/**
	 * Imprimi um array JS com as colunas a serem carregadas na View
	 * @param object stdClass $p : Propriedade da View
	 * @return string arrays javascript
	 **/
	public function print_ViewColumnsArray($p) {
		$arViewColumns = array_merge($p->arListingHead,$p->arFindColumnsOff);
		if(is_array($arViewColumns)) {
			$cont = 0;
			$arTemp = array();
			foreach($arViewColumns as $column) {
				// Verifica se a coluna já foi impressa
				if(!in_array($column->column,$arTemp)) {
					// Armazena no array temporario
					array_push($arTemp,$column->column);
					// Imprime coluna
					print '"'.$column->column.'"';
					if($cont<count($arViewColumns)-1) {
						print ',';	
					}
					$cont++;
				}	
			}
		}	
	}
	
	/**
	 * Imprimi um array JS com as colunas visíveis na listagem
	 * @param object stdClass $p : Propriedade da View
	 * @return string arrays javascript
	 **/
	public function print_ListingColumnsArray($p) {
		if(count($p->arListingHead)>0) {
			$cont = 0;
			$arTemp = array();
			foreach($p->arListingHead as $column) {
				// Verifica se a coluna já foi impressa
				if(!in_array($column->column,$arTemp)) {
					// Armazena no array temporario
					array_push($arTemp,$column->column);
					// Imprime coluna
					print '"'.$column->column.'"';
					if($cont<count($p->arListingHead)-1) {
						print ',';	
					}
					$cont++;
				}	
			}
		}	
	}
	
	/**
	 * Imprimi os objetos de cada campo no padrão JS
	 * @param array stdClass $fields
	 * @return string objetos javascript
	 **/
	public function print_FieldsObjects($fields) {
		if(is_array($fields)) {
			foreach($fields as $field) {
				// Se for um editor HTML, o value será o conteúdo da div
				if($field->dataType=='html') {
					$value = "$('#".$field->nameID."').summernote('code')";	
				}else {
					$value = "_azDataManipulation.checkIn('#".$field->nameID."')";
				}
				print "
					{
						'column' 		: '".$field->column."',
						// Faz checkin tratando certos tipos de dados e valores
						'value'			: ".$value."
					},
					";
			}
		}
	}
	
	/**
	 * Função para imprimir os argumentos de comparação do IF  da função JS azFilterData()
	 * Para o sistema saber quando os filtros foram alterados e fazer nova requisição de linhas ao WS
	 * @param object StdClass $p -> objecto com as configurações da View
	 * @return void
	 
	public function print_azFilterData_Return($p) {
		$compl = "";
		// Monta todos os campos de filtro opcionais para envio
		// $p->arFindColumnsOff = colunas padrão para filtragem offline
		foreach($p->arFindColumnsOff  as $col) {
			if(is_array($col)) {
				if(trim($col->column) != "") {
					if($col->rule == '='){
						$compl .= "element.".$col->column." == filtro | "		;
					}else if($col['funcao'] == '%'){
						$compl .= "element.".$col->column.".toLowerCase().indexOf(filtro) > -1 | ";	
					}
				}
			}
		}
		//Remove ultimos 2 caracteres ('| ');
		$compl = ($compl!="" ? substr($compl,0,-2) : "");
		
		//Imprimi os argumentos
		print $compl;
	}
	**/
	
	/**
	 * Função para imprimir os argumentos de comparação do IF  da função JS filtraBusca()
	 * Para o sistema saber quando os filtros foram alterados e fazer nova requisição de linhas ao WS
	 * @param object StdClass $p -> objecto com as configurações da View
	 * @return void
	 **/
	public function print_azFilterData_ArgumentsIF($p) {
		$complArgsIf = "";
		$cont=0;
		//Monta todos os campos de filtro opcionais para envio
		foreach($p->arFilters as $filter) {
			//Verefica se existem dois campos na mesma linha, ex: data1 à data2, valor1 e valor2,..
			if(count($filter)>1) {
				// campo 1
				$fieldValue = "$('#".$filter[0]->nameID."').val()";
				$fieldDefaultValue = "$('#".$filter[0]->nameID."').attr('az-default')";
				$complArgsIf .= $fieldValue." != ".$fieldDefaultValue." | ";
				// campo 2 
				$fieldValue = "$('#".$filter[1]->nameID."').val()";
				$fieldDefaultValue = "$('#".$filter[1]->nameID."').attr('az-default')";
				$complArgsIf .=  $fieldValue." != ".$fieldDefaultValue." | ";
			//Apenas uma campo por linha	
			}else {
				// Se for o filtro genérico (primeiro campo) pula
				if($cont==0) {
					$cont++;
					continue;
				}else {
					// Demais filtros (opcionais)
					$fieldValue = "$('#".$filter->nameID."').val()";
					$fieldDefaultValue = "$('#".$filter->nameID."').attr('az-default')";
					$complArgsIf .=  $fieldValue." != ".$fieldDefaultValue." | ";	
				}
			}
			$cont++;
		}
		//Remove ultimos 2 caracteres ('| ');
		$complArgsIf = ($complArgsIf!="" ? " | ".substr($complArgsIf,0,-2) : "");
		
		//Imprimi os argumentos
		print $complArgsIf;
	}
	/**
	 * Função para imprimir a sintaxe do return da função JS filtraBusca()
	 * @param object StdClass $p -> objecto com as configurações da View
	 * @return void
	 **/
	public function print_azFilterData_Return($p) {
		$compl = "";
		//Monta todos os campos de filtro opcionais para envio
		for($i=0;$i<count($p->arFindColumnsOff);$i++) {
			$col = $p->arFindColumnsOff[$i];
			if(is_object($col)) {
				
				if(trim($col->column) != "") {
					$compl .= "searchWords(element.".$col->column.", wordsFilter,'".$col->rule."')==true  ";
					if($i<count($p->arFindColumnsOff)-1) $compl .= " | ";
				}
			}
		}
		//Remove ultimos 2 caracteres ('| ');
		$compl = ($compl!="" ? substr($compl,0,-2) : "");
		//Se estiver nulo, imprimi a palavra 'false'
		$compl = (trim($compl)=='' ? 'false' : $compl);
		//Imprimi os argumentos
		print $compl;
	}
	/**
	 * Função para imprimir  1 objeto para cada filtro
	 * @param object stdClass $p : propriedades da View
	 * @return string array de objects em JS
	 **/
	public function print_FiltersObjects($p) {
		// Verifica se é um objeto
		if(is_array($p->arFilters)) {
			
			// Monta os objetos em JS
			foreach($p->arFilters as $filter) {
				
				//Verefica se existem dois campos na mesma linha, ex: data1 à data2, valor1 e valor2,..
				if(is_array($filter)) {
					// Definição do array JS com as colunas do filtro
					$arJSColumns = str_replace(array('[',']'),'',$filter[0]->columns);
					$arJSColumns = str_replace(',','","',$arJSColumns);
					// campo 1
					$fieldValue1 = "_azDataManipulation.checkIn('#".$filter[0]->nameID."')";
					// campo 2 
					$fieldValue2 = "_azDataManipulation.checkIn('#".$filter[1]->nameID."')";
					// novo Objeto campo
					$complFiltersObject .= '{
								"value1"  : '.$fieldValue1.',
								"value2"  : '.$fieldValue2.',
								"columns" : ["'.$arJSColumns.'"],
								"rule"	  : "'.$filter[0]->rule.'"
								},'."\r\n";
					
				//Apenas um campo por linha	
				}else {
					// Definição do array JS com as colunas do filtro
					$arJSColumns = str_replace(array('[',']'),'',$filter->columns);
					$arJSColumns = str_replace(',','","',$arJSColumns);
					// Campo
					$fieldValue = "_azDataManipulation.checkIn('#".$filter->nameID."')";
					// novo Objeto campo
					$complFiltersObject .= '{
								"value1"  : '.$fieldValue.',
								"value2"  : "",
								"columns" : ["'.$arJSColumns.'"],
								"rule"	  : "'.$filter->rule.'"
								},'."\r\n";
				}
			}
			
			//Remove ultimos 3 caracteres (',\r\n');
			$complFiltersObject = ($complFiltersObject!="" ? substr($complFiltersObject,0,-3) : "");
			
			//Imprimi os argumentos
			print $complFiltersObject;
		}
	}
	/**
	 * Imprimi um script adicional na função prepareContainer para exibir os arquivos dos frames upload
	 * e também para executar as funções de carregamento (onLoad) de cada campo (caso encontre)
	 * @param array Tabs $abas : array com todos os objetos tupo Aba da view
	 * @return string (javascript)
	 **/
	public function print_prepareContainer_content($tabs) {
		// Verifica se $abas é uma instância de Tabs
		if( $tabs instanceof Tabs == true ) {
			# Aqui é o script para preenchimento de todos os campos do form principal
			foreach($tabs->getAllMainForms() as $tabForm) {
				$form = $tabForm['form'];
				$tab = $tabForm['tab'];
				$formID= $form->nameID;
				if(is_array($form->getFields())) {
					foreach($form->getFields() as $field) {
						// Caso exista algo escrito na prop. onLoad do object campo
						if($field->onLoad!=''){
							// Prepara JS para executar a função ao ser carregado a informação dinamicamente
							// BUG: onChange não dispara nenhuma função, se o campo for alterado via javascript
							echo $field->onLoad."; \r\n";
						}
					}
				}
			}
			
			# Aqui é o script para preenchimento dos forms dinamicos
			foreach($tabs->getAllDynamicForms() as $tabForm) {
				$form = $tabForm['form'];
				$tab = $tabForm['tab'];
				// Imprimi script JS para carregar a lista de dados do form dinamico
				echo "showDynamicFormDataList('".$form->nameID."',objRet.".$form->table."); \r\n";
			}
			
			# Aqui é o script para preenchimento dos quadros de upload
			if(count($tabs->getAllUploadsFrames())>0) {
				// Imprimi script JS para carregar as imagens do quadro de Upload
				echo "showUploadFramesContent(objRet.uploadFramesContent); \r\n";
			}
		// Erro não é uma instância de Tabs
		}else return(false);
	}
	/**
	 * Imprimi o script para preencher os campos do form
	 * @param array Tabs $abas : array com todos os objetos tupo Aba da view
	 * @return string (javascript)
	 **/
	public function print_fillOutForms_content($tabs) {
		// Verifica se $abas é uma instância de Tabs
		if( $tabs instanceof Tabs == true ) {
			# Aqui é o script para preenchimento de todos os campos do form principal
			foreach($tabs->getAllMainForms() as $tabForm) {
				//var_dump($tabs->getAllMainForms());
				//exit();
				$form = $tabForm['form'];
				$tab = $tabForm['tab'];
				$formID= $form->nameID;
				if(is_array($form->getFields())) {
					foreach($form->getFields() as $field) {
						// Tratamento para moeda
						if($field->dataType=='currency') {
							echo "value = _azDataManipulation.floatToCurrency(objRet.".$form->table."[0].".$field->column."); \r\n";
						// Tratamento para date
						}else if(strpos($field->dataType,'date')>-1) {
							echo "value = _azDataManipulation.invertDate(objRet.".$form->table."[0].".$field->column."); \r\n";
						
						// Valor sem tratamento
						}else {
							echo "value = objRet.".$form->table."[0].".$field->column."; \r\n";	
						}
						echo "value = value==undefined ? '':value;\r\n";
						// Prepara JS para imprimir o valor da coluna carregada dinamicamente
						// Caso seja um input text/password
						if($field->fieldType=='input') {
							echo "$('#".$field->nameID."').val(value); \r\n";
							// Valor default, será utilizado para determinar se o campo foi alterado ou não
							echo "$('#".$field->nameID."').attr('az-default',value); \r\n";
						// Caso seja um textarea
						}else if($field->fieldType=='text') {
							echo "$('#".$field->nameID."').text(value); \r\n";
							echo "$('#".$field->nameID."').val(value); \r\n";// Bug jquery
							// Valor default, será utilizado para determinar se o campo foi alterado ou não
							echo "$('#".$field->nameID."').attr('az-default',value); \r\n";
						// Caso seja um select
						}else if($field->fieldType=='select' || $field->fieldType=='select2' ) {
							echo "
							var arValue = value.split(',');
							for(var u in arValue) {
								var val = arValue[u];
								$('#".$field->nameID." option[value=\"'+val+'\"]').attr('selected',true);
								$('#".$field->nameID." option[value=\"'+val+'\"]').prop('selected',true); 
							}
							";
							// Valor default, será utilizado para determinar se o campo foi alterado ou não
							//echo "$('#".$campo->nameID."').attr('sa-default',$('#".$campo->nameID." option[value=\"'+value+'\"]').text()); \r\n";
							echo "$('#".$field->nameID."').attr('az-default',value); \r\n";
							
							// Se for um select2 precisa reiniciar o objeto
							if($field->fieldType=='select2'){
								// Se foi informada a função formatState nas propriedades do campo :
								if($field->templateResult!='') {
									echo "
										$('#".$field->nameID."').select2({
											templateResult	: ".$field->templateResult."
										});";
								// Inicialização normal	
								}else {
									echo "$('#".$field->nameID."').select2();";
								}
								
							}
						// Caso seja um chekbox
						}else if($field->fieldType=='checkbox') {
							echo "if(value) { \r\n";
							echo "$('#".$field->nameID."').attr('checked','checked'); \r\n";
							// Valor default, será utilizado para determinar se o campo foi alterado ou não
							echo "$('#".$field->nameID."').attr('az-default','checked'); \r\n";
							echo "}else $('#".$campo->nameID."').attr('az-default','unchecked'); \r\n";
						// Caso seja uma DIV (Ex. : editor HTML summerNote)
						}else if($field->fieldType=='div') {
							echo "$('#".$field->nameID."').summernote('code', value)\r\n";
						}
						
						// Caso exista algo escrito na prop. onLoad do object campo
						if($field->onLoad!=''){
							// Prepara JS para executar a função ao ser carregado a informação dinamicamente
							// BUG: onChange não dispara nenhuma função, se o campo for alterado via javascript
							echo $field->onLoad."; \r\n";
						}
					}
					// Imprimi chamada ao validador do form
					echo "_azValidation.exec('".$formID."');";
				}
			}
		
			# Aqui é o script para preenchimento dos forms dinamicos
			foreach($tabs->getAllDynamicForms() as $tabForm) {
				$form = $tabForm['form'];
				$tab = $tabForm['tab'];
				if(is_array($form->getFields())) {
					foreach($form->getFields() as $field) {
						// Caso exista algo escrito na prop. onLoad do object campo
						if($field->onLoad!=''){
							// Prepara JS para executar a função ao ser carregado a informação dinamicamente
							// BUG: onChange não dispara nenhuma função, se o campo for alterado via javascript
							echo $field->onLoad."; \r\n";
						}
					}
				}
				// Imprimi script JS para carregar a lista de dados do form dinamico
				echo "showDynamicFormDataList('".$form->nameID."',objRet.".$form->table."); \r\n";
			}
			
			# Aqui é o script para preenchimento dos quadros de upload
			if(count($tabs->getAllUploadsFrames())>0) {
				// Imprimi script JS para carregar as imagens do quadro de Upload
				echo "showUploadFramesContent(objRet.uploadFramesContent); \r\n";
			}
			
		// Erro não é uma instância de Tabs
		}else return(false);
	}
	
	/**
	 * retorna um array com os ids(html) dos forms do pacote principal ou dos forms dinamicos
	 * ou dos quadros de upload
	 * @param string $tipo 'forms_principal', 'forms_dinamicos', 'forms_upload'
	 * @param Tabs $abas objeto que contém as abas da View
	 * @return javascript
	 **/
	public function getArrayIds($type,$tabs) {
		$arReturn = array();
		// Verifica se $abas é uma instância de Tabs
		if( $tabs instanceof Tabs==true) {
			if($type=='main_forms') {
				foreach($tabs->getAllMainForms() as $tabForm) {
					$tab = $tabForm['tab'];
					$form = $tabForm['form'];	
					array_push($arReturn,$form->nameID);
				}
			}else if($type=='dynamic_forms') {
				foreach($tabs->getAllDynamicForms() as $tabForm) {
					$tab = $tabForm['tab'];
					$form = $tabForm['form'];	
					array_push($arReturn,$form->nameID);
				}
			}else if($type=='upload_frames') {
				foreach($tabs->getAllUploadsFrames() as $frame) {
					array_push($arReturn,'files'.$frame->nameID);
				}	
			}
		}
		return($arReturn);	
	}
	
	/**
	 * Imprimi o script dentro da função JS 'insertUpdateRegister' para exibir os cadastros efetuados no form dinamico
	 * ex : form de endereços no cadastro de clientes
	 * @param array stdClass $campos
	 * @return javascript
	 **/
	public function print_insertUpdateRegister_content($tabs) {
		// Verifica se $abas é uma instância de Tabs
		if( $tabs instanceof Tabs==true) {
			// Tabs
			foreach($tabs->tabs as $tab) {
				if(is_array($tab->forms)==false) continue;
				// Formularios
				foreach($tab->forms as $form) {
					// Se não for um formulário dinamico, pula
					if(!$form->dynamic) continue;
					// Inicia objeto Javascript para armazenar as colunas da linha adicionada ao BD
					echo "linesObject=[{}];\r\n";
					// Inicia IF Javascript
					echo "if('".$form->nameID."'==form) {\r\n";
						// Cria coluna com o ID retornado do WS, após o cadastro ser realizado com sucesso
						echo "linesObject[0].novoID = novoID;\r\n";	
						// Verifica linhas e campos
						if(is_array($form->lines)==false || is_array($form->getFields())==false) continue;
						// Campos
						foreach($form->getFields() as $field) {
							// Pega o value do campo do form dinamico
							if($field->fieldType=='input' || $field->fieldType=='text') {
								echo "				columnValue = $('#".$field->nameID."').val();\r\n";
							// Caso seja um select
							}else if($field->fieldType=='select' || $field->fieldType=='select2' ) {
								echo "				columnValue = $('#".$field->nameID."').val();\r\n";
							// Caso seja um chekbox
							}else if($field->fieldType=='checkbox') {
								echo "				columnValue = $('#".$field->nameID."').val();\r\n";
							}
							// A coluna será criada quando o javascript ler este código ao incluir os dados,
							// Criando uma nova coluna com o valor digitado em cada campo
							echo "linesObject[0].".$field->column." = columnValue;\r\n";	
						}
						// No final, chama função padrão para duplicação de linhas dinamicas
						echo "showDynamicFormDataList(form, linesObject);\r\n";
					// Fecha IF Javascript
					echo "}\r\n";
					
				}
			}
		// Erro não é uma instância de Tabs
		}else return(false);
	}
	
	/**
	 * Imprimi o script para criar as colunas dinamicas, resultado de um cadastro 
	 * em um form dinamico na view, ex : form de endereços no cadastro de clientes
	 * @param array stdClass $campos
	 * @return javascript
	 **/
	public function printField($field) {
		// Se for informado a propriedade 'html', imprimi e para o código aqui
		if(trim($field->html)!='') {
			echo($field->html);
			return(true);
		}
		// Eventos
		$eventos  = trim($field->onBlur) != '' ? " onBlur='".$field->onBlur."' " : '';
		$eventos .= trim($field->onFocus) != '' ?  " onFocus='".$field->onFocus."' " : '';
		$eventos .= trim($field->onKeyDown) != '' ? " onKeyDown='".$field->onKeyDown."' ":''; 
		$eventos .= trim($field->onKeyPress) != '' ? " onKeyPress='".$field->onKeyPress."' ":''; 
		$eventos .= trim($field->onKeyUp) != '' ? " onKeyUp='".$field->onKeyUp."' ":''; 
		$eventos .= trim($field->onChange) != '' ? " onChange='".$field->onChange."' ":''; 
		
		// Atributo disabled
		$complemento  = $field->disabled == true ? ' disabled=disabled ' : '';
		
		// Obrigatorio
		$complemento  .= $field->required == true ? ' az-required=true ' : '';
		
		// Obrigatorio com uma condição
		$complemento  .=  ' az-condition=\''.$field->condition.'\' ' ;
		
		// Class form-control Bootstrap
		$complemento .= ' class="form-control az-field az-'.$field->fieldType.'" ';
		
		// Tipo de dados do campo (padrão SigeAzul)
		$complemento .= " az-dataType='".$field->dataType."' az-dataType-default='".$field->dataType."' ";
		
		// Titulo default
		$complemento .= " az-title-default='".$field->title."' ";
		
		// Coluna de relacionamento
		$complemento .= " az-column-db='".$field->column."' ";
		
		// Exemplo de preenchimento 
		$complemento .= " placeholder='".$campo->example."' title='".$field->example."' ";
		
		// Info de campo oculto 
		$complemento .= $field->hidden == true ? " az-hidden='true' " : "";
		
		// Parametros extras
		$complemento .= " ".$field->extra." ";	
		
		// Tipo campo
		$complemento .= " az-fieldType='".$field->fieldType."' az-ID='' ";	
		
		// Padroniza o campo pelo tipo de dados a serem inseridos
		// Tipo de dados : CPF
		if($field->dataType=='cpf') {
			$field->maxLength 	= 14;
		// Tipo de dados : CNPJ
		}else if($field->dataType=='cnpj') {
			$field->maxLength 	= 18;
		// Tipo de dados : IE
		}else if($field->dataType=='ie') {
			$field->maxLength 	= 15;
		// Tipo de dados : senha
		}else if($field->dataType=='password') {
			// Altera o tipo do campo para mascarar a senha
			$field->fieldType='password';
		// Tipo de dados : data
		}else if($field->dataType=='date') {
			$field->maxLength 	= 10;
			$complemento .= "  data-format='dd/MM/yyyy' ";
		// tipo de dados : hora
		}else if($field->dataType=='time') {
			$field->maxLength 	= 5;
			$complemento .= "  date-format='hh:mm' ";
		// Tipo de dados : dataHora
		}else if($field->dataType=='dateTime') {
			$field->maxLength 	= 16;
			$complemento .= " date-format='dd/MM/yyyy hh:mm' ";
		// Tipo de dados : postal
		}else if($field->dataType=='postal') {
			$field->maxLength 	= 10;
		// tipo de dados : numero
		}else if($field->dataType=='number') {
			$field->maxLength 	= 10;
		// tipo de dados : moeda
		}else if($field->dataType=='currency') {
			$field->maxLength 	= 16;
		}
		
		// Inputs (text, ie, cnpj, cpf, password, date, time, postal, number, currency)
		if($field->fieldType=='input' || $field->fieldType=='password') {
			
			// Se for data ou dataHora, mostra icone de data
			if(strpos($field->dataType,"date") !== false ) {
				$retorno = '
							<div class="input-group date" id="dh'.$field->nameID.'">';
				// Estabelece formato (apenas data ou data e hora)
				$formato = $field->dataType=='dateTime' ? 'DD/MM/YYYY HH:mm' : 'DD/MM/YYYY';			
				$complemento .= " date-format='".$formato."' ";
			}else $retorno = '';
			
			// Monta o campo
			$retorno .= "
							<input type='".$field->fieldType."' ".$disabled." name='".$field->nameID."' id='".$field->nameID."' 
			maxlength='".$field->maxLength."'  value='".$field->value."' az-default='".$field->value."' ".$eventos.$complemento.">\r\n";
			
			// Se for data ou dataHora, mostra icone de data
			if(strpos($field->dataType,"date") !== false) {
				$retorno .= ' 
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>	
							 <script type="text/javascript">
								// Campo data hora 1
								$("#dh'.$field->nameID.'").datetimepicker({
									locale : "pt-br",
									format : "'.$formato.'"
								});
							 </script>
				';
			}															
			// formataMoeda
			
		// Select (texto, nr)
		} else if($field->fieldType=='select' || $field->fieldType=='select2' ) {
			$retorno = "
							<select name='".$field->nameID."' id='".$field->nameID."' ".$disabled." ".$eventos.$complemento.">\r\n";
			
			// Monta as opções do select
			if(is_object($field->options) || is_array($field->options)) {
				foreach($field->options as $arOption) {
					$attr = $arOption->attributes;
					$retorno .= "
								<option value='".$attr->value."' ".$attr->extra.">".$arOption->text."</option>\r\n";
				}
			// Imprimi um option vazio, para evitar erros de validação no form.
			}else {
					$retorno .= "
								<option value=''></option>\r\n";
			}
			$retorno .= "
							</select>\r\n";
			
		// TextArea (texto)
		} else if($field->fieldType=='text') {
			$retorno = "
							<textarea name='".$field->nameID."' id='".$field->nameID."' maxlength='".$field->maxLength."' 
							cols='".$field->cols."' rows='".$field->rows."' ".$disabled." ".$eventos.$complemento.">
							".$field->value."</textarea>\r\n";
			
		// Div (editor html)
		} else if($field->fieldType=='div') {
			$retorno = "
							<div name='".$field->nameID."' id='".$field->nameID."' ".$disabled." ".$eventos.$complemento.">
							".$field->value."</div>\r\n";
		// Checkbox
		} else if($field->fieldType=='checkbox') {
			// Monta o html do input hidden, responsavel por armazenar os valores do chks marcados
			$retorno .= "
			<div style='width:100%; float:left;' class='".$field->nameID."'>
			<input type='hidden' id='".$field->nameID."' name='".$field->nameID."' ".$complemento." />";
			
			
			
			// Monta todas opções (checkboxes)
			if(is_array($field->options)) {
				foreach($field->options as $arOption) {
					$attr = $arOption->attributes;
					$retorno .= "
					<div class='checkbox-inline'><input type='checkbox'  value='".$attr->value."' ".$attr->extra." /><label >".$arOption->text."</label></div>\r\n";
				}
			// Imprimi um option vazio, para evitar erros de validação no form.
			}
			
			$retorno .= "</div>";
		}
		// Imprimi o campo/grupo criado
		echo $retorno;
	}
	
	
	
	/**
	 * Imprimi os cabeçalhos das abas no template
	 * @param Tabs $abas 
	 **/
	public function printTabs($tabs) {
		if(is_array($tabs)) {
			$cont = 0;
			foreach($tabs as $tab) : 
				$class = $cont==0 ? "active" : "";
				$cont++;
?>
				 <li role="presentation" class="<?=$class?>">
                	<a href="#pn<?=$tab->nameID?>" aria-controls="pn<?=$tab->nameID?>" 
                    role="tab" data-toggle="tab"><?=$tab->title?></a>
                </li>	
<?php		endforeach;
		}
	}
	
	/**
	 * Imprimi os forms de ca aba no template
	 * @param Tabs $abas 
	 **/
	public function printForms($tabs) {
		if(is_array($tabs)) {
			$cont = 0;
			foreach($tabs as $tab) : 
				$class = $contAba==0 ? "active" : "";		
					
?>
                <!--Aba <?=$tab->title?>-->
                <div role="tabpanel" class="tab-pane <?=$class?>" id="pn<?=str_replace(' ','',$tab->nameID)?>"><?php
					  	// Busca todos forms da corrente Aba
						//$arCoresFundo = array('bgCinza4');
						$contForm = -1;
					  	foreach($tab->forms as $form) {
							
							// Verifica se o form inicia já aberto ou oculto
							if($form->open) {
								$hide_form = '';
								$complLabel = '- ';
							// Oculto 	
							}else {
								$hide_form = 'hide';
								$complLabel = '+ ';
							}
							
							
							// Troca o fundo a cada form, até atingir o número máximo de cores, e reinicia
							if($contForm==count($arCoresFundo)-1) {
								$contForm=0;	
							}else {
								$contForm++;	
							}
							
							// Verifica se o formulário possuí algum icone informativo
							if(trim($form->info)!='') {
								$i = $form->info;
								$complInfo = '<i class="glyphicon glyphicon-info-sign handCursor" onClick="_azGeneral.alert(\'Informações\',\''.$i.'\')"></i>';
							}else $complInfo = '';
							
							
							
					  ?>
                      <div class="row ">
                        <div class="col-xs-12">
                        	<br />
                            <label class="bold handCursor" onclick="showHideDynamicForm('<?=$form->nameID?>',this)">
                            <i><?=$complLabel.$form->title?></i> 
                            </label> <?=$complInfo?>
                         
                        </div>
                      </div>
                      <!--FORMULARIO : <?=$form->title?> -->
					  <form class="az-form <?=$hide_form." ".$arCoresFundo[$contForm]?> <?=$form->dynamic ? 'dynamic' : 'main'?>" id="<?=$form->nameID?>" onSubmit="return(false)" >
                     	 <fieldset>
						 <?php
							// Busca todas as linhas do Formulario
							if(is_array($form->uploadFrames)) {
							echo '<!--UPLOAD DE ARQUIVOS-->';
							foreach($form->uploadFrames as $qUp) {
								
						 ?>
                       <!-- BOTÃO UPLOAD  -->
                       <div class="row ">
                             <div class="col-xs-12 col-md-4 col-lg-3">
                                 <!-- The fileinput-button span is used to style the file input field as button -->
                                 <span class="btn btn-primary fileinput-button">
                                    <i class="glyphicon glyphicon-plus"></i>
                                    <span>Selecione os arquivos...</span>
                                    <!-- The file input field used as target for the file upload widget -->
                                    <input id="<?=$qUp->nameID?>" destination="<?=$qUp->destination?>" 
                                    allowedExtensions="<?=$qUp->allowedExtensions?>" 
                                    maxAmount="<?=$qUp->maxAmount?>" maxSizeMB="<?=$qUp->maxSizeMB?>"  
                                    type="file" name="files[]" class="az-field"  value="" az-default="" 
                                    az-column-db="<?=$qUp->column?>" az-dataType='text'   multiple>
                                 </span>
                                 <!-- The global progress bar -->
                                 <div id="progress<?=$qUp->nameID?>" class="progress">
                                    <div class="progress-bar progress-bar-success"></div>
                                 </div>
                              </div>
                        </div>
                        <!-- ARQUIVOS E IMAGENS ENVIADOS -->
                        <div class="row" id="<?='files'.$qUp->nameID?>" >
                        </div>
                         <?php }}//Quadros upload ?>
                      	 <!--LINHAS --> 
						 <?php
							// Busca todas as linhas do Formulario
							if(is_array($form->lines)) {
							foreach($form->lines as $line) {
						  ?>
								<div class="row">	
								<!--COLUNAS-->
								<?php

                                // Busca todas as Colunas na linha
                                foreach($line->columns as $column) {
                                    $xs = $column->col_xs;
                                    $md = $column->col_md;
                                    $lg = $column->col_lg;
                                ?>
								<div class="col-xs-<?=$xs?> col-md-<?=$md?> col-lg-<?=$lg?>" >
								<?php
                                // Busca todas os campos da coluna
                                foreach($column->fields as $field) {
									
									// Verifica se o campo possuí algum icone informativo
									if(trim($field->info)!='') {
										$i = $field->info;
										$complInfo = '<i class="glyphicon glyphicon-info-sign handCursor" onClick="_azGeneral.alert(\'Informações\',\''.$i.'\')"></i>';
									}else $complInfo = '';
                                ?>
                                      <!--CAMPO <?=$field->title?>-->
                                      <div class="form-group <?=$field->hidden==true ? "hide" : ""?> initial">
                                        <label for="<?=$field->nameID?>" ><?=$field->title?> </label> <?=$complInfo?>
										<?php
                                            View::printField($field);
                                        ?>
                                        <div class="help-block with-errors"></div>
                                      </div>
							     <?php }//Campos ?>
                                  </div><?php 
                                        }//Colunas 
                                        // Se for form dinamico, cria botao de inclusão
                                        if ($form->dynamic) {
											// Script JS para iniciar o validador para este form dinamico
											echo "<script>_azAzul.afterLoad.push(function(){
												_azValidation.init('#".$form->nameID."')
											});
											</script>";
                                  ?>
                                  <div class="col-xs-1 col-md-1 col-lg-1">
                                    <div class="form-group">
                                    <button class="btn btn-success margin-bottom" style="margin-top:24px;" onclick="insertUpdateRegister('<?=$form->nameID?>', true,'insert','')"><b>+</b></button>
                                    </div>
                                  </div><?php }// If dinamico ?>
                              </div>
						  <?php 
							}}//Linhas 
							
							// Se for form dinamico, cria botoes de edição e exclusão
							// E uma lista de ítens cadastrados 
							if ($form->dynamic) :
                          ?>
                          <!--LISTAGEM DE <?=$form->title?>-->
                          <div class="" id='list<?=$form->nameID?>'>
                          	<!-- COLUNAS A SEREM INSERIDAS DINAMICAMENTE... -->			
                          </div>
                          <?php endif; ?>
                          </fieldset>
                      </form>    
				    <?php 	
                             }//Formularios 
                        // Mostra os botoes de concluir e fechar apenas na aba principal
                        if($contAba==0) {
                    ?>
                    <!-- BOTOES FECHAR - CONCLUIR -->
                    <br />
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-lg-12">
                        	<?php
									// Se for Modo de edição, não mostra a listagem, apenas um cadastro especifico
									if(View::config()->editMode==false) :
							?>
                         	<button type="button" class="btn btn-default" data-dismiss="modal" 
                            onClick="resetForms();showHideListing();">Fechar</button>
                            <?php	endif; ?>
                            
                            <button type="button" class="btn btn-success" id="btnConfirmation" az-action="update" az-ID="<?=View::config()->ID?>" 
                            onClick="insertUpdateRegister('Main', false, '', '')"></button>
                           
                        </div>
                    </div>
                 <?php } ?>
                </div>
<?php		
				$contAba++;
			endforeach;
		}
	}
	/**
	 * Imprimi os forms de ca aba no template
	 * @param string $titulo  : titulo a ser impresso (aceita html)
	 * @param bool $isPortable : identifica dispositivo portátil ou não
	 **/
	public function printTitleForm($title, $isPortable) {
?>
		<p class="cinzaEscuro" >   
			<!--<button class="btn btn-default glyphicon glyphicon-chevron-right <?=($isPortable==true ? 'hide' : '')?>" 
            id="btAbreMenuLat" onclick="openCloseLateralMenu()" ></button> -->
            <em class="tam20"><?=$title?></em>  
            <strong id="infoActionView" ></strong> 
        </p>
<?php	
	}
	/**
	 * Imprimi a listagem de dados no template
	 * @param object stdClass $p : Propriedades da View
	 **/
	public function printListing($p) {
	
?>
		 <!------------------------------------------------------------------
        --- Container cListagem: Paginação dos cadastros com filtro de busca
        ------------------------------------------------------------------->
        <div class="destaqueShadow bordaCinza cinzaEscuro" id="ctListing" >
            <!--Filtros-->
            <div class="col-xs-5 col-sm-6" style="padding-left:0">
            	<?php
					if($p->normalMode) :
				?>
                <button class="btn btn-primary margin-bottom btn-new-register" style="margin:3px" onClick="prepareContainer('insert',0);">
                	<b>+</b> Novo cadastro
                </button>
                <?php endif; ?>
            </div>
            <div class="col-xs-7 col-sm-6" style="padding-right:0">
                
                    <div class="input-group">
                        <input type="text" class="form-control " az-dataType="texto" az-default="" id="<?=$p->arFilters[0]->nameID?>" onKeyUp="azFilterData()" >
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="modal" data-target="#mdFiltroBusca">
                            <span class="glyphicon glyphicon-search"></span></button>
                        </span>
                    </div>
             </div>
            <!--Dados-->
            <table id="tbData" class="table table-striped table-hover">
                <thead>
                    <tr>
                    	<?php
							// Imprimi os cabeçalhos da listagem
							foreach(View::config()->arListingHead as $head){
								echo '<th class="handCursor '.($head->isPortable===false ? 'hidden-xs' : '').'" align="'.$head->align.
								'" onClick="orderColumn(this,\''.$head->column.'\')">'.$head->title.
								' <span class=""></span></th>';
							}
						?>
                        <?php
							if($p->normalMode || $p->editMode) :
						?>
                        <th align="right"><!--<button class="btn btn-default float-right dropdown-toggle" data-toggle="modal" data-target="#mdListingFunctions" >Funções </button>--></th>
                        <?php endif; ?>
                    </tr>
                </thead>
                <tbody>
                    <!--LINHAS/CADASTROS-->
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="12" bgcolor=""> 
                            <!--PAGINAÇÃO-->
                            <ul class="pagination"></ul>
                        </td>
                    </tr>
                </tfoot>
            </table>
		</div>
    </div>
<?php
	}
	
	
	public function printBusca($titulo) {
	}
	
}

/**
 * Class Tabs : Cria as Tabs do sistema
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 1.0.0
 */ 
class Tabs {
	public $tabs;
	public function __construct() {}
	/**
	 * Cria um objeto do tipo Aba
	 * @param string $titulo : titulo  da Aba
	 * @param View $objView ; instancia de View
	 * @return Aba
	 **/
	public function makeTab($title='Unknown', $objView='') {
		 $newTab = new Tab($title, $objView);
		 $this->tabs[] = $newTab;
		 return($newTab);
	}
	/**
	 * Retorna todos os quadros de um upload em 1 array
	 * @return array stdClass 
	 **/
	public function getAllUploadsFrames() {
		$retorno = array();
		if(is_array($this->tabs)) {
			foreach($this->tabs as $tab) {
				foreach($tab->forms as $form) {
					
					if(is_array($form->uploadFrames)) {
						// Combina todos os array em um unico array para retornar os forms encontrados
						$retorno = array_merge($retorno, $form->uploadFrames);
					}
				}
			}
		}
		return($retorno);
	}	
	
	/**
	 * Retorna todos os forms dinamicos
	 * @param Array $abas : objeto com todas as abas carregadas na View (opcional)
	 * @return array com object type Formulario
	 **/
	public function getAllDynamicForms($tabs='') {
		$Tabs = $tabs!='' ? $tabs : $this->tabs;
		
		$retorno = array();
		if(is_array($Tabs)) {
			
			foreach($Tabs as $tab) {
				
				foreach($tab->forms as $form) {
					
					if($form->dynamic==true && is_array($form->uploadFrames)==false) {
						
						// Combina todos os array em um unico array para retornar os forms encontrados
						$retorno[] = array("tab"=>$tab, "form"=>$form);
					}
				}
			}
		}
		return($retorno);	
	}
	
	/**
	 * Retorna todos os forms do pacote principal 
	 * @return array com object type Formulario
	 **/
	public function getAllMainForms() {
		$retorno = array();
		if(is_array($this->tabs)) {
			foreach($this->tabs as $tab) {
				if(is_array($tab->forms)) {
					foreach($tab->forms as $form) {
						if($form->dynamic==false && is_array($form->uploadFrames)==false) {
							// Combina todos os array em um unico array para retornar os forms encontrados
							$retorno[] = array("tab"=>$tab, "form"=>$form);
						}
					}
				}
			}
		}
		return($retorno);		
	}
	
	/**
	 * Retorna todos os campos dos forms do pacote principal 
	 * @param Array $abas : objeto com todas as abas carregadas na View (opcional)
	 * @return array com objects type Formulario
	 **/
	public function getAllMainFormsFields($tabs='') {
		$tabs = $tabs!='' ? $tabs : $this->tabs;
		$retorno = array();
		if(is_array($tabs)) {
			foreach($tabs as $tab) {
				foreach($tab->forms as $form) {
					if(is_array($form->getFields())) {
						if($form->dynamic==false && is_array($form->uploadFrames)==false) {
							// Combina todos os array em um unico array para retornar os campos encontrados
							$retorno = array_merge($retorno, $form->getFields());
						}
					}
				}
			}
		}
		return($retorno);		
	}
	
	/**
	 * Retorna todos os campos de um determinado tipo
	 * @param string $type : tipoCampo (input, select, select2, html, text, checkbox, radio..)
	 * @param Array $abas : objeto com todas as abas carregadas na View (opcional)
	 * @return array com obj stdClass
	 **/
	public function getAllFieldsFromType($type,$tabs='') {
		$tabs = $tabs!='' ? $tabs : $this->tabs;
		$retorno = array();
		if(is_array($tabs)) {
			foreach($tabs as $tab) {
				foreach($tab->forms as $form) {
					// Analisa todos os campos encontrados
					$fields = $form->getFields();
					if(is_array($fields)) {
						foreach( $form->getFields() as $field) {
							// Se for um campo do tipo html :
							if( $field->fieldType==$type ) {
								$retorno[] = $field;
							}
						}
					}
				}
			}
		}
		return($retorno);		
	}	
	
	
}

/**
 * Class Aba : Formularios, Linhas e Campos para montar os forms do sistema
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 1.0.0
 */ 
class Tab {
	public $title;
	public $forms;		// Array com todos os objects Formulario criados
	public $nameID;		// Atributo html name e ID da Aba (deve ser único) 
	/**
	 *	Construtor
	 *	@param string $titulo : titulo da Aba
	 **/
	public function __construct($title='Unknown') 
	{
		$this->title = $title;
		// Cria o nameID da Aba
		$this->nameID = $this->makeNameID('tab'.$title);
	}
	
	/**
	 *	Cria o atributo nameID utilizando uma string 
	 *	@param $string : string a ser tratada (remover acentos, espaços, ...)
	 *	@param View $objView
	 * 	@return $string nameID
	 */
	private function makeNameID($string) {
		// Retira acentos, devolvendo alfabeto puro
		$nameID = $this->takeAccents($string);
		
		// Limpa demais caractéres especiais e espaços
		$nameID = $this->clearString($nameID);
		
		// Salva o nameID criado
		return($nameID);
	}
	
	/**
	 * Limpa uma string de caracteres especiais para criar o atributo nameID da aba e do form
	 * Elimina caracteres com acentos e demais caracteres que não sejam alfabeto puro ou números
	 * @param $string : string a ser limpa, string $espacos : caracter para substituir espaços
	 * @return string sem espaços e sem caracteres especiais
	 **/
	public function clearString($string,$spaces="") {
		if($string!=''){
			$stringLimpa = str_replace(' ',$spaces,preg_replace('/[^a-zA-Z0-9]+/', '',$string));
		}
		return($stringLimpa);
	}
	
	/**
	 * Substituí caracteres acentuados por alfabeto puro
	 * @param $string : string a ser tratada, retirando os acentos
	 * @return string alfabeto puro
	 **/
	public function takeAccents($string){
		$trocarIsso = array('à','á','â','ã','ä','å','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ü','ú','ÿ',
		'À','Á','Â','Ã','Ä','Å','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','O','Ù','Ü','Ú','Ÿ');
		$porIsso = array('a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','y','A',
		'A','A','A','A','A','C','E','E','E','E','I','I','I','I','N','O','O','O','O','O','O','U','U','U','Y');
		$titletext = str_replace($trocarIsso, $porIsso, $string);
		return($titletext);
	}
	
	/**
	 * Cria um objeto do tipo Formulario
	 * @param string $titulo : titulo  do Formulario,  string $tabela tabela no BD responsavel por armazenar 
	 * os dados deste form, bool $dinamico : caso seja true, cria botões de inclusão, edição e exclusão dentro da Aba
	 * bool $aberto : caso seja true, inicia o form aberto, caso contrario deixa oculto
	 **/
	public function makeForm($title,$table, $dynamic=false, $open=true,$info='') {
		 $newForm = new Form($title, $table, $dynamic, $open,$info);
		 // Cria o atributo inicial nameID para o form recem criado
		 $newForm->nameID = $this->makeNameID('form'.$this->title.$newForm->title);
		 $this->forms[] = $newForm;
		 return($newForm);
	}	
	/**
	 * Seleciona um form pelo  titulo
	 * @param string $name : titulo do form
	 * @return Formulario
	 **/
	public function getFormByName($name)  {
		if(is_array($this->forms)) {
			foreach($this->forms as $form) {
				if($form->title == $name){
					return($form);
				}
			}
		}
		return(false);
	}
	
}
/**
 * Class Formulario
 */ 
class Form{
	public $dynamic;	// Indica se o form é dinamico, ie, manipula dados de forma independente do pacote principal
	public $open;		// Indica se os campos já aparecem aberto ou ocultos
	public $title;		// Titulo do form, exibido na view

	public $lines;		// Array contendo as linhas do form
	public $table;		// Nome da tabela que o formulário manipula
	public $uploadFrames;	// Array contendo os quadros para upload 
	public $nameID;		// Atributo html name e ID do form (deve ser único) * Dispensável caso seja form do pacote Principal
	public $info;		// Caso seja informado, cria um icone informativo contendo uma mensagem ao usuário sobre determinado campo ou form.
	public function __construct($title='Unknown', $table,  $dynamic=false, $open=false, $info='') 
	{
		$this->title = $title;
		$this->table = $table;
		$this->dynamic = $dynamic;
		$this->open = $open;	
		$this->info = $info;
		
	}
	/**
	 * Cria uma nova linha dentro da instancia do Formulario
	 **/
	public function makeLine() {
		 $novaLinha = new Line();
		 $this->lines[] = $novaLinha;
		 return($novaLinha);
	}
	
	/**
	 * Cria quadro para upload de arquivos dentro da instancia do Formulario
	 * @param string $destino local remoto para transferencia e armazenamento dos arquivos, 
	 * string $nameID atributo ID do button (file input), string $coluna coluna do BD para cadastrar o nome do arquivo após upload
	 * string $extensoes extensoes permitidas com separado '|'
	 * int $limiteArquivos número máximo de arquivos a serem enviados e armazenados, int $tamanhoMaximoMB tam. max. em MB permitido
	 * 
	 **/
	public function makeUploadFrame($destination,$nameID,$column='', $allowedExtensions = 'jp?g|bmp|png|txt|rar|zip|doc|xls|docx|xlsx',
	$maxAmount=5,$maxSizeMB=2 ) {
		 $novoQuadroUpload = new stdClass();
		 $novoQuadroUpload->destination = $destination;
		 $novoQuadroUpload->nameID = $nameID;
		 $novoQuadroUpload->column = $column;
		 $novoQuadroUpload->allowedExtensions = $allowedExtensions;
		 $novoQuadroUpload->maxAmount = $maxAmount;
		 $novoQuadroUpload->maxSizeMB = $maxSizeMB;
		 $this->uploadFrames[] = $novoQuadroUpload;
		 return($novoQuadroUpload);
	}
	
	/**
	 * Retorna um array com todos campos criados
	 * @param string $titulo : titulo do form a ser requerido os campos
	 * @return array stdClass campos
	 **/
	public function getFields(){
		$ret = false;
		if(is_array($this->lines)) {
			foreach($this->lines as $line) {
				foreach($line->columns as $column) {
					foreach($column->fields as $field) {
						$ret[] = $field;
					}
				}
			}
		}
		return($ret);	
	}
}
/**
 * Class Linha
 */ 
class Line  {
	public $columns=array();
	/**
	 * Cria uma coluna
	 * @param int $col_xs (largura em celulares), $col_md (tablets), $col_lg (pc e notebook)
	 **/
	public function makeColumn($col_xs=6, $col_md=6, $col_lg=6) {	
		$novaColuna = new Column($col_xs, $col_md, $col_lg);
		$this->columns[] = $novaColuna;
		return($novaColuna);
	}
}

/**
 * Class Coluna
 */ 
class Column  {
	public $col_xs;
	public $col_md;
	public $col_lg;
	public $fields=array();
	public function __construct($col_xs=6, $col_md=6, $col_lg=6) 
	{
		$this->col_xs = $col_xs;
		$this->col_md = $col_md;
		$this->col_lg = $col_lg;	
	}
	/**
	 * Cria um campo
	 * @param array $ar_campo : [{'titulo' : campo, 'tipo' : (moeda, data, numero, cep, cpf, cnpj,ie,senha), 'coluna' : '', 'value':''},...]
	 **/
	public function makeField($ar_campo) {	
		// Exemplos de preenchimento para o determinado tipo de dados
		if($ar_campo['example']=='') {
			// date, time, dateTime, number, currency, text, postal, cpf, cnpj, ie, password, phone, email
			switch($ar_campo['dataType']) {
				case "date" :
					$ar_campo['example'] = 'Ex. : 01/01/2020';
					$ar_campo['maxLength'] = 10;
					break;
				case "time" :
					$ar_campo['example'] = 'Ex. : 12:00';
					$ar_campo['maxLength'] = 5;
					break;
				case "dateTime" :
					$ar_campo['example'] = 'Ex. : 01/01/2020 12:00';
					$ar_campo['maxLength'] = 16;
					break;
				case "currency" :
					$ar_campo['example'] = 'Ex. : 1.000,00';
					$ar_campo['maxLength'] = 16;
					break;	
				case "postal" :
					$ar_campo['example'] = 'Ex. : 18.160-000';
					$ar_campo['maxLength'] = 10;
					break;
				case "cpf" :
					$ar_campo['example'] = 'Ex. : 999.999.999-00';
					$ar_campo['maxLength'] = 14;
					break;	
				case "cnpj" :
					$ar_campo['example'] = 'Ex. : 99.999.999/0001-00';
					$ar_campo['maxLength'] = 18;
					break;
				case "ie" :
					$ar_campo['example'] = 'Ex. : 999.999.999.999';
					$ar_campo['maxLength'] = 15;
					break;	
				case "phone" :
					$ar_campo['example'] = 'Ex. : (15) 9.9999-999 ou (15) 3033-4715';
					$ar_campo['maxLength'] = 16;
					break;	
				case "email" :
					$ar_campo['example'] = 'Ex. : teste@gmail.com.br';
					break;	
			}
		}
		// Convert Array para objeto StdClass (gambiarrex)
		$novoCampo = json_decode(json_encode($ar_campo));
		$this->fields[] = $novoCampo;
		
		// Retorna a referencia do campo recém criado (sempre a logica do ultimo array)
		return($this->fields[count($this->fields)-1]);
	}
}





?>
