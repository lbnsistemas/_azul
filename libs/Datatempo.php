<?php 
/*
	CLASSE: DATEINTERVAL
	EMPRESA: LBN DESENVOLVIMENTO DE SISTEMAS
	AUTOR: LUCAS F. B. NUNES
	EMAIL: suporte@lbsistemas
	CRIÇÃO: 22/03/2015
	ATUALIZAÇÃO: 
	DESC: ESTA CLASSE SIMULA DATEINTERVAL DO PHP 5.3
*/
class Datatempo {

    /**
	 * Retorna a diferença de anos, meses, dias, horas, minutos e segundos entre duas datas
	 * @param string $data1 format AAAA-MM-DD, string $data2 format AAAA-MM-DD
	 * @return object DateInterval {y->anos, m->meses, d->dias, h->horas, i->minutos, s->segundos}
	 */
    public function diferenca ( $data1, $data2) {
		//Convert as datas em segundos
		$time_datetime1 = strtotime($data1);
		$time_datetime2 = strtotime($data2);
		//Diferença de segundos entre as duas datas
		$time_to_convert = ($time_datetime2 - $time_datetime1);
		
        $FULL_YEAR = 60*60*24*365.25;
        $FULL_MONTH = 60*60*24*(365.25/12);
        $FULL_DAY = 60*60*24;
        $FULL_HOUR = 60*60;
        $FULL_MINUTE = 60;
        $FULL_SECOND = 1;

        $seconds = 0;
        $minutes = 0;
        $hours = 0;
        $days = 0;
        $months = 0;
        $years = 0;

        while($time_to_convert >= $FULL_YEAR) {
            $years ++;
            $time_to_convert = $time_to_convert - $FULL_YEAR;
        }

        while($time_to_convert >= $FULL_MONTH) {
            $months ++;
            $time_to_convert = $time_to_convert - $FULL_MONTH;
        }

        while($time_to_convert >= $FULL_DAY) {
            $days ++;
            $time_to_convert = $time_to_convert - $FULL_DAY;
        }

        while($time_to_convert >= $FULL_HOUR) {
            $hours++;
            $time_to_convert = $time_to_convert - $FULL_HOUR;
        }

        while($time_to_convert >= $FULL_MINUTE) {
            $minutes++;
            $time_to_convert = $time_to_convert - $FULL_MINUTE;
        }

        $seconds = $time_to_convert; // remaining seconds
		
		// Objeto de retorno
		$objRet = new stdClass();
        $objRet->y = $years;
        $objRet->m = $months;
        $objRet->d = $days;
        $objRet->h = $hours;
        $objRet->i = $minutes;
        $objRet->s = $seconds;
		return($objRet);
    }
	
	//Retorna o nome do mês ref. ao numero informado de 1 a 12
	public function tituloMes($n){
		$n = sprintf("%02d",$n);
		$mes["01"] = "Janeiro";
		$mes["02"] = "Fevereiro";
		$mes["03"] = "Março";
		$mes["04"] = "Abril";
		$mes["05"] = "Maio";
		$mes["06"] = "Junho";
		$mes["07"] = "Julho";
		$mes["08"] = "Agosto";
		$mes["09"] = "Setembro";
		$mes["10"] = "Outubro";
		$mes["11"] = "Novembro";
		$mes["12"] = "Dezembro";
		return($mes[$n]);
	}
	
	
	/**
	  * Retonar um objeto STD CLASS
	  * @params string $data uma data no padrão US(AAAA-MM-DD hh:ii:ss) ou BR(DD-MM-AAAA hh:ii:ss) 
	  * @return std object
	  */
	public function get_std_data($data) {
		//Meses
		$mesnome["01"] = "Janeiro";
		$mesnome["02"] = "Fevereiro";
		$mesnome["03"] = "Março";
		$mesnome["04"] = "Abril";
		$mesnome["05"] = "Maio";
		$mesnome["06"] = "Junho";
		$mesnome["07"] = "Julho";
		$mesnome["08"] = "Agosto";
		$mesnome["09"] = "Setembro";
		$mesnome["10"] = "Outubro";
		$mesnome["11"] = "Novembro";
		$mesnome["12"] = "Dezembro";
		//Dias da semana
		$diasemana["0"] = "Domingo";
		$diasemana["1"] = "Segunda-feira";
		$diasemana["2"] = "Terça-feira";
		$diasemana["3"] = "Quarta-feira";
		$diasemana["4"] = "Quinta-feira";
		$diasemana["5"] = "Sexta-feira";
		$diasemana["6"] = "Sabado";
		//Objeto StdClass para o retorno
		$ret = new stdClass();
		//Verifica se o parametro foi informado
		if (trim($data)!="") {
			// Extrai a hora da data
			$hora=(trim(substr($data,10,9))!="" ? substr($data,10,9)  : "");
			$data=str_replace($hora,"",$data);
			
			// Identifica o separador
			if (strpos($data,'-')>0) $sp = '-';
			if (strpos($data,'/')>0) $sp = '/';
			
			// Fatia a data
			$data_fat = explode($sp,$data);
			
			// Se estiver no padrão US
			if (strpos($data,$sp)==4) {
				$ano = $data_fat[0];
				$dia = $data_fat[2];
				$mes = $data_fat[1];	
				
			// Se estiver no padrão BR
			}else if (strpos($data,$sp)==2) {
				$ano = $data_fat[2];
				$dia = $data_fat[0];
				$mes = $data_fat[1];	
			}
			
			// Se a hora for informada :
			if(trim($hora)!='') {
				// Fatia a hora, min e segundo
				$hora = explode(':',$hora);
				$hor = $hora[0];
				$min = $hora[1];
				$seg = $hora[2];
			}
			
			// Propriedades da data [dia(nr, dia da semana), mês(nr, nome) e ano]
			$ret->dia 		= $dia;
			$ret->diaSemana = $diasemana[date('w', strtotime($data))];
			$ret->mes 		= $mes;
			$ret->mesNome 	= $mesnome[$mes];
			$ret->ano 		= $ano;
			// Propriedades do horário, caso seja informado
			$ret->hora		= $hor;
			$ret->minuto	= $min;
			$ret->seg		= $seg;
			
			//Retorna o objeto com as propriedades extraidas da data
			return($ret);
			
		}else {
			return(false);	
		}
	}
	

	/**
	* @autor: Carlos H. Reche
	* @data: 11/08/2004
	*/
	/* Devido à variação de dias entre os meses (pode ter 28, 29, 30 ou 31), o cálculo com diferenças entre timestamps nunca poderá ser exato, a não ser que o cálculo comece pelo número de dias (ou horas, minutos, segundos). Para minimizar ao máximo essa diferença, eu criei esta constante para utilizar durante o cálculo:
	*/
	public function converte_segundos($total_segundos, $inicio = 'Y') {	
		define('dias_por_mes', ((((365*3)+366)/4)/12) );
		
		$comecou = false;
		
		if ($inicio == 'Y')
		{
		$array['anos'] = floor( $total_segundos / (60*60*24* dias_por_mes *12) );
		$total_segundos = ($total_segundos % (60*60*24* dias_por_mes *12));
		$comecou = true;
		}
		if (($inicio == 'm') || ($comecou == true))
		{
		$array['meses'] = floor( $total_segundos / (60*60*24* dias_por_mes ) );
		$total_segundos = ($total_segundos % (60*60*24* dias_por_mes ));
		$comecou = true;
		}
		if (($inicio == 'w') || ($comecou == true))
		{
		$array['semanas'] = floor( $total_segundos / (60*60*24*7) );
		$total_segundos = ($total_segundos % (60*60*24*7 ));
		$comecou = true;
		}
		if (($inicio == 'd') || ($comecou == true))
		{
		$array['dias'] = floor( $total_segundos / (60*60*24) );
		$total_segundos = ($total_segundos % (60*60*24));
		$comecou = true;
		}
		if (($inicio == 'H') || ($comecou == true))
		{
		$array['horas'] = floor( $total_segundos / (60*60) );
		$total_segundos = ($total_segundos % (60*60));
		$comecou = true;
		}
		if (($inicio == 'i') || ($comecou == true))
		{
		$array['minutos'] = floor($total_segundos / 60);
		$total_segundos = ($total_segundos % 60);
		$comecou = true;
		}
		$array['segundos'] = $total_segundos;
		
		return $array;
	}
	//ADICIONA TANTOS DIAS DE UMA DATA
	public function addDayIntoDate($date,$days) {
		$thisyear = substr ( $date, 0, 4 );
		$thismonth = substr ( $date, 5, 2 );
		$thisday =  substr ( $date, 8, 2 );
		$nextdate = mktime ( 0, 0, 0, $thismonth, $thisday + $days, $thisyear );
		return strftime("%Y-%m-%d", $nextdate);

	}

	//SUBTRAI TANTOS DIAS DE UMA DATA
	public function subDayIntoDate($date,$days) {
		 $thisyear = substr ( $date, 0, 4 );
		 $thismonth = substr ( $date, 5, 2 );
		 $thisday =  substr ( $date, 8, 2 );
		 $nextdate = mktime ( 0, 0, 0, $thismonth, $thisday - $days, $thisyear );
		 return strftime("%Y-%m-%d", $nextdate);
	}
	
	//Funcao do JS - isBissexto (verifica se o ano inf. é bissexto
	public function isBissexto($ano){
		if ((($ano % 4 == 0) && ($ano % 100 != 0)) || ($ano % 400 == 0))  return (true);
		return(false);
	}
	
	//Funcao do JS - Compara duas datas, e diz se a primeira é maior que a segunda
	public function data1MaiorData2($data1,$data2){
		//Identifica separador
		if (strpos($data1,'-')>0) $sp = '-';
		if (strpos($data1,'/')>0) $sp = '/';
		
		//Primeira data
		$data1 = explode($sp,$data1);
		$ano1 = $data1[2];
		$mes1 = $data1[1];
		
		//Identifica separador
		if (strpos($data2,'-')>0) $sp = '-';
		if (strpos($data2,'/')>0) $sp = '/';
		$dia1 = $data1[0];
		//Segunda Data
		$data2 = explode($sp,$data2);
		$ano2 = $data2[2];
		$mes2 = $data2[1];
		$dia2 = $data2[0];
		//Datas em milessegundos
		$ms1= strtotime($ano1.'-'.$mes1.'-'.$dia1);
		$ms2= strtotime($ano2.'-'.$mes2.'-'.$dia2);
		//Compara os ms
		if($ms1>$ms2)return(true);	
		else return(false);
	}
	
	//Função do JS - datamensal
	public function dataMensal($data,$padSaida_US_BR){
		//IDENTIFICA SEPARADOR
		if (strpos($data,'-')>0) $sp = '-';
		if (strpos($data,'/')>0) $sp = '/';
		//FATIA A DATA 
		$data_fat = explode($sp,$data);
		//CASO SEJA PADRAO US
		if (strpos($data,$sp)==4) {
			$ano = $data_fat[0];
			$dia = $data_fat[2];
			$mes = $data_fat[1];	
		//CASO SEJA PADRAO BR
		}else if (strpos($data,$sp)==2) {
			$ano = $data_fat[2];
			$dia = $data_fat[0];
			$mes = $data_fat[1];
		}
		//PROXIMO MES
		if ($mes<12) {
			$proximo_mes = $mes+1;
			$proximo_ano = $ano;
		}else {
			$proximo_mes = 1;
			$proximo_ano = $ano+1;
		}
		//VERIFICA MESES COM 30 DIAS
		if ($dia==31 && ($proximo_mes==4 || $proximo_mes==6 || $proximo_mes==9 || $proximo_mes==11)) {
			$ultimo_dia = 30;
		}else {
			$ultimo_dia = 31;
		}
		//VERIFICA ANO BISSEXTO
		if ($dia>28 && $proximo_mes==2) {
			//VERIFICA SE O CORRENTE ANO É MULTIPLO DE 400
			if($proximo_ano%400==0) { 
				$ultimo_dia=29;
			//MULTIPLO DE 4 E NAO DE 100
			}else if($proximo_ano%4==0 && $proximo_ano%100!=0) {
				$ultimo_dia=29;
			}else {
				$ultimo_dia=28;
			}
		}
		//ACRESCENTA ZERO AO MESES MENORES QUE 10
		$proximo_dia = sprintf("%02d",$proximo_dia);
		$proximo_mes = sprintf("%02d",$proximo_mes);	
		
		//VERIFICA SE O DIA INFORMADO NAO É MAIOR QUE O ULTIMO DIA DO MES CONSEQUENTE
		if ($dia>$ultimo_dia) {
			$proximo_dia = $ultimo_dia;	
		}else {
			$proximo_dia = $dia;	
		}
		if($padSaida_US_BR=='BR') {
			return $proximo_dia.$sp.$proximo_mes.$sp.$proximo_ano;	
		}else {
			return $proximo_ano.$sp.$proximo_mes.$sp.$proximo_dia;		
		}
	}
	
	//Soma tantos dias a uma determinada data
	public function somaDias($data, $dias,$padSaida_US_BR) {    
		//HORA
		$data = explode(" ", $data);
		$hora = $data[1];
		//IDENTIFICA SEPARADOR
		if (strpos($data[0],'-')>0) $sp = '-';
		if (strpos($data[0],'/')>0) $sp = '/';
		
		//FATIA A DATA 
		$data_fat = explode($sp,$data[0]);
		//CASO SEJA PADRAO US
		if (strpos($data[0],$sp)==4) {
			$ano = $data_fat[0];
			$dia = $data_fat[2];
			$mes = $data_fat[1];	
		//CASO SEJA PADRAO BR
		}else if (strpos($data[0],$sp)==2) {
			$ano = $data_fat[2];
			$dia = $data_fat[0];
			$mes = $data_fat[1];
		}
		//Soma a quantidade de dias ao dia atual
		$diafuturo=$dia+$dias;    		
		//Acerta diferença de dias somados
		while($diafuturo>$this->nrDiasMes($mes,$ano)) {
			$diafuturo-=$this->nrDiasMes($mes,$ano);        
			$mes++;       
			if($mes>12) {            
				$mes=1;            
				$ano++;        
			}    
		}  
		//Acrescenta 0 em numeros com um digito na data
		$diafuturo = sprintf("%02d",$diafuturo);
		$mes = sprintf("%02d",$mes);
		//Hora
		$hora=(trim($hora)!="" ? $hora = " ".$hora : $hora="");
		//Retorno
		if($padSaida_US_BR=='BR') {
			return $diafuturo.$sp.$mes.$sp.$ano.$hora;	
		}else {
			return $ano.$sp.$mes.$sp.$diafuturo.$hora;		
		}
	}
	
	//FUnção para somar dias úteis
	public function somadiasUteis($data, $dias,$padSaida_US_BR) {
		//IDENTIFICA SEPARADOR
		if (strpos($data,'-')>0) $sp = '-';
		if (strpos($data,'/')>0) $sp = '/';
		
		//FATIA A DATA 
		$data_fat = explode($sp,$data);
		//CASO SEJA PADRAO US
		if (strpos($data,$sp)==4) {
			$ano = $data_fat[0];
			$dia = $data_fat[2];
			$mes = $data_fat[1];	
		//CASO SEJA PADRAO BR
		}else if (strpos($data,$sp)==2) {
			$ano = $data_fat[2];
			$dia = $data_fat[0];
			$mes = $data_fat[1];
		}
		//Soma a quantidade de dias ao dia atual
		$diafuturo=$dia+$dias;    		
		//Acerta diferença de dias somados
		while($diafuturo>$this->nrDiasMes($mes,$ano)) {
			$diafuturo-=$this->nrDiasMes($mes,$ano);        
			$mes++;       
			if($mes>12) {            
				$mes=1;            
				$ano++;        
			}    
		}  
	
		//se cair sabado -> soma 2 dias, se cair domingo -> soma 1 dia
		$diaSemana = date("w", mktime(0,0,0,$mes,$diafuturo,$ano));	
		
		//Caso seja sabado
		if ($diaSemana==6) {
			$diafuturo += 2;
		//Caso seja domingo	
		}else if ($diaSemana==0) {
			$diafuturo += 1;
		}
		
		if($diaSemana==6 || $diaSemana==0){
			//Acerta diferença de dias somados
			while($diafuturo>$this->nrDiasMes($mes,$ano)) {
				$diafuturo-=$this->nrDiasMes($mes,$ano);        
				$mes++;       
				if($mes>12) {            
					$mes=1;            
					$ano++;        
				}    
			}  
		}
	
    	//Acrescenta 0 em numeros com um digito na data
		$diafuturo = sprintf("%02d",$diafuturo);
		$mes = sprintf("%02d",$mes);
		//Hora
		$hora=(trim($hora)!="" ? $hora = " ".$hora : $hora="");
		//Retorno
		if($padSaida_US_BR=='BR') {
			return $diafuturo.$sp.$mes.$sp.$ano.$hora;	
		}else {
			return $ano.$sp.$mes.$sp.$diafuturo.$hora;		
		}
	}

	//Retorna a quantidade de dias em determina mes
	public function nrDiasMes($mes,$ano) {  
		if(($mes<8 && $mes%2==1) || ($mes>7 && $mes%2==0))return 31;    
		if($mes!=2) return 30;  
		//VERIFICA SE O CORRENTE ANO É MULTIPLO DE 400
		if($ano%400==0) return 29;
		//MULTIPLO DE 4 E NAO DE 100
		if($ano%4==0 && $ano%100!=0) return 29;
		//NAO É BISSEXTO
		return 28;
	}
	

	
	//Retorna o ultimo dia do mês do ano informado
	public function ultimoDiaMes($ano='',$mes=''){
		if($ano=='') $ano = date('Y');
		if($mes=='') $mes = date('m');	
					
		//VERIFICA MESES COM 30 DIAS
		if ($mes==4 || $mes==6 || $mes==9 || $mes==11) {
			$ultimo_dia = 30;
		//SE FOR MES 2 VERIFICA ANO BISSEXTO
		}else if ($mes==2) {
			//VERIFICA SE O CORRENTE ANO É MULTIPLO DE 400
			if($ano%400==0) { 
				$ultimo_dia=29;
			//MULTIPLO DE 4 E NAO DE 100
			}else if($ano%4==0 && $ano%100!=0) {
				$ultimo_dia=29;
			}else {
				$ultimo_dia=28;
			}
		}else {
			$ultimo_dia = 31;
		}
		
		//RETORNA O ULTIMO DIA DO MES
		return $ultimo_dia;
		
	}
	
	/**
	 * Valida data e hora 
	 * @param  string $dataHora uma data com uma possivel hora
	 * @return bool
	 */
	public function validaData($dataHora){
		//IDENTIFICA SEPARADOR
		if (strpos($dataHora,'-')>0) $sp = '-';
		if (strpos($dataHora,'/')>0) $sp = '/';
		//FATIA A DATA 
		$data_fat = explode(" ",$dataHora);
		$data_fat = explode($sp,$data_fat[0]);
		$hora = $data_fat[1];
		//CASO SEJA PADRAO US
		if (strpos($dataHora,$sp)==4) {
			$ano = $data_fat[0];
			$dia = $data_fat[2];
			$mes = $data_fat[1];	
		//CASO SEJA PADRAO BR
		}else if (strpos($dataHora,$sp)==2) {
			$ano = $data_fat[2];
			$dia = $data_fat[0];
			$mes = $data_fat[1];
		}
		$validaHora = true;	
		if(trim($hora)!="") {
			$h = trim(substr($hora,0,2));
			$m = trim(substr($hora,3,2));
			$s = trim(substr($hora,5,2));
			if(strlen($h)==2 && strlen($m)==2 && strlen($s)==2) {
				if((int)$h >=0 && (int)$h < 24 && (int)$m >=0 && (int)$m <60 && (int)$s >=0 && (int)$s <= 60 ) {
					$validaHora = true;	
				} else {
					$validaHora = false;		
				}
			}else {
				$validaHora = false;		
			}
		}
		
		//Verifica apenas a data
		$validaData = checkdate($mes, $dia, $ano);
		
		//Verifica se data e hora foram validadas
		if($validaHora==true && $validaHora==true){
			return(true);	
		}
		return(false);
	}
	
	/**
	 * Converte data padrão EN para BR e vice-versa
	 * @param string $data, string $sepSaida separado de data ('-' ou '/')
	 * @return string data convertida
	 */
	public function convertData($data,$sepSaida='-') {
		if (trim($data)!="") {
			//IDENTIFICA SEPARADOR
			if (strpos($data,'-')>0) $sp = '-';
			if (strpos($data,'/')>0) $sp = '/';
			//FATIA A DATA 
			$data_fat = explode(" ",$data);
			$hora = $data_fat[1];
			$data_fat = explode($sp,$data_fat[0]);
			//CASO SEJA PADRAO US
			if (strpos($data,$sp)==4) {
				$ano = $data_fat[0];
				$dia = $data_fat[2];
				$mes = $data_fat[1];	
				$retorno = $dia.$sepSaida.$mes.$sepSaida.$ano;//RETORNO FORMATO BR
			//CASO SEJA PADRAO BR
			}else if (strpos($data,$sp)==2) {
				$ano = $data_fat[2];
				$dia = $data_fat[0];
				$mes = $data_fat[1];
				$retorno = $ano.$sepSaida.$mes.$sepSaida.$dia;//RETORNO FORMATO US	
			}
			if(trim($hora)!="") {
				$h = sprintf("%02d",trim(substr($hora,0,2)));
				$m = sprintf("%02d",trim(substr($hora,3,2)));
				$s = sprintf("%02d",trim(substr($hora,5,2)));
				$hora  = $h.":".$m.":".$s;
				$hora = " ".$hora;
			}
			return $retorno.$hora;
		} else return "";	
	}
}

?>
