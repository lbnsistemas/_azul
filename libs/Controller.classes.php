<?php
require_once(SYS_SRC_MODELS.'mMain.php');
require_once(SYS_SRC_LIBS.'Datatempo.php');
require_once(SYS_SRC_LIBS.'File.php');
require_once(SYS_SRC_LIBS.'File/Image.php');
require_once(SYS_SRC_LIBS.'File/Upload.php');
require_once(SYS_SRC_LIBS.'PHPMailer/class.phpmailer.php');
require_once(SYS_SRC_LIBS.'PHPMailer/class.smtp.php');
/**
 * Classe Campos : Cria e manipula diversos objetos stdClass (campos),
 * necessário para compor os argumentos das ações INSERIR e ALTERAR
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 1.0.0
 */
class Fields {
	// Array para conter os objetos stdClass de cada campo criado
	private $arrayFields = array();

	/**
	 * Inicia a classe com os campos informados no construtor
	 * @param object StdClass $campos
	 * @return void
	 */
	function __construct ($campos=array()){
		$this->arrayFields = $campos;
	}

	/**
	 * Cria e adiciona um novo objeto stdClass(campo) ao array $arrayCampos
	 * @param string $col nome da coluna, string $val value da coluna
	 * @return void
	 */
	public function add($col, $val) {
		if($col!='' && $val!==false) {
			$obj = new StdClass();
			$obj->column = $col;
			$obj->value = $val;
			array_push($this->arrayFields,$obj);

			$obj = null;

		}else return(false);
	}
	/**
	 * @return $arrayCampos ou false
	 */
	public function ret() {
		if(count($this->arrayFields)>0) {
			return($this->arrayFields);
		}else return(false);
	}
}
