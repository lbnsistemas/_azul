<?php
require_once(SYS_SRC_LIBS.'Database/aDatabase.php');
/**
 * Classe que manipula BD e registra alterações em LOG 
 * 
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 1.1.0
 */ 
class Database extends aDatabase { 
	private $mysqli;
	private $rows;
	private $stringSQL;
	private $errorSQL;
	/**
	 * Contruct
	 */
	public function __construct() 
	{
		//Caso Database seja instânciada, inicia a conexão automáticamente
		$this->connect();	
	}
	
	/**
	 * Retorna a ultima string executada no BD
	 * @return string 
	 */
	protected function get_stringSQL() {
		return($this->stringSQL);	
	}
	
	/**
	 * Retorna a ultima string executada no BD
	 * @return string 
	 */
	protected function get_errorSQL() {
		return($this->errorSQL);	
	}
	
	/**
	 * @abstract
	 */
	protected function connect()
	{
		$this->mysqli = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		$this->mysqli->set_charset('utf8');
		if($this->mysqli->connect_errno) {
			trigger_error('Falha na conexão ao banco de dados. Erro: '.$this->mysqli->connect_errno,E_USER_WARNING);
		}else 
		if($this->mysqli->ping()==false) {
			trigger_error('Servidor da base de dados está fora',E_USER_WARNING);
		}	
	}
	/**
	 * @abstract
	 */
	protected function disconnect()
	{
		$this->mysqli->close();
	}
	/**
	 * @abstract
	 */
	protected function writeLog($string, $erro=false)
	{	
		//Log de erros nas querys mysql
		if($erro){
			$fp = fopen(SYS_FILE_LOG_ERROR_MYSQL, "a");
		//Log de querys mysql executadas com sucesso
		}else {
			$fp = fopen(SYS_FILE_LOG_MYSQL, "a");
		}
		//Cabeçalho
		$logHeader  = "[".date('Y-m-d H:i:s')."] ";
		$logHeader .= ($_SESSION[SES_USER_NAME]!='' ? " [".$_SESSION[SES_USER_NAME]."] " : '');
		$logHeader .= '['.SYS_USER_IP.'] ['.$_SERVER['SCRIPT_FILENAME'].'] ';
		//Conteudo do log
		$logConteudo = $logHeader."\r\n".$string."\r\n";
		fwrite($fp, $logConteudo);
		fclose($fp);
		return(true);
	}
	
	/**
	 * @abstract
	 */
	protected function getInstanceMysqli()
	{
		return $this->mysqli;
	}
	/**
	 * @abstract
	 */
	protected function rows()
	{
		return $this->rows;
	}
	/**
	 * @abstract
	 */
	protected function id()
	{
		return $this->mysqli->insert_id;
	}
	/**
	 * @abstract
	 */
    protected function select($table,$compl='',$cols='*')
	{
		
		$cols = trim($cols)=='' ? '*' : $this->mysqli->escape_string($cols);
		$stringMysql = "SELECT $cols FROM ".$this->mysqli->escape_string($table)." $compl";
		//die($stringMysql.'');
		//die($stringMysql);
		$this->stringSQL = $stringMysql;//Salva a string SQL para uma possível consulta;
		$result = $this->mysqli->query($stringMysql,MYSQLI_USE_RESULT);
		$linhas = array();
		if($this->mysqli->errno) {
			$errorString = $this->mysqli->error." - String : ".$stringMysql;
			$this->errorSQL = $this->mysqli->error;//Salva a msg de erro para uma possivel consulta
			$this->writeLog($errorString,true);//Registra erro no log errosmysql
			trigger_error($errorString ,E_USER_WARNING);
			return(false);			
		}
		while($row = $result->fetch_assoc()) {
			$linhas[] = $row;	
		}
		$this->rows = count($linhas);
		$result->close();
		return($linhas);
	}
	/**
	 * @abstract
	 */
	protected function delete($table, $col_id,  $id)
	{
		$stringMysql = "DELETE FROM ".$this->mysqli->escape_string($table)." WHERE ";
		if(is_array($id)) {
			foreach($id as $i) {
				$stringMysql .= 	$this->mysqli->escape_string($col_id)."=".$this->mysqli->escape_string($i)." OR";
			}
			$stringMysql = substr($stringMysql,0,-3);
		}else {
			$stringMysql .= $this->mysqli->escape_string($col_id)." = ".$this->mysqli->escape_string($id);
		}

		$this->stringSQL = $stringMysql;//Salva a string SQL para uma possível consulta;
		$this->mysqli->query($stringMysql,MYSQLI_USE_RESULT);
		if($this->mysqli->errno) {
			$errorString = $this->mysqli->error." - String : ".$stringMysql;
			$this->errorSQL = $this->mysqli->error;//Salva a msg de erro para uma possivel consulta
			$this->writeLog($errorString,true);//Registra erro no log errosmysql
			trigger_error($errorString ,E_USER_WARNING);
			return(false);			
		}
		$this->rows = $this->mysqli->affected_rows;
		$this->writeLog($stringMysql);//Registra delete logo mysql
		return(true);
	}
	/**
	 * @abstract
	 */
	protected function update($table, $dados, $col_id,  $id, $writeLog = true)
	{
		$stringMysql = "UPDATE ".$this->mysqli->escape_string($table)." SET ".$dados
		." WHERE ";
		// Verifica se foi informado um array com diversos IDs selecionados para edição
		if(is_array($id)) {
			for($c = 0 ; $c < count($id); $c++) {
				$i = $id[$c];
				if(trim($i) == '') continue;
				$stringMysql .= $this->mysqli->escape_string($col_id)." = ".$this->mysqli->escape_string($i);
				// Se ainda não for o ultimo ID, acrescenta o operador OR
				if($c < count($id)-1) {
					$stringMysql .= ' OR '	;
				}
			}	
		// Único ID selecionado para edição
		}else {
			$stringMysql .= $this->mysqli->escape_string($col_id)." = ".$this->mysqli->escape_string($id);	
		}
		$this->stringSQL = $stringMysql;//Salva a string SQL para uma possível consulta;
		$this->mysqli->query($stringMysql,MYSQLI_USE_RESULT);
		if($this->mysqli->errno) {
			$errorString = $this->mysqli->error." - String : ".$stringMysql;
			$this->errorSQL = $this->mysqli->error;//Salva a msg de erro para uma possivel consulta
			$this->writeLog($stringMysql,true);//Registra erro no log errosmysql
			trigger_error($errorString ,E_USER_WARNING);
			return(false);			
		}
		$this->rows = $this->mysqli->affected_rows;
		if($writeLog) {
			$this->writeLog($stringMysql);//Registra update logo mysql
		}
		return(true);
	}
	/**
	 * @abstract
	 */
	protected function insert($table,  $dados, $writeLog = true)
	{
		$stringMysql = "INSERT INTO ".$this->mysqli->escape_string($table)." SET ".$dados;
		$this->stringSQL = $stringMysql;//Salva a string SQL para uma possível consulta;
		$this->mysqli->query($stringMysql,MYSQLI_USE_RESULT);
		if($this->mysqli->errno) {
			$errorString = $this->mysqli->error." - String : ".$stringMysql;
			$this->errorSQL = $this->mysqli->error;//Salva a msg de erro para uma possivel consulta
			$this->writeLog($errorString,true);//Registra erro no log errosmysql
			trigger_error($errorString ,E_USER_WARNING);
			return(false);			
		}
		$this->rows = $this->mysqli->affected_rows;
		if($writeLog) {
			$this->writeLog($stringMysql);//Registra insert logo mysql
		}
		return(true);
	}
	/**
	 * @abstract
	 */
	protected function transaction($sqlStrings,$writeLog=true)
	{
		if(is_array($sqlStrings)) {
			$stringsMysql = '';
			$this->mysqli->query("SET AUTOCOMMIT=0");
			$this->mysqli->query("START TRANSACTION"); 
     		$this->mysqli->query("BEGIN"); 
			// Lê e executa todas strings sql
			foreach($sqlStrings as $string) {
				$this->stringSQL .= $string.";/r/n/r/n";//Salva a string SQL para uma possível consulta;
				$this->mysqli->query($string,MYSQLI_USE_RESULT);
				if($this->mysqli->errno) {
					$errorString = $this->mysqli->error." - String : ".$string;
					$this->errorSQL = $this->mysqli->error;//Salva a msg de erro para uma possivel consulta
					$this->writeLog($errorString,true);//Registra erro no log errosmysql
					$this->mysqli->query("ROLLBACK");// Desfaz a corrente transaction
					$this->mysqli->query("SET AUTOCOMMIT=1");//Retorna autommit ao valor default
					//trigger_error($errorString ,E_USER_WARNING);
					return(false);			
				}
				$stringsMysql .= $string."\r\n\r\n";
			}
			$this->mysqli->query("COMMIT"); 
			$this->mysqli->query("SET AUTOCOMMIT=1");//Retorna autommit ao valor default
			$this->rows = $this->mysqli->affected_rows; // Nr. total de linhas afetadas
			if($writeLog) {
				$this->writeLog($stringsMysql);//Registra insert logo mysql
			}
			return(true);
		}else {
			trigger_error("\$sqlStrings não é um array como esperado na função Database::transaction." ,E_USER_WARNING);
			return(false);
		}
	}
	
}