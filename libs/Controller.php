<?php
require_once(SYS_SRC_LIBS.'Controller.classes.php');
/**
 * Classe que fornece as funções principais para todos os controllers do sistema
 *
 * @author Lucas Nunes <suporte@lbnsistemas.com.br> | http://www.lbnsistemas.com.br/
 * @copyright 2016 direitos reservados, Lucas Nunes
 * @version 3.5.5
 */
class Controller extends mMain  {
	// Last action status
	private $statusLastAction='';
	// Return the last action status
	public function statusLastAction() {
		return $this->statusLastAction;
	}
	// When a function (action) called not exists
	public function __call($name,$args) {
		$this->azReturn('Ação inválida. Name: '.$name,'fail',true,$name);
	}
	// Validade package controller
	public function isControllerRequest() {
		// Check is controller request
		if(isset($_POST['envelopeJSON'])==false || isset($_POST['controller'])==false || isset($_POST['view'])==false
		|| isset($_POST['action'])==false || isset($_POST['application'])==false || isset($_POST['module'])==false) return(false);
		// Validade JSON string in envelope
		if(!$json_test = json_decode($_POST['envelopeJSON'])) return(false);
		// Validation complete, All right!
		return(true);
	}
	/**
	 * Deleta arquivos e depois a pasta
	 * @param string $dir: caminho do diretorio
	 * @return boolean
	 */
	public static function delTree($dir) { 
	   if(is_dir($dir)) {
		   $files = array_diff(scandir($dir), array('.','..')); 
			foreach ($files as $file) { 
			  (is_dir("$dir/$file")) ? Controller::delTree("$dir/$file") : unlink("$dir/$file"); 
			} 
			return rmdir($dir);
	   }else {
			return(false);   
	   }
		
	} 
	/**
	 * Gera um código randomico de x posições com números ou demais caracteres
	 * @param string $length : tamanho do código
	 * @param bool $allowChars: permite ou não utilização de letras no código
	 * @return string : tipo de dados
	 */
	public function generateCode ($length = 20,$allowChars=false)
	{
		$nps = "";
		for($i=0;$i<$length;$i++)
		{
			if($allowChars) {
				$nps .= chr( (mt_rand(1, 36) <= 26) ? mt_rand(97, 122) : mt_rand(48, 57 ));
			}else {
				$nps .= mt_rand(0, 9);
			}
		}
		return $nps;
	}
	/**
	 * Pega o tipo de $dados informado no parametro
	 * tipos possíveis : date, datetime, data, dataHora, hora, float, moeda, cep, cpf, cnpj, fone, numero, texto, ie, email
	 * @param string $data : dados a serem analisados
	 * @return string : tipo de dados
	 */
	public function getDataType($data) {
		$dataType = false;

		// Testa se é do tipo date
		$exp = "/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/";
		if(preg_match($exp, $data)) {
			return('date');
		}

		// Testa se é do tipo data
		$exp = "/^[0-9]{2}[\/\-][0-9]{2}[\/\-][0-9]{4}$/";
		if(preg_match($exp, $data)) {
			return('data');
		}

		// Testa se é do tipo datetime
		$exp = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/";
		if(preg_match($exp, $data)) {
			return('datetime');
		}

		// Testa se é do tipo dataHora
		$exp = "/^[0-9]{2}[\/\-][0-9]{2}[\/\-][0-9]{4} [0-9]{2}:[0-9]{2}:?[0-9]{2}?$/";
		if(preg_match($exp, $data)) {
			return('dataHora');
		}

		// Testa se é do tipo hora
		$exp = "/^[0-9]{2}:[0-9]{2}:?[0-9]{2}?$/";
		if(preg_match($exp, $data)) {
			return('hora');
		}

		// Testa se é do tipo float
		$exp = "/^[0-9]*\.[0-9]{2,4}$/";
		if(preg_match($exp, $data)) {
			return('float');
		}

		// Testa se é do tipo moeda
		$exp = "/^(\d{1,3}\.)+\d{1,3},\d{2,4}$/";
		if(preg_match($exp, $data)) {
			return('moeda');
		}
	}
	/**
	 * Altera o objeto campo dentro do array informado, atribuindo um novo value
	 * @param array Object $arFields : array a ter o objeto alterado
	 * @param string $column_nameID : nome da coluna(az-column) ou ID do campo
	 * @return void
	 */
	public function setValue(&$arFields, $column_nameID,$value) {
		if(is_array($arFields)) {
			foreach($arFields as $field) {
				if($field->column==$column_nameID || $field->nameID == $column_nameID) {
					$field->value = $value;
					break;
				}
			}
		}
	}
	/**
	 * Retorna o value de um campo dentro do array de campos enviados ao WS
	 * @param array Object $arFields
	 * @param string $column_nameID : nome da coluna (az-column) ou ID do campo
	 * @return string value
	 */ 
	public function getValue($arFields, $column_nameID) {
		$value = false;
		if(is_array($arFields)) {
			foreach($arFields as $field) {
				if($field->column==$column_nameID || $field->nameID == $column_nameID) {
					$value = is_array($field->value)  ? $field->value : urldecode($field->value);
					break;
				}
			}
		}
		return($value);
	}
	/**
	 * Retorna uma Instancia ativa da classe mysqli com a última conexao válida
	 * @return Instance of mysqli conected
	 */
	public function getMysqli() {
		Database::connect();
		return(Database::getInstanceMysqli());
	}
	/**
	 * Inicia classe File e realiza leitura dos arquivos de determinada pasta
	 * @param string $pasta caminho para os arquivos
	 * @return array  [{name,extension,thumbnail},...]
	 */
	public function readFiles($folder){
		$file = new File();
		// Seta o diretorio de trabalho
		if($file->setWorkDir(SYS_SRC_APP.$folder)==true && $file->setUrlWorkDir(SYS_APP_URL.$folder)) {
			// Verifica se existem arquivos válidos
			if($files = $file->getFilesDir()) {
				return($files);
			}
		// Diretorio inválido
		}else {
			return(false);
		}
	}
	/**
	 * Verifica se um dado nome da arquivo faz referencia com um thumbnail em uma det. pasta
	 * @param string #fileName:string nome do arquivo com extensao
	 * @param $folder:string -> caminho relativo ou real para os arquivos
	 * @return array  [url arquivo, url thumbnail]
	 */
	public function hasThumbnail($fileName,$folder){
		$files = $this->readFiles($folder); // Todos arquivos da pasta
		if($files!==false && is_array($files)==true) {
			foreach($files as $file) {
				//die($file['name'].".".$file['extension']." ---- ".$fileName);
				if($file['name'].".".$file['extension']==$fileName && $file['thumbnail']!='') {
					// Retorna url do arquivo e url do thumbnail
					return(array('url'=>$file['url'],'thumbnail'=>$file['thumbnail']));
				}
			}
		}
		// Erro na leitura dos arquivos ou não existe um thumbnail para o arquivo informado
		return(false);
	}
	/**
	 * Deleta um determinado arquivo, checado todas as permissões
	 * @param stdClass $args : {fileName, pasta, view, webService}
	 * @return string json mensagem sucesso  ou falha
	 */
	public function delete_file($args,$chkPermission=true) {
		if($this->checksPermission('D',$args->view,$args->secureAdvanced)===true || $chkPermission==false){
			$file = new File();
			// Seta o diretorio de trabalho
			if($file->setWorkDir(SYS_SRC_APP.$args->folder)==true && $file->setUrlWorkDir(SYS_APP_URL.$args->folder)==true) {
				$ret = $file->deleteFile($args->fileName);
				
				//die(SYS_SRC_APP.$args->folder.$args->fileName);
				
				//Exclusão efetuada com sucesso
				if($ret) {
					// Excluí o thumbnail se houver
					$name = pathinfo($args->fileName, PATHINFO_FILENAME );
					$ext = pathinfo($args->fileName, PATHINFO_EXTENSION );
					$retThumbnail = $file->deleteFile($name."-thumbnail.".$ext);
					// Retorno
					return($this->azReturn('','win',$args->webService,'del-thumbnail : '.$retThumbnail));
				//Falha na Exclusão
				}else {
					
					return($this->azReturn('Arquivo não deletado devido a uma falha interna.','fail',$args->webService, $ret));
				}
				
				
			// Erro de diretórios
			}else {
				return($this->azReturn('Falha na abertura do diretório.','fail',$args->webService,SYS_SRC_APP.$args->folder));
			}
		}else {
			return($this->azReturn('Permissão para deletar não cadastrada, verifique o cadastro de usuários.','fail',$args->webService));
		}
	}
	/**
	 * Concluí upload de arquivo (recepção)
	 * @param stdClass $envelope : {acao:'upload', PK : (chave primaria da tabela principal),
	 *  array arquivos, int tamanhoMaxMB, bool alteraNomeArquivo, array extensoesPermitidas, bool thumbnail}
	 * @return string json mensagem sucesso(dados adc. : name, extension, thumbnail, status) ou falha
	 */
	public function uploadFile($envelope) {
		// Inicia classe para Upload de arquivos, passando como parametro o objeto stdClass $envelope
		$upload = new Upload($envelope);
		// Processa o(s) arquivo(s) enviados, seguindo as regras de processamento setadas no inicio da classe
		$ret = $upload->processFiles();
		// Se o retorno não for um array, houve erro de processamento
		if(!is_array($ret)) {
			return($this->azReturn('Falha na tentativa de upload de arquivo.','fail',$envelope->webService,$ret));
		// Sucesso no Upload!
		}else {
			$qtdArquivos = count($ret);
			$qtdFalha = 0;
			$msg = '';
			foreach($ret as $file) {
				if(!$file[0]['status_upload']){
					$qtdFalha++;
				}
			}
			// Caso todos fracassado, retorna falha
			if($qtdArquivos==$qtdFalha) {
				$type = 'fail';	
				$msg = 'Falha em todos os envios.';
			// Se um ou mais fracassaram mas um teve êxito, retorno info
			}else if($qtdFalha>0) {
				$type = 'info';	
				$msg = $qtdFalha.' falha(s) de envio.';
			// Se todos tiveram sucesso, retorna sucesso
			}else {
				$type = 'win';	
			}
			return($this->azReturn($msg,$type,$envelope->webService,$ret));
		}
	}

	/**
	 * retornar a ultima PK reservada na tabela temp_ids ou reserva uma nova
	 * @param string $tabela, string $col_id coluna da PK, string $view, bool webservice
	 * @param bool secureAdvanced segurança avançada para validação de action e acesso
	 * @return string json mensagem sucesso ou falha
	 */
	public function findReserveID($table,$col_id,$view, $webservice=false,$secureAdvanced=true) {
		$chkAcessPermission = $this->checksPermission('V',$view,$secureAdvanced);
		// Verifica se está logado e possui permissão
		if($chkAcessPermission===true){
			$ret = mMain::reservedID($table,$col_id);
			//Erro de execução
			if(!$ret) {
				return($this->azReturn('Falha no processamento da solicitação.','fail',$webservice));
			//ID encontrado/reservado com sucesso
			}else if($ret > 0) {
				return($this->azReturn('','win',$webservice,$ret));

			//ID não encontrado/reservado
			}else {
				return($this->azReturn('ID não encontrado/reservado. Tente novamente.','fail',$webservice,$ret));
			}
		// Verifica se o usuário está deslogado
		}else if($chkAcessPermission==-1) {
			return($this->azReturn('Seu acesso expirou, realize o login novamente.','info',$webservice,$chkAcessPermission));
		// Sem permissão de visualização(==false)
		}else {
			return($this->azReturn('Você não tem permissão de visualização nesta página.','info',$webservice,$chkAcessPermission));
		}
	}
	/**
	 * Inclui, através da model, um det. tipo de cadastro no BD
	 * @param  array objects stdClass $args : campos no formato : [{nameID, value, tipoDados},...]
	 * @return string json mensagem sucesso ou falha
	 */
	public function azInsert($args, $chkPermission=true) {
		if($this->checksPermission('I',$args->view,$args->secureAdvanced)===true || $chkPermission==false){
			// Tratamento de caracteres especiais
			foreach($args->fields as $key=>$field){
				$args->fields[$key]->value = urldecode($field->value);
			}
			$objRet = mMain::mInsert($args);
			//Erro de execução
			if(!$objRet) {
				return($this->azReturn('Falha no processamento da solicitação.','falha',$args->webService,$objRet));

			//Inclusão efetuada com sucesso
			}else if($objRet->ret==true) {
				return($this->azReturn($args->msgWin,'win',$args->webService,$objRet));

			//Falha na inclusão
			}else if($objRet->ret==false) {
				return($this->azReturn($args->msgWin,'fail',$args->webService,$objRet));

			//Sessão expirada
			}else if($objRet->ret==-1) {
				return($this->azReturn('Seu acesso expirou, realize o login novamente.','info',$args->webService,$objRet));
			}
		}else {
			return($this->azReturn('Permissão para inclusão não cadastrada, verifique o cadastro de usuários.','fail',$args->webService));
		}
	}
	/**
	 * Alterar, através da model, um det. tipo de cadastro no BD
	 * @param object $dados linhas a serem inseridas + ID
	 * @param int ID Primary key do cadastro
	 * @param model $model, default : mMain
	 * @return string json mensagem sucesso + ultimo ID gerado ou falha
	 */
	public function azUpdate($args, $model = '', $chkPermission=true) {
		if($this->checksPermission('A',$args->view,$args->secureAdvanced)===true || $chkPermission==false){
			// Tratamento de caracteres especiais
			foreach($args->fields as $key=>$field){
				$args->fields[$key]->value = urldecode($field->value);
			}
			// Utiliza a model informada no parametro ou a principal :
			$objRet = $model=='' ?  mMain::mUpdate($args) : $model->mUpdate($args);
			//Erro de execução
			if(!$objRet) {
				return($this->azReturn('Falha no processamento da solicitação.','fail',$args->webService,$objRet));
			//Inclusão efetuada com sucesso
			}else if($objRet->ret==true) {
				return($this->azReturn($args->msgWin,'win',$args->webService,$objRet));

			//Sessão expirada
			}else if($objRet->ret==-1) {
				return($this->azReturn('Seu acesso expirou, realize o login novamente.','info',$args->webService,$objRet));

			//Falha na inclusão
			}else if($objRet->ret==false) {
				return($this->azReturn($args->msgFail,'fail',$args->webService,$objRet));
			}
		}else {
			return($this->azReturn('Permissão para alteração não cadastrada, verifique o cadastro de usuários.','fail',$args->webService));
		}
	}
	/**
	 * !OBSOLETO! [Está função será excluída em versões futuras]
	 *
	 *
	 * Deletar, através da model, um det. tipo de cadastro no BD
	 * @param int $ID PK
	 * @return string json mensagem sucesso  ou falha
	 */
	public function azDelete($args, $chkPermission=false) {
		if($this->checksPermission('D',$args->view,$args->secureAdvanced)===true || $chkPermission==false){
			$objRet = mMain::mDelete($args);
			//Erro de execução
			if(!$objRet) {
				return($this->azReturn('Falha no processamento da solicitação.','fail',$args->webService,$objRet));
			//Exclusão efetuada com sucesso
			}else if($objRet->ret['rows'] > 0) {
				return($this->azReturn($args->msgWin,'win',$args->webService,$objRet));
			//Falha na Exclusão
			}else {
				return($this->azReturn($args->msgFail,'fail',$args->webService,$objRet));
			}
		}else {
			return($this->azReturn('Permissão para deletar não cadastrada, verifique o cadastro de usuários.','fail',$args->webService));
		}
	}
	/**
	 * Seleciona, através da model, uma listagem de cadastros
	 * @param stdClass $pacote stdClass :  tabela, inicio, limit, orderBy, filtros(value1, value2, colunas, regra), view, colunas, complSQL, webService
	 * @param model $model, default : mMain
	 * @return string json mensagem sucesso + as linhas encontradas ou falha
	 */
	public function azView($args, $model = '', $chkPermission=true) {
		if($this->checksPermission('V',$args->view,$args->secureAdvanced)===true ||  $chkPermission==false){
			// Prepara as colunas visíveis na View (iniciadas com V_), caso forem solicitadas
			if(is_array($args->columns)) {
				$columns = '';
				foreach($args->columns as $alias) {
					/* Verifica se possuí a inicial 'V_' (coluna visivel na listagem da View)
					if(strpos($alias,'V_')!==false) {
						$col = substr($alias,2,strlen($alias));
						$colunas .= $col." ".$alias.", ";
					// Coluna não vísivel na View
					}else {
						$colunas .= $alias.", ";
					}*/
					$columns .= $alias.", ";
				}
				$args->columns = $columns;
				// Retira o ultimo ', '
				if(substr($args->columns,-2) == ", " ) $args->columns = substr($args->columns,0,-2);
			}
			// Utiliza a model informada no parametro ou a principal :
			$objRet = $model=='' ?  mMain::mSelect($args) : $model->mSelect($args);

			//Erro de execução
			if(!$objRet) {
				return($this->azReturn('Falha no processamento da solicitação.','fail',$args->webService,$objRet));
			//Linhas encontradas
			}else if($objRet->ret!=false) {
				// Retorna as linhas encontradas no padrão JSON
				return($this->azReturn('','win',$args->webService,$objRet));
			//Nenhuma linha encontrada com a a consultado informada
			}else if($objRet->ret==false) {
				$msgFail = $args->msgFail!='' ? $args->msgFail : 'Nenhum cadastro foi encontrado.';
				return($this->azReturn($msgFail,'info',$args->webService,$objRet));
			}
		}else {
			return($this->azReturn('Permissão para visualizar não cadastrada, verifique o cadastro de usuários.','fail',$args->webService));
		}
	}
	/**
	 * Registra uma ação executada pelo framework ( se está função for chamada dentro da action )
	 *
	 * @return string json mensagem sucesso + as linhas encontradas ou falha
	 */
	public function registerAction() {
		// Utiliza a model principal
		$objRet = mMain::registerAction($args);
		//Erro de execução
		if(!$objRet) {
			return($this->azReturn('Falha no registro da ação.','fail',false,$objRet));
		//Linhas encontradas
		}else if($objRet->ret!=false) {
			// Retorna as linhas encontradas no padrão JSON
			return($this->azReturn('','win',false,$objRet));
		//Nenhuma linha encontrada com a a consultado informada
		}else if($objRet->ret==false) {
			return($this->azReturn('Nenhuma ação pôde ser registrada.','info',false,$objRet));
		}
	}
	/**
	 * Executa um determinado tipo de token
	 *
	 * @return string json mensagem sucesso + as linhas encontradas ou falha
	 */
	public function doLoginByToken() {
		// t é o token recebido do dispositivo e u é o nome de usuário
		if(strlen($_GET['t'])==32 && isset($_GET['u']) && $_SESSION[SES_LOGIN]==false) {
			// Utiliza a model principal
			$ret = mMain::verifyLoginByToken($_GET['u'],$_GET['t']);

			// Se retornou um array, significa que o token foi validado
			if(count($ret)>0 && is_array($ret)) {
				$row = $ret[0];

				$_SESSION[SES_LOGIN]=true;
				$_SESSION[SES_USER_NAME]=$row['USE_NAME'];
				$_SESSION[SES_USER_ID]=$row['USE_ID'];
				$_SESSION[SES_USER_LEVEL]=$row['USE_LEVEL'];
				// Cria e salva Token de acesso
				$_SESSION[SES_TOKEN] = Controller::getAccessToken(true);
				// Salva hora e ip de acesso ao sistema
				$obj = new stdClass();
				$cp = new Fields();
				$cp->add('ACC_MODULE', _module);
				$cp->add('ACC_TOKEN',$_GET['t']);
				$cp->add('ACC_URL',$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
				$cp->add('ACC_NAVEGATOR', $_SERVER['HTTP_USER_AGENT']);
				$cp->add('ACC_IP', Controller::IP());
				$cp->add('AZ_DHREGISTER', date('Y-m-d H:i:s'));
				$cp->add('AZ_USREGISTER', $_GET['u']);
				$obj->fields = $cp->ret();
				$obj->table = DB_NAME.'.az_access';
				$obj->view = _view;
				$obj->webService = false;
				$ret = Controller::azInsert($obj);
				// Salva em sessão o ID de acesso
				$_SESSION[SES_ACC_ID] = $ret->data->ret->ID;
				// Retorna mensagem tipo sucesso
				return(Controller::azReturn('Login efetuado com sucesso.','win',false));
			// Falha
			}else {
				return(Controller::azReturn('Usuário ou token inválido.','fail',false));
			}
		}
	}
	/**
	 * Retorna um token de acesso seguro utilizado na validação das actions e dos acessos
	 * @param bool $secureAdvanced habilita segurança avançada (recomendado)
	 * @return string token
	 */
	public function getAccessToken($secureAdvanced=true) {
		// Token validação do acesso, modo avançado :
		// (mais seguro porém solicita login a cada mudança de ip do client)
		if($secureAdvanced!==false) {
			//$token =  md5($this->IP().$_SERVER['HTTP_USER_AGENT'].$_SESSION[SES_USER_ID]);
			$token =  md5($_SERVER['HTTP_USER_AGENT'].$_SESSION[SES_USER_ID]);

		// Token para validação de acesso simples (menos seguro)
		}else {
			$token =  md5($_SESSION[SES_USER_ID]);
		}
		return($token);
	}
	/**
	 * Valida sessão, checando se a mesma ainda está ativa
	 * e verificando o token de acesso
	 * @param bool $secureAdvanced habilita segurança avançada (recomendado)
	 * @return bool
	 */
	public function validSession($secureAdvanced=true){
		//die($_SESSION[SES_TOKEN]. " - ".$this->getAccessToken(false));
		// Verifica se a sessão está ativa e se o token é válido
		if ($_SESSION[SES_LOGIN]==true && $_SESSION[SES_TOKEN]==$this->getAccessToken($secureAdvanced)) {
			return(true);
		}else {
			return(false);
		}
	}
	/**
	 * Checar permissao para realizar a Ação na View, e caso seja informado, verifica se a tabela é publica ou privada
	 * sendo privada, é necessário estar logado no sistema e ter permissão de acesso a mesma
	 * @param string $acao | _I_ => Incluir, _A_ => Alterar, _V_ => Visualizar, _D_ => Deletar |
	 * @param string $view  nome do arquivo da view
	 * @param bool $secureAdvanced modo de segurança avançado para registro e validação de sessões
	 * @return true(permissao/acesso concedido), -1(sessão inativa), false(permissão negada)
	 */
	public function checksPermission($action, $view,$secureAdvanced=true){
		// Verifica se a view é privada e necessita de validação de acesso e permissões
		// Passa 'batido' se o usuário tem nível de  PROPRIETARIO, MANTENEDOR OU ADMINISTRATOR (se estiver com login ativo)
		if($this->validSession($secureAdvanced)) {
				if($_SESSION[SES_USER_LEVEL]!='OWNER' && $_SESSION[SES_USER_LEVEL]!='MAINTENANCE'
				&& $_SESSION[SES_USER_LEVEL]!='ADMINISTRATOR' ) {
					// Verifica se o usuario tem a permissão requisitada para determinada View
					$ret = mMain::checksPermission($view,$_SESSION[SES_USER_ID]);
					if($ret!=false && count($ret)>0) {
						$ret = $ret[0];
						if(strpos($ret['PER_PERMISSION'],$action) > -1) {
							return(true);
						}
					}
					// Caso a permissão nao seja encontrada, retorna false
					return(false);
				//PROPRIETARIO, MANTENEDOR ou ADMINISTRADOR, acesso concedido sem checagem de permissão
				}else {
					return(true);
				}
		// Sessão inválida ou inativa
		}else {
			return(-1);
		}
	}
	/**
	 * Identifica o IP do Client que enviou a requisição HTTP
	 * @return string $ip
	 */
	public function IP() {
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];

		}else{
		  $ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	/**
	 * Realiza requisições externas, client to server
	 * @param string $url ou ip do server, string $camposPOST formulario separados por '&',
	 * @param string $metodo GET ou POST
	 * @return string ou false
	 */
	public function loadExternal($url,$POSTFields='',$method='GET'){
		$ch = curl_init();
		//Requisição via GET
		if($method=='GET') {
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_FAILONERROR,1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 15);
			$retValue = trim(curl_exec($ch));

		//Requisição via POST
		}else {
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 15);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $POSTFields);
			$retValue = trim(curl_exec($ch));
		}
		curl_close($ch);
		return $retValue;
	}
	/**
	 * Convert um numero do padrão float para moeda e vice-versa
	 * @param string $valor, int $casasDec nr. de casas decimais
	 * @return string numero convertido
	 */
	public function convertNumber($valor,$casasDec=2) {
		if(empty($valor)) {
			return 0;	
		}
		// Padrão moeda : conversão para float
		if(strpos(",",$valor)>-1) {
			if (trim($valor)!="") {
				$valor_novo = str_replace(".","",$valor);
				$valor_novo =  str_replace(",",".",$valor_novo);
				return $valor_novo;
			}else return "0.00";
		// Padrão float : conversão para moeda
		}else {
			if (trim($valor)!="") {
			$valor_novo = number_format($valor,$casasDec,",",".");
			return $valor_novo;
			}else return "0,00";
		}
	}
	/**
	 * Retorna uma mensagem para view no padrão JSON
	 * @param string $msg, string $tipo tipo da mensagem retornada [mensagem, falha, sucesso],
	 * @param bool $webService : true -> encerra a execução e printa o retorno (JSON), StdClass objRetorno
	 * @param MySQLi $mysqli
	 * @return string json
	 */
  	protected function azReturn($message,$type='info', $webService=false, $objReturn=false){
			$message = str_replace('"',"'",$message);
			$message = trim(preg_replace('/\s+/', ' ', $message));
			if(DEBUG_MODE) {
				$return = '{"payload-original":{"action":"'.$_POST['action'].'", "module":"'._module.'", "view":"'._view.'","controller":"'.$_POST['controller'].'"}, "type":"'.$type.'", "message" : "'.$message.'","data":'.json_encode($objReturn).', "time":"'.date('H:i:s').'"}';
			}else {
				$return = '{"type":"'.$type.'", "message" : "'.$message.'","data":'.json_encode($objReturn).', "time":"'.date('H:i:s').'"}';
			}
	
		/*/ Registrar as falhas que possuem um objRetorno, no log "errosphp.log" do cliente
		// Sem, contudo, 'morrer' a aplicação
		if($objRetorno!==false && $tipo=='falha') {
			$linhaErro = "\r\n[".date('Y-m-d H:i:s')."] [FALHA] [".SYS_USER_IP."-".SES_USER_NAME."] [action: ".
			$_POST['action']."] \r\n  ".json_encode($objRetorno);
			error_log($linhaErro,3,SYS_FILE_LOG_ERROR_PHP);
		}//*/
		// Se for modo webservice, imprimi o retorno no padrão JSON e encerra a solicitação
		if($webService) {
			echo($return);
			exit();
		// Retorna um objeto stdClass e prossegue normalmente
		}else {
			return(json_decode($return));
		}
	}
}
