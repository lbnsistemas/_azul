<?php
/*
var_dump($_FILES);
var_dump($_POST);
exit();
/***
 * Starts a session with a specific timeout and a specific GC probability.
 * @param int $timeout The number of seconds until it should time out.
 * @param int $probability The probablity, in int percentage, that the garbage
 *        collection routine will be triggered right now.
 * @param strint $cookie_domain The domain path for the cookie.
 */
function session_start_timeout($timeout=5, $probability=100, $cookie_domain='/') {
	// Set the max lifetime
	ini_set("session.gc_maxlifetime", $timeout);

	// Set the session cookie to timout
	ini_set("session.cookie_lifetime", $timeout);

	// Change the save path. Sessions stored in teh same path
	// all share the same lifetime; the lowest lifetime will be
	// used for all. Therefore, for this to work, the session
	// must be stored in a directory where only sessions sharing
	// it's lifetime are. Best to just dynamically create on.
	$seperator = strstr(strtoupper(substr(PHP_OS, 0, 3)), "WIN") ? "\\" : "/";
	$path = $_SERVER['DOCUMENT_ROOT'] . $seperator . '..' . $seperator . "session_" . $timeout . "sec";
	if(!file_exists($path)) {
		if(!mkdir($path, 750)) {
			trigger_error("Failed to create session save path directory '$path'. Check permissions.", E_USER_ERROR);
		}
	}
	ini_set("session.save_path", $path);


	// Set the chance to trigger the garbage collection.
	ini_set("session.gc_probability", $probability);
	ini_set("session.gc_divisor", 100); // Should always be 100

	// Start the session!
	session_start();

	// Renew the time left until this session times out.
	// If you skip this, the session will time out based
	// on the time when it was created, rather than when
	// it was last used.
	if(isset($_COOKIE[session_name()])) {
		setcookie(session_name(), $_COOKIE[session_name()], time() + $timeout, $cookie_domain);
	}
}
// Iinicia/reinicia a sessão com x segundos de duração
session_start_timeout(60*60*24*30);//30 dias

// Sessões criadas pelo controller específico da view que requisitou o download
$urlFile = $_SESSION['urlFileToDownload'];
$srcFile = $_SESSION['srcFileToDownload'];

// Verifica a extensão do arquivo (não permite download de arquivos *.php, .htaccess)
if($urlFile!=''){
	// Pega somente a extensão do arquivo a ser baixado
	$ext = explode(".",$urlFile);
	$ext = end($ext);
	$extensao = strtolower($ext);
	
	
	// Verifica se é um arquivo proíbido
	if($extensao!='php' && $extensao!='htaccess' && $extensao!='') {

		// Caso não seja um arquivo proibido, retorna os cabeçalhos e o arquivo solicitado
		header('Content-Description: File Transfer');
        mb_internal_encoding('UTF-8');
		header('Content-type: text/html; charset=UTF-8');
        header('Content-Disposition: attachment; filename=' . basename($srcFile));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($srcFile));
		ob_clean();
		flush();
		readfile($srcFile);
		
		// Destrói as sessões de download
		$_SESSION['urlFileToDownload'] = null;
		$_SESSION['srcFileToDownload'] = null;
		
		
		// Verifica se é para deletar o arquivo 
		if(isset($_GET['deleteFile'])) {
			unlink($srcFile);
		}
		// Encerra
		exit;
	}
}

// Se chegar aqui o download não foi permitido
die('download não permitido');
	
?>	